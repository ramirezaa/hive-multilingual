Systemd Units
-------------

This directory contains systemd services and timers specific to this website.


# Initial setup

On the server, run the `enable.sh` script. Then, start `hive.target`:

    $ sudo /srv/hive/src/hive/systemd/enable.sh
    $ sudo systemctl start hive.target

Verify that the timer is running:

    $ sudo systemctl list-timers
