#!/bin/bash
DIR=$(dirname $(realpath $0));
systemctl enable "$DIR/hive-manage@.service";
systemctl enable "$DIR/hive-manage@.timer";
systemctl enable "$DIR/hive-manage@process_payments.timer";
systemctl enable "$DIR/hive.target";
