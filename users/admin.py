from django import forms
from django.contrib import admin
from hive.admin import admin_site
from users.forms import UserCreationForm
from contacts.models import Contact


class UserCreationAdminForm(UserCreationForm):
    first_name = forms.CharField()
    last_name = forms.CharField()

    def save(self, commit=True):
        user = super().save(commit=commit)
        if not user.contact:
            contact = Contact(
                first_name=self.cleaned_data['first_name'],
                last_name=self.cleaned_data['last_name']
            )
            contact.save(commit=commit)
            user.contact = contact
            user.save(commit=commit)
        else:
            user.contact.first_name = self.cleaned_data['first_name']
            user.contact.last_name = self.cleaned_data['first_name']


class UserAdmin(admin.ModelAdmin):
    form = UserCreationAdminForm
    list_display = ['email', 'name', 'is_active', 'is_superuser']
    list_filter = ['is_superuser']

    def name(self, obj):
        return obj.contact_id and str(obj.contact) or ''


# Disabling user management until I can make it more usable
# admin_site.register(models.User, UserAdmin)
