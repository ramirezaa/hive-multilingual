# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-24 16:43
from __future__ import unicode_literals

from django.db import migrations


def update_is_staff(apps, schema_editor):
    User = apps.get_model('users', 'User')
    User.objects.filter(is_superuser=True).update(is_staff=True)


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20170824_1109'),
    ]

    operations = [
        migrations.RunPython(update_is_staff)
    ]
