from django.utils.translation import activate


class UserLanguageMiddleware:
    """
    This middleware checks for a cookie called language-code and sets the
    user's language based on that value. If the cookie doesn't exist, it uses
    the `preferred_language` field from the user contact.
    """
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        lang = request.session.get('language_code')

        if request.GET.get('lang'):
            lang = request.GET['lang']

        request.session['language_code'] = lang

        if request.user and request.user.is_authenticated() and request.user.contact:
            contact = request.user.contact
            if lang is not None and lang != contact.preferred_language:
                contact.preferred_language = lang
                contact.save()

            lang = contact.preferred_language

        if lang is not None:
            activate(lang)

        response = self.get_response(request)
        return response
