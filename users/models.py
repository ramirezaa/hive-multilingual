from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

from contacts.models import Contact


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, is_superuser=False,
                    **extra_fields):
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, is_active=True,
                          is_superuser=is_superuser, last_login=now,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def get_by_natural_key(self, username):
        return self.get(email__iexact=username)

    def create_superuser(self, email, password, **extra_fields):
        return self.create_user(email, password, is_superuser=True,
                                **extra_fields)


class User(AbstractBaseUser):
    USERNAME_FIELD = 'email'
    email = models.EmailField(_('Email'), unique=True)
    contact = models.OneToOneField(Contact, related_name='user', null=True, blank=True)
    date_created = models.DateTimeField(default=timezone.now, editable=False)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False, editable=False)
    is_superuser = models.BooleanField(default=False, editable=False)
    objects = UserManager()

    def __str__(self):
        return self.get_full_name() or self.email

    @property
    def username(self):
        return self.email

    @property
    def email_with_name(self):
        name = self.get_full_name()
        if name:
            return '{0} <{1}>'.format(name, self.email)
        return self.email

    @property
    def photo(self):
        if self.contact_id and self.contact.photo:
            return self.contact.photo

    def save(self, *args, **kwargs):
        if self.contact_id:
            self.contact.email = self.email
            self.contact.save()
        super().save(*args, **kwargs)

    def get_full_name(self):
        if not self.contact_id:
            return self.email
        return '{0.first_name} {0.last_name}'.format(self.contact).strip()

    def get_short_name(self):
        if not self.contact_id:
            return self.email
        return self.contact.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def has_perm(self, perm, obj=None):
        if self.is_superuser:
            return True
        app_label, perm = perm.split('.')
        return self.has_module_perms(app_label)

    def has_module_perms(self, app_label):
        return self.is_superuser
