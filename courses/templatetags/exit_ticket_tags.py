import random
import re
from django.template import Library
from django.utils.safestring import mark_safe
from markdown import markdown

register = Library()


@register.filter
def format_first_line(text):
    lines = text.strip().splitlines()
    first_line = re.sub(r'^<p>(.*?)</p>', r'\1', markdown(lines[0]))
    return mark_safe(first_line)


@register.filter
def format_extra_lines(text):
    lines = text.strip().splitlines()
    extra_text = '\n'.join(lines[1:]).strip('\n')
    if extra_text.strip() == '':
        return ''
    html = markdown(extra_text, extensions=['markdown.extensions.codehilite'])
    return mark_safe(html)


@register.filter
def randomize_choices(choices):
    enumerated = list(enumerate(choices))
    random.shuffle(enumerated)
    return ((i + 1, choice) for i, choice in enumerated)
