from django.shortcuts import render, get_object_or_404
from django.contrib.auth.views import login_required
from django.contrib.auth import logout
from django.contrib import messages
from django.utils import timezone
from django.urls import reverse_lazy as reverse
from django import http

from courses import models
from programs.models import Program, Session
from students.models import Student, StudentLogin


def get_student(request):
    user = request.user
    if not user or not user.is_authenticated():
        return

    try:
        return user.student
    except Student.DoesNotExist:
        pass

    try:
        student = StudentLogin.objects.get(service='GOOGLE', username=user.email).student
    except StudentLogin.DoesNotExist:
        return

    # associate student w/ user
    student.user = user
    student.save()
    return student


def student_login_required(view):
    @login_required(login_url=reverse('login'))
    def wrapper(request, *args, **kwargs):
        student = get_student(request)
        if not student:
            messages.add_message(
                request, messages.ERROR,
                'Could not find student matching {}'.format(request.user.email))
            logout(request)
            return http.HttpResponseRedirect(reverse('login'))
        return view(request, *args, **kwargs)
    return wrapper


@student_login_required
def index(request):
    student = get_student(request)
    # programs = student.programs.current().exclude(course=None)
    programs = student.programs.all().exclude(course=None)
    context = {
        'programs': programs
    }
    return render(request, 'courses/index.html', context)


@student_login_required
def course(request, program_id, session_number=None):
    program = get_object_or_404(Program, pk=program_id)
    student = get_student(request)

    if session_number:
        session = get_object_or_404(
            Session, program=program, type='PROGRAM', number=session_number)
    else:
        today = timezone.now().date()
        session = program.sessions.filter(type='PROGRAM', date__gte=today).order_by('date')[0]

    exit_tickets = []
    if program.course:
        for exit_ticket in program.course.exit_tickets.order_by('-session_number'):
            exit_tickets.append((exit_ticket, exit_ticket.get_student_best(student)))

    context = {
        'program': program,
        'session': session,
        'exit_tickets': exit_tickets
    }
    return render(request, 'courses/course.html', context)


@student_login_required
def session(request, program_id, session_number=None):
    program = get_object_or_404(Program, pk=program_id)
    student = get_student(request)

    if session_number:
        session = get_object_or_404(
            Session, program=program, type='PROGRAM', number=session_number)
    else:
        today = timezone.now().date()
        session = program.sessions.filter(type='PROGRAM', date__gte=today).order_by('date')[0]

    try:
        exit_ticket = program.course.exit_tickets.get(session_number=session.number)
    except models.ExitTicket.DoesNotExist:
        exit_ticket = None
        questions = []

    student_ticket = None
    error = None
    if exit_ticket:
        student_ticket = exit_ticket.get_student_latest(student)
        questions = exit_ticket.questions.order_by('question_number')
        error = None
        if exit_ticket.can_retake(student) and request.method == 'POST':
            answers = []
            for i, question in enumerate(questions):
                key = 'question_{}'.format(question.pk)
                answer = set(int(n.strip()) for n in request.POST.getlist(key))
                if len(answer) > 0:
                    answers.append(answer)

            num_answered = len(answers)

            if num_answered < exit_ticket.questions.count():
                messages.add_message(request, messages.ERROR, 'Please answer each question!')
            else:
                results = exit_ticket.build_results(answers)
                num_correct = sum(1 for r in results if r['is_correct'])
                student_ticket = models.StudentExitTicket.objects.create(
                    session=session,
                    exit_ticket=exit_ticket,
                    student=student,
                    num_questions=exit_ticket.questions.count(),
                    num_correct=num_correct,
                    results=results
                )
                return http.HttpResponseRedirect('.')

    context = {
        'program': program,
        'session': session,
        'exit_ticket': exit_ticket,
        'questions': questions.order_by('?'),
        'student_ticket': student_ticket,
        'error': error
    }
    return render(request, 'courses/session.html', context)
