from django.conf.urls import url
from courses import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    url('^$', views.index, name='courses-index'),
    url(r'^login/$',
        auth_views.LoginView.as_view(template_name='courses/student_login.html'), name='login'),
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),
    url('^courses/(?P<program_id>[^/]+)/$',
        views.course, name='courses-course'),
    url('^courses/(?P<program_id>[^/]+)/sessions/(?P<session_number>[^/]+)/$',
        views.session, name='courses-session')
]
