# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-09-24 18:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0008_studentexitticket_populate_dates'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentexitticket',
            name='date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
