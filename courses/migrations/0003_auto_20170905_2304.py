# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-06 04:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('courses', '0002_auto_20170827_0050'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='exitticketquestion',
            options={'ordering': ['exit_ticket', 'question_number']},
        ),
        migrations.AddField(
            model_name='studentexitticket',
            name='num_correct',
            field=models.IntegerField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='studentexitticket',
            name='num_questions',
            field=models.IntegerField(default=None),
            preserve_default=False,
        ),
    ]
