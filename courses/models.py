from datetime import timedelta
from django.contrib.postgres.fields import JSONField
from django.core.validators import validate_comma_separated_integer_list
from django.db import models
from django.utils import timezone
from students.models import Student


class Course(models.Model):
    category = models.ForeignKey('programs.ProgramCategory')
    name = models.CharField(max_length=64)
    description = models.TextField()
    detailed_description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class ExitTicket(models.Model):
    course = models.ForeignKey(Course, related_name='exit_tickets')
    session_number = models.PositiveIntegerField()

    def get_student_latest(self, student):
        student_tickets = StudentExitTicket.objects.filter(student=student, exit_ticket=self)
        if student_tickets.count() > 0:
            return student_tickets.order_by('-date')[0]
        return None

    def get_student_best(self, student):
        student_tickets = StudentExitTicket.objects.filter(student=student, exit_ticket=self)
        if student_tickets.count() > 0:
            return student_tickets.order_by('-num_correct', '-date')[0]
        return None

    def can_retake(self, student):
        # Students must wait 22 hours before re-taking
        time_ago = timezone.now() - timedelta(hours=22)
        recent_tickets = StudentExitTicket.objects.filter(
            student=student, exit_ticket=self, date__gte=time_ago)
        return recent_tickets.count() == 0

    def can_view_answers(self, student):
        # If students got 100%, they can view the answers at any time
        best = self.get_student_best(student)
        if best and best.score == 100:
            return True

        # Otherwise, students can only view answers within 10 hours of taking the exit ticket
        time_ago = timezone.now() - timedelta(hours=10)
        recent_tickets = StudentExitTicket.objects.filter(
            student=student, exit_ticket=self, date__gte=time_ago)
        if recent_tickets.count() > 0:
            if recent_tickets.latest('date').score == 100:
                return True
        return recent_tickets.count() > 0

    def save(self, *args, **kwargs):
        # update order fields
        for i, question in enumerate(self.questions.all()):
            question.order = i + 1
            question.save()
        super().save(*args, **kwargs)

    def build_results(self, answers, team=None):
        # FIXME: good god this is ugly
        """
        Returns answers in same format as StudentExitTicket, to be rendered with
        `courses/includes/exit_ticket_results.html`. If `team` is passed as an array of students,
        team stats will be calculated in addition to global stats.
        """
        results = []
        answers = list(answers)

        # Calculate stats
        global_results = {}
        team_results = {}

        for question in self.questions.all():
            global_results[question.id] = {
                'correct': 0,
                'answered': 0
            }
            if team:
                team_results[question.id] = {
                    'correct': 0,
                    'answered': 0
                }

        def get_percent(n, d):
            return d and round((n / d) * 100) or 0

        def update_result_choices(result, student_ticket):
            student_result = [
                r for r in student_ticket.results if r['question_id'] == result['question_id']][0]
            student_choices = sorted(student_result['choices'], key=lambda c: c['number'])

            for i, choice in enumerate(result['choices']):
                if not choice.get('global_result'):
                    choice['global_result'] = {'count': 0, 'percent': 0}
                if not choice.get('team_result'):
                    choice['team_result'] = {'count': 0, 'percent': 0}

                student_choice = student_choices[i]
                if student_choice['selected']:
                    choice['global_result']['count'] += 1
                    choice['global_result']['percent'] = get_percent(
                        choice['global_result']['count'],
                        global_results[result['question_id']]['answered'])
                    if student_ticket.student in team:
                        choice['team_result']['count'] += 1
                        choice['team_result']['percent'] = get_percent(
                            choice['team_result']['count'],
                            team_results[result['question_id']]['answered'])

        team = team or []
        for student_ticket in self.student_results.all():
            for result in student_ticket.results:
                global_results[result['question_id']]['answered'] += 1
                if student_ticket.student in team:
                    team_results[result['question_id']]['answered'] += 1
                if result['is_correct']:
                    global_results[result['question_id']]['correct'] += 1
                    if student_ticket.student in team:
                        team_results[result['question_id']]['correct'] += 1

        # calculate percentages
        for r in global_results.values():
            r['percent'] = get_percent(r['correct'], r['answered'])
        for r in team_results.values():
            r['percent'] = get_percent(r['correct'], r['answered'])

        for i, question in enumerate(self.questions.all()):
            result = question.build_result(answers[i])
            result['global_results'] = global_results[question.id]
            if team_results:
                result['team_results'] = team_results[question.id]
            for student_ticket in self.student_results.all():
                update_result_choices(result, student_ticket)
            results.append(result)

        return results

    def __str__(self):
        return '{} - Session {}'.format(self.course.name, self.session_number)


class ExitTicketQuestion(models.Model):
    exit_ticket = models.ForeignKey(ExitTicket, related_name='questions')
    question_number = models.IntegerField()
    question_text = models.TextField(help_text="Markdown supported")
    choices = models.TextField(
        blank=True, help_text="One choice per line")
    correct_answers = models.CharField(
        validators=[validate_comma_separated_integer_list],
        max_length=32,
        help_text="Comma-separated numbers (1-n)")

    class Meta:
        unique_together = ['exit_ticket', 'question_number']
        ordering = ['exit_ticket', 'question_number']

    @property
    def choices_list(self):
        return self.choices.strip().splitlines()

    @property
    def multiple_choice(self):
        return len(self.correct_answers.split(',')) > 1

    @property
    def correct_answers_set(self):
        return set(int(n.strip()) for n in self.correct_answers.split(','))

    def build_result(self, answer):
        choices = []
        answer = set(answer)

        for c, choice in enumerate(self.choices_list):
            choice_num = c + 1
            choices.append({
                'number': choice_num,
                'text': choice.strip(),
                'correct': choice_num in self.correct_answers_set,
                'selected': choice_num in answer
            })

        return {
            'is_correct': (answer == self.correct_answers_set),
            'question_id': self.id,
            'question_number': self.question_number,
            'question_text': self.question_text,
            'choices': choices
        }


class StudentExitTicket(models.Model):
    date = models.DateTimeField(default=timezone.now)
    session = models.ForeignKey('programs.Session', related_name='exit_tickets')
    student = models.ForeignKey(Student, related_name='exit_ticket_results')
    exit_ticket = models.ForeignKey(ExitTicket, related_name='student_results')
    num_questions = models.IntegerField()
    num_correct = models.IntegerField()
    results = JSONField()

    @property
    def score(self):
        return int(round(self.num_correct / self.num_questions, 2) * 100)

    @property
    def can_retake(self):
        return self.exit_ticket.can_retake(self.student)

    @property
    def can_view_answers(self):
        return self.exit_ticket.can_view_answers(self.student)

# class Assessment(models.Model):
#   name = models.CharField(max_length=128)
