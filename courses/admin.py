from django.contrib import admin
from hive.admin import register
from . import models


@register(models.Course)
class CourseAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">school</i>'
    list_display = ['name', 'category']
    list_filter = ['category']


class ExitTicketQuestionInline(admin.StackedInline):
    model = models.ExitTicketQuestion
    fields = [
        'question_number',
        'question_text',
        'choices',
        'correct_answers'
    ]
    extra = 0


@register(models.ExitTicket)
class ExitTicketAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">live_help</i>'
    inlines = [ExitTicketQuestionInline]
    list_filter = ['course']
    list_display = ['__str__']
    fields = [('course', 'session_number')]
