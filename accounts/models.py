from django.db import models
from django.utils.translation import gettext_lazy as _
from bitfield import BitField
from contacts.models import Contact
from users.models import User
from . import constants


class Account(models.Model):
    user = models.OneToOneField(User, null=True)
    default_contact = models.OneToOneField(
        'AccountContact', related_name='account_default',
        null=True, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    billing_id = models.CharField(max_length=32, blank=True)
    flags = BitField(flags=constants.ACCOUNT_FLAGS)

    class Meta:
        ordering = ['default_contact__last_name', 'default_contact__first_name']

    @property
    def account_number(self):
        return '{:0>6}'.format(self.id)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # if the associated user doesn't have an associated contact, hook that
        # up with the account's default contact
        if self.user_id and self.default_contact and not self.user.contact_id:
            self.user.contact = self.default_contact.contact_ptr
            self.user.save()

    def __str__(self):
        return '#{} - {}'.format(self.account_number, self.default_contact)


class AccountContact(Contact):
    account = models.ForeignKey(Account, related_name='contacts')
    legal_guardian = models.BooleanField()
    emergency_contact = models.BooleanField()
    can_volunteer = models.BooleanField(default=False)
    relationship = models.CharField(
        _('Relationship to student'),
        max_length=100, help_text=_('eg: mother, father, legal guardian, etc.'))

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # If the associated account has not default contact, use this one
        if not self.account.default_contact:
            self.account.default_contact = self
            self.account.save()
