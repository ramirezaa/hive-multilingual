from django.contrib import admin
from import_export import resources, fields as ie_fields
from import_export.admin import ExportActionModelAdmin
from hive.admin import admin_site
from contacts.admin import ContactAdminMixin
from accounts import models


class AccountAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">folder_shared</i>'
    list_display = ['__str__', 'date_added']
    search_fields = [
        'id',
        'contacts__first_name',
        'contacts__last_name',
        'contacts__email',
        'students__first_name',
        'students__last_name'
    ]
    fields = ['user', 'default_contact']


class AccountContactResource(resources.ModelResource):
    account = ie_fields.Field()
    address = ie_fields.Field()

    class Meta:
        model = models.AccountContact
        fields = [
            'account', 'first_name', 'last_name', 'email', 'phone', 'address'
        ]

    def dehydrate_account(self, obj):
        return '#' + obj.account.account_number

    def dehydrate_address(self, obj):
        return obj.account.default_contact.formatted_address


class AccountContactAdmin(ContactAdminMixin, ExportActionModelAdmin):
    icon = '<i class="material-icons">person</i>'
    list_display = ['email', 'first_name', 'last_name', 'account']
    search_fields = [
        'first_name', 'last_name', 'account__account_number', 'email'
    ]
    resource_class = AccountContactResource

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields['account'].widget.can_add_related = False
        form.base_fields['account'].widget.can_edit_related = False
        form.base_fields['account'].required = False
        form.base_fields['account'].help_text = (
            'Leave blank to create a new account')
        return form

    def save_model(self, request, obj, form, change):
        account = None
        if not obj.account_id:
            # auto-create an account
            account = models.Account()
            account.save()
            obj.account = account
        super().save_model(request, obj, form, change)

    def __init__(self, *args, **kwargs):
        self.fieldsets = (
            ('Account Info', {'fields': (
                'account', 'legal_guardian', 'emergency_contact',
                'relationship'
            )}),
        ) + self.fieldsets
        super().__init__(*args, **kwargs)


admin_site.register(models.Account, AccountAdmin)
admin_site.register(models.AccountContact, AccountContactAdmin)
