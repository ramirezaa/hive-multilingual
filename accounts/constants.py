# If changing the order of these flags, create a datamigraiton to update
# existing records with the correct bits
ACCOUNT_FLAGS = (
    ('PAST_DUE', 'Past Due'),
    ('CC_FAILED', 'Credit Card Failed'),
    ('ON_HOLD', 'On Hold'),
    ('CANCELED', 'Canceled'),
    ('HOLD_NOTIFY', 'Hold Notifications')
)
