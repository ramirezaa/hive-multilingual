from django.apps import AppConfig


class AccountsConfig(AppConfig):
    icon = '<i class="material-icons">folder_shared</i>'
    name = 'accounts'
