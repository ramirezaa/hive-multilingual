import braintree
import logging
from decimal import Decimal
from django.conf import settings
from hashids import Hashids

from .exceptions import BillingError


logger = logging.getLogger(__name__)

# create hasher for non-predicatble unique account ids
id_hasher = Hashids(settings.SECRET_KEY, min_length=8)


def customer_id(account):
    return 'ACCT-{}'.format(account.id)


def order_id(registration):
    if registration.invoice_number:
        return registration.invoice_number
    return 'REG-{}'.format(registration.id)


def get_payment_methods(account):
    try:
        customer = braintree.Customer.find(customer_id(account))
    except braintree.exceptions.NotFoundError:
        return []
    return customer.payment_methods


def process_payment(request, registration, amount=None):
    """
    Processes the payment for a registration and sets recurring if applicable.
    The payment_method_nonce must exist in the request POST data.

    One-time payments can be made by passing an amount.
    """
    account = registration.account
    contact = account.default_contact

    # get create the client vault record
    try:
        customer = braintree.Customer.find(customer_id(account))
    except braintree.exceptions.NotFoundError:
        customer_result = braintree.Customer.create({
            'id': customer_id(account),
            'first_name': contact.first_name,
            'last_name': contact.last_name,
            'email': contact.email,
            'custom_fields': {
                'source': 'registration'
            }
        })
        if not customer_result.is_success:
            raise BillingError(customer_result.message)
        customer = customer_result.customer

    # Get or create the payment method
    if request.POST.get('payment_method_nonce'):
        address = request.POST.get('billing_address1')
        if request.POST.get('billing_address2'):
            address += '\n' + request.POST.get('billing_address2')
        payment_method_result = braintree.PaymentMethod.create({
            'customer_id': customer.id,
            'payment_method_nonce': request.POST['payment_method_nonce'],
            'billing_address': {
                'street_address': address,
                'locality': request.POST.get('billing_city'),
                'region': request.POST.get('billing_state'),
                'postal_code': request.POST.get('billing_zip')
            }
        })
        if not payment_method_result.is_success:
            raise BillingError(payment_method_result.message)
        payment_method_token = payment_method_result.payment_method.token
    elif request.POST.get('payment_method_token'):
        payment_method_token = request.POST['payment_method_token']
    else:
        raise ValueError('Payment method token or nonce was not provided')

    if not amount and registration.payment_frequency == 'ONE_TIME':
        amount = registration.invoice_total

    transaction = None

    if amount:
        # Force 2-digit precision in decimal amount
        amount = Decimal(amount) + Decimal('0.00')
        result = braintree.Transaction.sale({
            'payment_method_token': payment_method_token,
            'amount': amount,
            'order_id': order_id(registration),
            'options': {
                'submit_for_settlement': True
            }
        })
        if not result.is_success:
            raise BillingError(result.message)
        transaction = result.transaction

    elif registration.payment_frequency == 'MONTHLY':
        num_payments = registration.num_payments or 1
        amount = registration.invoice_total / num_payments
        # Force 2-digit precision in decimal amount
        amount = Decimal(amount) + Decimal('0.00')
        plan_id = 'PROGRAM_FEE_MONTHLY_{}'.format(num_payments)

        result = braintree.Subscription.create({
            'payment_method_token': payment_method_token,
            'plan_id': plan_id,
            'price': amount,
            'id': order_id(registration),
        })

        if len(result.subscription.transactions) > 0:
            transaction = result.subscription.transactions[0]

    else:
        raise ValueError('Amount or payment frequency not provided')

    return transaction


def cancel_transaction(transaction):
    # voids a recently submitted transaction and any associated subscriptions
    void_result = transaction.void(transaction.id)
    if not void_result.is_success:
        raise BillingError(void_result.message)
    if transaction.subscription_id:
        subscription = braintree.Subscription.find(transaction.subscription_id)
        cancel_result = subscription.cancel(subscription.id)
        if not cancel_result.is_success:
            raise BillingError(cancel_result.message)


def find_payments(order_id):
    try:
        subscription = braintree.Subscription.find(order_id)
    except braintree.exceptions.NotFoundError:
        transactions = braintree.Transaction.search(
            braintree.TransactionSearch.order_id == order_id)
    else:
        transactions = subscription.transactions

    for transaction in transactions:
        if transaction.status == 'settled':
            yield transaction
