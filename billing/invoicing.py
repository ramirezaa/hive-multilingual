import json
import logging
import re
from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django.conf import settings
from django.db import models
from xero import Xero
from xero.auth import PrivateCredentials
from xero.exceptions import XeroBadRequest
from . import payments


logger = logging.getLogger(__name__)

# set up xero credentials
xero = Xero(PrivateCredentials(
    settings.XERO_CONSUMER_KEY, settings.XERO_RSA_KEY))


def get_account_contact(account):
    contact = account.default_contact
    if not contact and account.contacts.count() > 0:
        contact = account.contacts.all()[0]
    return contact


def xero_contact_name(contact):
    if not contact.account:
        return None
    return '{} (#{})'.format(contact.name, contact.account.account_number)


def create_contact(account):
    phones = []
    contact = get_account_contact(account)
    if contact.phone:
        area_code, number = contact.phone.split('-', 1)
        phones.append({
            'PhoneAreaCode': area_code,
            'PhoneNumber': number,
            'PhoneType': 'DEFAULT'
        })
    name = xero_contact_name(contact)
    new_contact = {
        'AccountNumber': account.account_number,
        'ContactStatus': 'ACTIVE',
        'IsCustomer': True,
        'Name': name,
        'FirstName': contact.first_name,
        'LastName': contact.last_name,
        'EmailAddress': account.user.email,
        'ContactPersons': [],
        'ContactGroups': [],
        'Phones': phones,
        'Addresses': [
            {
                'AddressType': 'STREET',
                'PostalCode': contact.postal_code,
                'AddressLine1': contact.address1,
                'AddressLine2': contact.address2,
                'Region': contact.state,
                'City': contact.city
            }
        ],
    }
    xero.contacts.put(new_contact)


def get_contact(account):
    # search by account number, then by name with account number, then by name alone.
    searches = [
        {'AccountNumber': account.account_number},
    ]
    contact = get_account_contact(account)
    if contact:
        searches.append({'Name': xero_contact_name(contact)})
        searches.append({'Name': contact.name})

    for search in searches:
        result = xero.contacts.filter(**search)
        if len(result) > 0:
            return result[0]
    return None


def get_contact_by_id(contact_id):
    result = xero.contacts.filter(ContactID=contact_id)
    if len(result) > 0:
        return result[0]
    return None


def get_or_create_contact(account):
    contact = get_contact(account)
    if contact is not None:
        return contact
    create_contact(account)
    result = xero.contacts.filter(AccountNumber=account.account_number)
    return result[0]


def create_invoice(registration):
    contact = get_or_create_contact(registration.account)

    # XXX: This is a somewhat kludgy way of getting the last session date,
    # since theoretically someone can be registering for multiple programs with
    # different dates. It should, however, work for most use cases.
    sessions = registration.student_registrations.annotate(
        start_date=models.Min('program__sessions__date')
    )[0].program.sessions.all().order_by('date')
    if sessions.count() == 0:
        # if there are no sessions, assume due date is immediate
        due_date = timezone.now()
    elif registration.num_payments:
        # if registrant chose multiple payments, due date is last payment date
        due_date = timezone.now() + relativedelta(
            months=registration.num_payments)
    else:
        # otherwise, due date is one month after first session
        due_date = sessions[0].date + relativedelta(months=1)

    # build line items
    line_items = []
    for reg in registration.student_registrations.all():
        line_items.append({
            'Description': str(reg),
            'Quantity': 1,
            'UnitAmount': str(reg.program.fee),
            'AccountCode': settings.REGISTRATION_FEES_ACCOUNT_CODE
        })

    new_invoice = {
        'Type': 'ACCREC',
        'Status': 'AUTHORISED',
        'Contact': {'ContactID': contact['ContactID']},
        'Date': timezone.now(),
        'DueDate': due_date,
        'LineItems': line_items,
        'Reference': 'REG:{}'.format(registration.pk),
    }
    if settings.REGISTRATION_FEES_INVOICE_THEME_ID:
        new_invoice['BrandingThemeID'] = settings.REGISTRATION_FEES_INVOICE_THEME_ID
    result = xero.invoices.put(new_invoice)[0]
    registration.invoice_number = result['InvoiceNumber']
    registration.save()
    return result


def add_discount(invoice_number, amount, description=None):
    """
    Adds a discount to an invoice
    """
    if description is None:
        description = 'Discount'
    results = xero.invoices.filter(InvoiceNumber=invoice_number)
    if len(results) == 0:
        raise ValueError("Invoice {} was not found".format(invoice_number))
    invoice_id = results[0]['InvoiceID']
    invoice = xero.invoices.get(invoice_id)[0]
    invoice['LineItems'].append({
        'Description': description,
        'Quantity': 1,
        'UnitAmount': -amount,
        'AccountCode': settings.REGISTRATION_FEES_DISCUONTS_ACCOUNT_CODE
    })
    xero.invoices.save(invoice)


def get_invoices(xero_contact_id):
    return xero.invoices.filter(Contact_ContactID=xero_contact_id, order='Date')


def get_invoice(invoice_number):
    results = xero.invoices.filter(InvoiceNumber=invoice_number)
    if len(results) == 0:
        return None
    return results[0]


def get_invoice_by_id(invoice_id):
    results = xero.invoices.filter(InvoiceID=invoice_id)
    if len(results) == 0:
        return None
    return results[0]


def get_invoice_url(invoice_number):
    invoice = get_invoice(invoice_number)
    if invoice is None:
        return None

    result = xero.invoices.get(invoice['InvoiceID'] + '/OnlineInvoice')
    if len(result['OnlineInvoices']) == 0:
        return None
    return result['OnlineInvoices'][0]['OnlineInvoiceUrl']


def get_invoice_pdf(invoice_id):
    invoice = xero.invoices.get(invoice_id, headers={'Accept': 'application/pdf'})
    return invoice


def void_invoice(invoice_number):
    xero.invoices.save({
        'InvoiceNumber': invoice_number,
        'Status': 'VOIDED'
    })


def submit_payment(request, invoice, amount):
    # Get contact info from invoice
    contact = get_contact_by_id(invoice['Contact']['ContactID'])
    name = contact['Name']
    email = contact['EmailAddress']

    # FPG only allows alphanumeric for order ids
    order_id = re.sub(r'[^A-Za-z0-9]', '', invoice['InvoiceNumber'])

    return payments.submit_payment(request, order_id, amount, name=name, email=email)


def process_payments():
    """
    Find gateway payments for all unpaid invoices and add payments as necessary
    """

    invoices = xero.invoices.filter(raw='AmountDue > 0')

    for invoice in invoices:
        # make map of payments by reference so as not to create duplicates
        invoice_payments = dict(
            (p['Reference'], p) for p in invoice['Payments'] if p.get('Reference'))

        logger.info('Finding payments for invoice #{}'.format(
            invoice['InvoiceNumber']))

        invoice_notes = []

        # Find payments for this invoice
        order_id = invoice['InvoiceNumber']
        for transaction in payments.find_payments(order_id):
            ref = str(transaction.id)

            # if the payment has already been recorded, skip it
            if ref in invoice_payments:
                logger.debug('  skipping transaction #{} (already processed)'
                             .format(ref))
                continue

            logger.debug('  processing transaction #{} for ${}'.format(
                ref, transaction.amount))
            amount = transaction.amount
            date = transaction.created_at.date()

            amount_due = invoice['AmountDue']

            # add this payment to the invoice
            payment = {
                'Invoice': {'InvoiceID': invoice['InvoiceID']},
                'Account': {
                    'AccountID': settings.REGISTRATION_FEES_PAYMENT_ACCOUNT_ID
                },
                'Reference': ref,
                'Date': date,
                'Amount': str(amount)
            }

            if amount > amount_due:
                # The overpayment will have to be accounted for during bank
                # transaction processing or reconciliation, since Xero can't
                # account for overpayment at this step.
                payment['Amount'] = str(amount_due)

            logger.debug('Adding payment: ' +
                         json.dumps(payment, default=str, indent=2))

            try:
                xero.payments.put(payment)
            except XeroBadRequest as e:
                logger.error('Could not add payment to Xero: ' + e.problem)
                continue

            if amount > amount_due:
                # Create invoice note about overpayment
                note = 'Invoice was overpaid by ${} (ref: {})'.format(
                    amount - amount_due, ref)
                invoice_notes.append(note)
                logger.info(note)

            logger.info('Added payment of ${} to invoice #{}'.format(
                amount, invoice['InvoiceNumber']))

        # for note in invoice_notes:
        #   Can't do this yet. See here: http://bit.ly/2hrjbRT
