class BillingError(Exception):
    pass


class BillingValidationError(Exception):
    pass
