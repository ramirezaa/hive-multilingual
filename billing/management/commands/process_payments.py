from django.core.management.base import BaseCommand  # , CommandError
from billing.invoicing import process_payments


class Command(BaseCommand):
    help = 'Process invoice payments from the payment gateway'

    # def add_arguments(self, parser):
    #     parser.add_argument('some_id', nargs='+', type=int)

    def handle(self, *args, **options):
        process_payments()
