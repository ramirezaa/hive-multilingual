(function($) {

  function getField(el) {
    var $input = $(el);
    var $container = $input.parents('.input-field');
    var $label = $container.find('label');
    var $helper = $container.find('.helper-text');
    if (!$helper.length) {
      $helper = $('<span class="helper-text"></span>');
      $label.after($helper);
    }
    return {
      container: $container,
      input: $input,
      label: $label,
      helper: $helper
    }
  }

  function setFieldError(el, error) {
    var field = getField(el);
    field.input.addClass('invalid');
    field.helper.attr('data-error', error).show();
  }

  function resetField(el) {
    var field = getField(el);
    field.input.removeClass('invalid');
    field.helper.removeAttr('data-error');
    if (!field.helper.text()) {
      field.helper.hide();
    }
  }

  function resetFormErrors() {
    $('#formError').text('').hide();
    var fields = $('#form')[0].elements;
    for (var i=0; i<fields.length; i++) {
      resetField(fields[i]);
    }
  }

  function getValue(fieldName) {
    var sel = '#form [name=' + fieldName + ']';
    if ($(sel).attr('type') == 'radio') {
      return $(sel + ':checked').val();
    }
    return $(sel).val();
  }

  var submitText;
  function disableSubmit(text) {
    var submit = $('[type=submit]');
    submitText = submit.attr('value');
    submit.attr('disabled', 'disabled');
    if (text) {
      submit.attr('value', text);
    }
  }

  function enableSubmit() {
    var submit = $('[type=submit]');
    submit.removeAttr('disabled');
    submit.attr('value', submitText);
  }

  /**** Braintree Hosted Fields ****/
  // https://developers.braintreepayments.com/guides/hosted-fields/overview/javascript/v3

  var paymentFields = {
    number: {
      selector: '#cardNumber',
      description: 'credit card number'
    },
    expirationDate: {
      selector: '#expirationDate',
      description: 'expiration date'
    },
    cvv: {
      selector: '#cvv',
      description: 'cvv'
    }
  };

  function handleBraintreeError(err, client) {
    enableSubmit();

    var message = null;
    if (err.type == 'CUSTOMER') {
      var errors = {};
      if (err.code == 'HOSTED_FIELDS_FIELDS_EMPTY') {
        for (var field in paymentFields) {
          errors[field] = 'This field is required.';
        }
      }

      if (err.code == 'HOSTED_FIELDS_TOKENIZATION_CVV_VERIFICATION_FAILED') {
        message = 'CVV verification failed.';
        errors[field] = null;
      }

      if (err.code == 'HOSTED_FIELDS_FIELDS_INVALID') {
        var invalidKeys = err.details.invalidFieldKeys;
        for (var i=0; i<invalidKeys.length; i++) {
          var field = invalidKeys[i];
          errors[field] = paymentFields[field].description + ' is invalid.';
          message = message || errors[field];
        }
      }

      for (field in errors) {
        var $field = $(paymentFields[field].selector);
        setFieldError($field[0], errors[field]);
      }

      if (message) {
        // Not the cleanest way to show an error, but the user will be scrolled down at this point
        alert('There was an error with your payment information: ' + message);
      }
    } else {
      // TODO: set up sentry js
      console.error(err);
      throw new Error(err.message);
    }
  }

  function resetPaymentField(field) {
    resetField($(paymentFields[field].selector)[0]);
  }

  function resetPaymentForm() {
    $('#paymentError').text('').hide();
    for (var field in paymentFields) {
      resetPaymentField(field);
    }
  }

  braintree.client.create({
    authorization: window.BRAINTREE_TOKENIZATION_KEY
  }, function(err, client) {
    if (err) return handleBraintreeError(err, client);
    var options = {
      id: 'form',
      client: client,
      fields: paymentFields,
      styles: {
        '@media screen and (-webkit-min-device-pixel-ratio:0)': {
          'input': {
            'font-size': '16px'
          }
        }
      }
    };

    // set placeholder color
    /*
    var props = [
      '::placeholder', '::-webkit-input-placeholder', ':-moz-placeholder',
      '::-moz-placeholder', ':-ms-input-placeholder'
    ]
    for (var i=0; i<props.length; i++) {
      options.styles[props[i]] = {color: '#ccc'};
    }
    */

    braintree.hostedFields.create(options, function(err, hostedFields) {
      hostedFieldsInstance = hostedFields;
      if (err) return handleBraintreeError(err, client);
      enableSubmit();

      hostedFields.on('empty', function(event) {
        resetPaymentField(event.emittedBy);
      });

      hostedFields.on('notEmpty', function(event) {
        resetPaymentField(event.emittedBy);
      });

      hostedFields.on('inputSubmitRequest', function(event) {
        $('form').submit();
      });

      hostedFields.on('validityChange', function(event) {
        var fieldName = event.emittedBy;
        var field = event.fields[fieldName];
        var $field = $(field.container);
        var $label = $field.parents('.input-field').find('label');
        if (!(field.isValid || field.isPotentiallyValid)) {
          setFieldError(field.container, paymentFields[fieldName].description + ' is invalid.');
        } else {
          resetPaymentField(fieldName);
        }
      });

      hostedFields.on('focus', function(event) {
        resetPaymentForm();
        var fieldName = event.emittedBy;
        var field = event.fields[fieldName];
        $(field.container).addClass('focused');
        var label = $('label[for=' + field.container.id + ']');
        label.addClass('active');
        if (field.container.id == 'expirationDate' && field.isEmpty) {
          hostedFields.setAttribute({field: fieldName, attribute: 'placeholder', value: 'MM/YYYY'});
        }
      });

      hostedFields.on('blur', function(event) {
        var fieldName = event.emittedBy;
        var field = event.fields[fieldName];
        $(field.container).removeClass('focused');
        var label = $('label[for=' + field.container.id + ']');
        if (field.isEmpty) {
          label.removeClass('active');
        }
        if (field.container.id == 'expirationDate' && field.isEmpty) {
          hostedFields.removeAttribute({field: fieldName, attribute: 'placeholder'});
        }
      });

      $('#form').on('submit', function(event) {
        if ($('input[name="payment_method"]:checked').val() != 'ASSISTANCE') {
          disableSubmit('Please wait...');
          event.preventDefault();
          resetPaymentForm();

          hostedFields.tokenize(function(err, payload) {
            if (err) return handleBraintreeError(err, client);
            $('#form').append('<input type="hidden" name="payment_method_nonce" value="' + payload.nonce + '">');
            // prevent handler from going recursive
            $('#form').off('submit');
            $('#form').submit();
          });
        }
      });
    });
  });

})(jQuery);
