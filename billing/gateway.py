import json
import logging
import re
import requests
from datetime import date
from sys import version_info
from .exceptions import BillingError, BillingValidationError

if (version_info > (3, 0)):
    # Import urljoin for Python 3
    from urllib.parse import urljoin
else:
    # Import urljoin for Python 2
    from urlparse import urljoin


logger = logging.getLogger(__name__)


def _to_camel_case(s):
    return re.sub(r'(?!^)_([A-z])', lambda m: m.group(1).upper(), s)


def _to_lower_underscore(s):
    return re.sub(
        r'(?!^)([A-Z])', lambda m: '_' + m.group(1).lower(), s).lower()


def _json_default(obj):
    if isinstance(obj, date):
        return obj.strftime('%m/%d/%Y')
    return str(obj)


def _json_dump(obj):
    return json.dumps(obj, indent=2, default=_json_default)


class GatewayError(BillingError):
    def __init__(self, result):
        self.error_messages = result.error_messages
        super(GatewayError, self).__init__(self.error_messages[0])


class GatewayValidationError(BillingValidationError):
    def __init__(self, result):
        self.validation_failures = result.validation_failures
        self.error_messages = [
            e.message for e in self.validation_failures
        ]
        super(GatewayValidationError, self).__init__(self.error_messages[0])


class ResultObject(object):
    def __init__(self, data):
        self._data = data

    def __getattr__(self, attr):
        # convert key from lower_underscore to camelCase
        key = _to_camel_case(attr)

        if key not in self._data:
            # if camelCase doesn't work, try PascaleCase
            key = key[0].upper() + key[1:]

        if key not in self._data:
            raise AttributeError("'{}' object has no attribute {}'".format(
                self.__class__.__name__, attr))

        value = self._data[key]
        if type(value) == dict:
            return ResultObject(value)
        if type(value) == list:
            return [type(o) == dict and ResultObject(o) or o for o in value]
        return value

    def __setattr__(self, attr, value):
        if not attr.startswith('_'):
            raise AttributeError("Result attributes are read-only")
        super(ResultObject, self).__setattr__(attr, value)

    def __dir__(self):
        return [_to_lower_underscore(k) for k in self._data.keys()]

    def __repr__(self):
        def change_keys(o):
            new = {}
            for key, val in o.items():
                if type(val) == dict:
                    val = change_keys(val)
                if type(val) == list:
                    val = [
                        type(o) == dict and change_keys(o) or o for o in val]
                new[_to_lower_underscore(key)] = val
            return new
        return _json_dump(change_keys(self._data))


class Result(ResultObject):
    def __init__(self, response):
        self._raw_response = response
        try:
            data = json.loads(response.text)
        except json.JSONDecodeError:
            data = {
                'isSuccess': False,
                'errorMessages': [response.text]
            }
        super(Result, self).__init__(data)


class Client(object):
    VERSION = '1.2.0'

    def __init__(self, merchant_key, processor_id, test_mode=False):
        self.merchant_key = merchant_key
        self.processor_id = processor_id
        self.test_mode = test_mode

    @property
    def url(self):
        domain = 'secure.1stpaygateway.net'
        if self.test_mode:
            domain = 'secure-v.goemerchant.com'
        return 'https://{}/secure/RestGW/Gateway/Transaction/'.format(domain)

    def request(self, action, data):
        post_data = {}
        for key, value in data.items():
            # convert key from lower_underscore to camelCase
            key = re.sub(r'(?!^)_([A-z])', lambda m: m.group(1).upper(), key)
            post_data[key] = value

        post_data.update({
            'merchantKey': self.merchant_key,
            'processorId': self.processor_id
        })

        url = urljoin(self.url, action)
        headers = {'Content-Type': 'application/json', 'Charset': 'utf-8'}
        payload = json.dumps(post_data, default=_json_default, indent=2)

        logger.info('Sending payment gateway request:')
        logger.info('  URL: ' + url)
        logger.info('  HEADERS: ' + json.dumps(headers))
        logger.info('  POST DATA: ' + payload)

        response = requests.post(url, headers=headers, data=payload)

        result = Result(response)
        if not result.is_success:
            if result.validation_has_failed:
                raise GatewayValidationError(result)
            logger.error('Gateway Response: ' + response.text)
            raise GatewayError(result)

        logger.debug('  RESULT: ' + _json_dump(result._data))
        return result

    def create_auth(self, **data):
        return self.request('Auth', data)

    def create_auth_using_vault(self, **data):
        return self.request('AuthUsingValut', data)

    def create_sale(self, **data):
        return self.request('Sale', data)

    def create_sale_vault(self, **data):
        return self.request('SaleUsingVault', data)

    def create_credit(self, **data):
        return self.request('Credit', data)

    def create_credit_retail_only(self, **data):
        return self.request('CreditRetailOnly', data)

    def create_credit_retail_only_using_vault(self, **data):
        return self.request('CreditRetailOnlyUsingVault', data)

    def perform_void(self, **data):
        return self.request('Void', data)

    def create_re_auth(self, **data):
        return self.request('ReAuth', data)

    def create_re_sale(self, **data):
        return self.request('ReSale', data)

    def create_re_debit(self, **data):
        return self.request('ReDebit', data)

    def query(self, **data):
        return self.request('Query', data)

    def close_batch(self, **data):
        return self.request('CloseBatch', data)

    def perform_settle(self, **data):
        return self.request('Settle', data)

    def apply_tip_adjust(self, **data):
        return self.request('TipAdjust', data)

    def perform_ach_void(self, **data):
        return self.request('AchVoid', data)

    def create_ach_credit(self, **data):
        return self.request('AchCredit', data)

    def create_ach_debit(self, **data):
        return self.request('AchDebit', data)

    def create_ach_credit_using_vault(self, **data):
        return self.request('AchCreditUsingVault', data)

    def create_ach_debit_using_vault(self, **data):
        return self.request('AchDebitUsingVault', data)

    def get_ach_categories(self, **data):
        return self.request('AchGetCategories', data)

    def create_ach_categories(self, **data):
        return self.request('AchCreateCategory', data)

    def delete_ach_categories(self, **data):
        return self.request('AchDeleteCategory', data)

    def setup_ach_store(self, **data):
        return self.request('AchSetupStore', data)

    def create_vault_container(self, **data):
        return self.request('VaultCreateContainer', data)

    def create_vault_ach_record(self, **data):
        return self.request('VaultCreateAchRecord', data)

    def create_vault_credit_card_record(self, **data):
        return self.request('VaultCreateCCRecord', data)

    def create_vault_shipping_record(self, **data):
        return self.request('VaultCreateShippingRecord', data)

    def delete_vault_container_and_all_assc_data(self, **data):
        return self.request('VaultDeleteContainerAndAllAsscData', data)

    def delete_vault_ach_record(self, **data):
        return self.request('VaultDeleteAchRecord', data)

    def delete_vault_credit_card_record(self, **data):
        return self.request('VaultDeleteCCRecord', data)

    def delete_vault_shipping_record(self, **data):
        return self.request('VaultDeleteShippingRecord', data)

    def update_vault_container(self, **data):
        return self.request('VaultUpdateContainer', data)

    def update_vault_ach_record(self, **data):
        return self.request('VaultUpdateAchRecord', data)

    def update_vault_credit_card_record(self, **data):
        return self.request('VaultUpdateCCRecord', data)

    def update_vault_shipping_record(self, **data):
        return self.request('VaultUpdateShippingRecord', data)

    def query_vault(self, **data):
        return self.request('VaultQueryVault', data)

    def query_vault_for_credit_card_records(self, **data):
        return self.request('VaultQueryCCRecord', data)

    def query_vault_for_ach_records(self, **data):
        return self.request('VaultQueryAchRecord', data)

    def query_vault_for_shipping_records(self, **data):
        return self.request('VaultQueryShippingRecord', data)

    def modify_recurring(self, **data):
        return self.request('RecurringModify', data)

    def submit_acct_updater(self, **data):
        return self.request('AccountUpdaterSubmit', data)

    def submit_acct_updater_vault(self, **data):
        return self.request('AccountUpdaterSubmitVault', data)

    def get_acct_updater_return(self, **data):
        return self.request('AccountUpdaterReturn', data)
