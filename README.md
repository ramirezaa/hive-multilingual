# Hive
Hive is Bold Idea's student and volunteer registration system.

The hive codebase serves as an endpoint three websites. All data is
stored in one database.

**hive.boldidea.org** is the "central nervous system" of Bold Idea. It
includes all functionality needed by volunteers and staff. The admin
site (used only by staff and certain authorized volunteers) is located
at hive.boldidea.org/admin.

**programs.boldidea.org** is the registration site where parents sign up
their students.

**teams.boldidea.org** is for students. Right now, this site serves as a
home for student assessments, but we may use it for other functionality
in the future, such as collaborating w/ teammates, giving students a
page to track their overall year-to-year progress at Bold Idea, etc.

## Contributing
### Coding Style
Please follow the Django coding standards when contributing to Hive.

https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/

### Requirements
* git
* python >= 3.6
* postgresql >= 9.5
* postgis
* pip
* virtualenv

Make sure you have pip installed. If not, run:

    $ sudo easy_install pip

The `easy_install` command is available in the `python-setuptools` package.
Make sure you're using the `easy_install` that came bundled with python3 (some
systems have python2 and python3 installed together).

Next, make sure you have virtualenv installed for python3. If not, you can run:

    $ sudo pip install virtualenv

Note: Some systems with python2 and python3 installed together have separate
`pip` and `pip3` commands. Newer systems may have `pip2` for python2 and `pip`
for python3.

You can run `pip --version` to see which python version it uses:

    $ pip --version
    pip 9.0.1 from /usr/lib/python2.7/site-packages (python 2.7)
    $ pip3 --version
    pip 9.0.1 from /usr/lib/python3.6/site-packages (python 3.6)

### Setting up the local environment
If you're familiar with virtualenv already, you're welcome to use your own
workflow. Be sure to read the section on _Environment variables_ below. The
`virtualenvwrapper` tool can be used to easily manage your virtualenvs. For
information on how to install and configure virtualenv, see
[the documentation][1].

Here's the setup process for hive once you've installed and configured
virtualenvwrapper:

    $ git clone git@bitbucket.org:boldidea/hive.git
    $ mkvirtualenv hive
    $ cd hive
    $ workon hive
    $ pip install -e .

The repository has a setup.py, hence why `pip install -e .` works. This
symlinks the working source directory to the virtualenv's installed packages,
and also installs the project's python dependencies.

### Static and media directories
The project's `STATIC_DIR` and `MEDIA_DIR` should not be stored in the
repository, as they contain files generated per-environment. The best place for
these is in your virtualenv directory.

    $ mkdir $VIRTUAL_ENV/static
    $ mkdir $VIRTUAL_ENV/media

### Environment variables
Hive relies on environment variables for environment-specific settings. It's
important that you *do not* store any of these variables in the repository, as
they contain sensitive information and environment-specific settings.

You can store defaults for these environment variables in your
`$VIRTUAL_ENV` directory in a file called config.env.

    $ open $VIRTUAL_ENV/config.env

The environment variables needed by hive are as follows:

```sh
DJANGO_SITE=hive
DJANGO_SETTINGS_MODULE=hive.settings
DJANGO_DEBUG=True
DJANGO_SECRET_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
DJANGO_STATIC_ROOT=$VIRTUAL_ENV/static
DJANGO_MEDIA_ROOT=$VIRTUAL_ENV/media
BRAINTREE_ENVIRONMENT=sandbox
BRAINTREE_TOKENIZATION_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
BRAINTREE_MERCHANT_ID=XXXXXXXXXXXXXXXX
BRAINTREE_PUBLIC_KEY=XXXXXXXXXXXXXXXX
BRAINTREE_PRIVATE_KEY=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
XERO_CONSUMER_KEY=XXXXXXXXXXXXX
XERO_RSA_KEY_FILE=XXXXXXXXXXXXX
DJANGO_DB_HOST="localhost"
DJANGO_DB_NAME="hive_development"
DJANGO_DB_USER="XXXXXXXX"
DJANGO_DB_PASSWORD="XXXXXXXX"
```

`DJANGO_DB_USER` and `DJANGO_DB_PASSWORD` are optional if you've
set up postgresql locally with [Trust Authentication][3].

#### Sensitive information
The settings with "XXXX..." contain sensitive information. **Never
commit sensitive information such as private keys or passwords to the
repository.**

You'll need to generate your own value for `DJANGO_SECRET_KEY`. You can
use the following python script to generate one:

```python
#!/usr/bin/env python
import random
chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_%@'
print(''.join([random.SystemRandom().choice(chars) for i in range(50)]))
```
For the correct `BRAINTREE_*` and `XERO_*` settings, ask Ben on
the #hive-dev slack channel. You can leave out these settings if you're
just getting set up and add them later.

### Postgis installation
Install the `postgis` package, then run the following command in a psql
session:

```sql
CREATE EXTENSION postgis;
```

The `FPG_*` and `XERO_*` settings only needed for the registration and
admin sites. If you need these settings, contact Ben Davis
<ben@boldidea.org>.

The `DJANGO_SITE` setting controls which site you're working on. It
defaults to "hive", which is the site for hive.boldidea.org (volunteers
and staff). If you're working on programs.boldidea.org (the registration
site), change this setting to "programs". If you're working on
teams.boldidea.org (the student site), change this setting to "teams".

### Branches & tags
We don't use any real versioning scheme for Hive, since there's only one
deployment. Tags are used to tag indivudal deployments. The `deploy.sh`
script automatically tags deployments.

The `master` branch contains the code to be deployed. When working on a
"hotfix", you can work on the master branch. When developing a new
feature that will take more than a day to work on, create a new branch
for that feature. Once the branch has been merged into `master`, it can
be deleted.


[1] https://virtualenvwrapper.readthedocs.io/en/latest/install.html

[2] http://virtualenvwrapper.readthedocs.io/en/latest/scripts.html#postactivate

[3] https://www.postgresql.org/docs/9.6/static/auth-methods.html
