import uuid
from setuptools import setup, find_packages

try: # for pip >= 10
    from pip._internal.req import parse_requirements
except ImportError: # for pip <= 9.0.3
    from pip.req import parse_requirements

setup(
    name='hive',
    version='1.0',
    packages=find_packages(),
    install_requires=[
        str(r.req) for r in
        parse_requirements('requirements.txt', session=uuid.uuid1())
        if r.req
    ],
    package_data={'hive': []},
    include_package_data=True,
    long_description=open('README.md').read(),
    scripts=['manage.py']
)
