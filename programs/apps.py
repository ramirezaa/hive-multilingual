from django.apps import AppConfig


class ProgramsConfig(AppConfig):
    name = 'programs'
    icon = '<i class="material-icons">import_contacts</i>'
