import json

from django.conf import settings
from django.contrib import admin
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.db.models import F, Count
from django.utils.dateformat import format as date_format
from django.urls import reverse
from import_export import resources, fields as ie_fields
from import_export.admin import ExportActionModelAdmin
from hive.admin import register
from billing import invoicing
from programs import models


@register(models.ProgramCategory)
class ProgramCategoryAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">folder_open</i>'
    list_display = ['name']
    fields = ['name', 'slug', 'photo', 'description']
    prepopulated_fields = {'slug': ('name',), }


class SessionInline(admin.TabularInline):
    model = models.Session
    fields = ['type', 'date', 'start_time', 'end_time', 'description']
    extra = 0


class SemesterFilter(admin.SimpleListFilter):
    title = 'semester'
    parameter_name = 'semester'

    def lookups(self, request, model_admin):
        sessions = models.Session.objects.all().with_semester()
        sessions = sessions.values('year', 'semester').distinct()
        for item in sessions.order_by('-year', 'semester'):
            yield (
                '{}-{}'.format(item['year'], item['semester']),
                '{} {}'.format(item['semester'].title(), item['year'])
            )

    def queryset(self, request, queryset):
        val = self.value()
        if val is None:
            return queryset
        year, semester = val.split('-')
        return queryset.for_semester(year, semester)


@register(models.Program)
class ProgramAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">import_contacts</i>'
    list_display = [
        'name', 'program_code', '_ages', 'schedule_description', '_start_date',
        '_num_sessions'
    ]
    list_filter = [
        SemesterFilter, 'category', 'location', 'enrollment_type'
    ]
    fields = [
        ('name', 'photo'),
        ('program_code', 'category'),
        'volunteer_requirements',
        'hide_on_volunteer_signup',
        ('enrollment_type', 'fee'),
        'registration_url',
        ('allow_monthly_payments', 'num_monthly_payments'),
        'base_description',
        'location',
        'location_info',
        ('min_age', 'max_age'), ('min_grade', 'max_grade'),
        ('num_student_seats', 'student_volunteer_ratio'),
        ('registration_open_date', 'registration_close_date'),
        'schedule_description'
    ]
    inlines = [SessionInline]

    def change_view(self, request, object_id, form_url='', extra_context=None):
        # provide data for default location instructions when location is
        # selected
        extra_context = extra_context or {}
        extra_context['location_json'] = json.dumps(
            {l.pk: l.instructions for l in models.Location.objects.all()},
            indent=2)
        return super().change_view(request, object_id, form_url, extra_context)

    def _ages(self, obj):
        return obj.age_range_description
    _ages.short_description = 'Ages'

    def _start_date(self, obj):
        return obj.start_date
    _start_date.short_description = 'Start date'
    _start_date.admin_order_field = 'start_date'

    def _num_sessions(self, obj):
        return obj.sessions.count()
    _num_sessions.short_description = '# Sessions'


class RegistrationProgramCategoryFilter(admin.SimpleListFilter):
    title = 'Program category'
    parameter_name = 'category_id'

    def lookups(self, request, model_admin):
        return ((c.pk, c.name) for c in models.ProgramCategory.objects.all())

    def queryset(self, request, queryset):
        val = self.value()
        if val is None:
            return queryset
        return queryset.filter(
            student_registrations__program__category_id=self.value()
        ).distinct()


class RegistrationProgramLocationFilter(admin.SimpleListFilter):
    title = 'Program location'
    parameter_name = 'location_id'

    def lookups(self, request, model_admin):
        return ((l.pk, l.name) for l in models.Location.objects.all())

    def queryset(self, request, queryset):
        val = self.value()
        if val is None:
            return queryset
        return queryset.filter(
            student_registrations__program__location_id=self.value()
        ).distinct()


class RegistrationResource(resources.ModelResource):
    account = ie_fields.Field()
    program = ie_fields.Field()
    parent_first_name = ie_fields.Field()
    parent_last_name = ie_fields.Field()
    parent_email = ie_fields.Field()
    parent_phone = ie_fields.Field()
    address = ie_fields.Field()
    students = ie_fields.Field()
    payment_method = ie_fields.Field()

    class Meta:
        model = models.Registration
        fields = [
            'id', 'date', 'account', 'program', 'parent_first_name',
            'parent_last_name', 'parent_email', 'students', 'address',
            'payment_method', 'financial_assistance_requested',
            'financial_assistance_granted', 'invoice_number'
        ]
        export_order = fields

    def dehydrate_account(self, obj):
        return '#' + obj.account.account_number

    def dehydrate_program(self, obj):
        return obj.program_name

    def dehydrate_parent_first_name(self, obj):
        return obj.account.default_contact.first_name

    def dehydrate_parent_last_name(self, obj):
        return obj.account.default_contact.last_name

    def dehydrate_parent_email(self, obj):
        return obj.account.default_contact.email

    def dehydrate_parent_phone(self, obj):
        return obj.account.default_contact.phone

    def dehydrate_address(self, obj):
        return obj.account.default_contact.formatted_address

    def dehydrate_students(self, obj):
        students = obj.student_registrations.filter(program__id=obj.program_id)
        names = [s.student.first_name for s in students]
        if len(names) == 1:
            return names[0]
        elif len(names) == 2:
            return ' and '.join(names)
        else:
            names[-1] = 'and ' + names[-1]
            return ', '.join(names)

    def dehydrate_payment_method(self, obj):
        return obj.get_payment_method_display()


@register(models.Registration)
class RegistrationAdmin(ExportActionModelAdmin):
    icon = '<i class="material-icons">assignment_turned_in</i>'
    """
    This ModelAdmin is mostly just used to view the registration details and
    grant a financial aid amount.
    """
    list_display = [
        '_regnum', '_date', '_account', '_program', '_parent_name',
        '_num_students', '_payment_method'
    ]

    search_fields = [
        'student_registrations__student__first_name',
        'student_registrations__student__last_name',
        'account__default_contact__first_name',
        'account__default_contact__last_name'
    ]

    list_filter = [
        SemesterFilter,
        RegistrationProgramCategoryFilter,
        RegistrationProgramLocationFilter,
        'payment_method'
    ]

    fields = ['financial_assistance_granted']

    resource_class = RegistrationResource

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.annotate(
            program_id=F('student_registrations__program__id'),
            program_name=F('student_registrations__program__name'),
            num_students=Count('student_registrations', distinct=True)
        )

    def get_object(self, request, object_id, from_field=None):
        model = self.model
        queryset = model.objects.all()
        if from_field is None:
            field = model._meta.pk
        else:
            field = model._meta.get_field(from_field)
        try:
            object_id = field.to_python(object_id)
            return queryset.get(**{field.name: object_id})
        except (model.DoesNotExist, ValidationError, ValueError):
            return None

    def get_readonly_fields(self, request, obj=None):
        if obj.financial_assistance_granted is None:
            return []
        return ['financial_assistance_granted']

    def _regnum(self, obj):
        return '#' + obj.registration_number
    _regnum.short_description = 'Reg. #'
    _regnum.admin_order_field = 'registration__id'

    def _date(self, obj):
        return date_format(obj.date, settings.DATE_FORMAT)
    _date.short_description = 'Date'
    _date.admin_order_field = 'date'

    def _account(self, obj):
        return '#' + obj.account.account_number
    _account.short_description = 'Account #'
    _account.admin_order_field = 'account__id'

    def _program(self, obj):
        return obj.program_name
    _program.short_description = 'Program'
    _program.admin_order_field = 'student_registrations__program__name'

    def _parent_name(self, obj):
        return '{0.first_name} {0.last_name}'.format(
            obj.account.default_contact)
    _parent_name.short_description = 'Parent name'
    _parent_name.admin_order_field = 'account__default_contact__first_name'

    def _num_students(self, obj):
        return obj.num_students
    _num_students.short_description = '# Students'
    _num_students.admin_order_field = 'num_students'

    def _parent_email(self, obj):
        return '<a href="mailto:{0}" target="_blank">{0}</a>'.format(
            obj.account.default_contact.email)
    _parent_email.short_description = 'Parent email'
    _parent_name.admin_order_field = 'account__default_contact__email'
    _parent_email.allow_tags = True

    def _payment_method(self, obj):
        return obj.get_payment_method_display()
    _payment_method.short_description = 'Payment method'
    _payment_method.admin_order_field = 'payment_method'

    def save_model(self, request, obj, form, change):
        existing = models.Registration.objects.get(pk=obj.pk)
        changed = (existing.financial_assistance_granted is None and
                   obj.financial_assistance_granted is not None)

        if changed and obj.invoice_number is None:
            raise ValidationError(
                "Cannot apply discount: An invoice does not exist for this "
                "registration.")
        invoicing.add_discount(
            obj.invoice_number,
            obj.financial_assistance_granted,
            'Financial assistance discount'
        )

        obj.student_registrations.filter(status='PENDING').update(status='COMPLETED')

        messages.add_message(
            request,
            messages.INFO,
            "Added discount for invoice #{}".format(obj.invoice_number)
        )

        return super().save_model(request, obj, form, change)


@register(models.Location)
class LocationAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">pin_drop</i>'
    fieldsets = (
        (None, {'fields': (
            'name', 'nickname', 'about', 'logo', 'photo', 'website', 'phone',
            'extension', 'notes'
        )}),
        ('Address', {'fields': ((
            'address1', 'address2', 'city', 'state', 'postal_code'
        ), 'instructions')})
    )


# @register(models.StudentRegistration)
class StudentRegistrationAdmin(admin.ModelAdmin):
    list_filter = [
        'program__category', 'program__location',
    ]
    list_display = [
        '_regnum', '_student', '_date', '_category', '_location',
    ]
    list_display_links = None
    fields = [
        'student', 'registration'
    ]

    def get_queryset(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs).select_related(
            'program', 'registration')

    def _student(self, obj):
        return '{0.first_name} {0.last_name}'.format(obj.student)
    _student.short_description = 'Student'
    _student.admin_order_field = 'student__first_name'

    def _regnum(self, obj):
        reg = obj.registration
        url = reverse('admin:programs_registration_change', args=[reg.id])
        return '<a href="{}">{}</a>'.format(url, reg.registration_number)
    _regnum.short_description = 'Reg. #'
    _regnum.admin_order_field = 'registration__id'
    _regnum.allow_tags = True

    def _date(self, obj):
        return date_format(obj.registration.date, settings.DATE_FORMAT)
    _date.short_description = 'Date'
    _date.admin_order_field = 'registration__date'

    def _category(self, obj):
        return obj.program.category
    _category.short_description = 'Category'
    _category.admin_order_field = 'program__category__name'

    def _location(self, obj):
        return str(obj.program.location)
    _location.short_description = 'Location'
    _location.admin_order_field = 'program__location__name'
