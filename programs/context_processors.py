from .cart import SessionCart


def session_cart(request):
    cart = SessionCart(request)
    return {
        'cart': cart
    }
