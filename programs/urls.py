from django.conf.urls import url
from programs import views

urlpatterns = [
    url('^$', views.index, name='programs-index'),
    url('^cart/$', views.view_cart, name='programs-view-cart'),
    url('^checkout/$', views.checkout, name='programs-checkout'),
    url('^create-account/$', views.create_account,
        name='create-account'),
    url('^registration-complete/(?P<regid>[^/]+)/$',
        views.checkout_complete, name='programs-checkout-complete'),
    url('^my-account/$', views.my_account, name='programs-my-account'),
    url('^invoice/(?P<invoice_id>[^/]+)/$',
        views.view_invoice, name='programs-view-invoice'),
    url('^invoice/(?P<invoice_id>[^/]+)/pdf/$',
        views.view_invoice_pdf, name='programs-view-invoice-pdf'),
    url('^(?P<category>[^/]+)/$', views.category_index,
        name='programs-category-index'),
    url('^(?P<category>[^/]+)/(?P<program_code>[^/]+)/$',
        views.view_program, name='programs-program'),
    url('^(?P<category>[^/]+)/(?P<program_code>[^/]+)/add/$',
        views.cart_add, name='programs-cart-add'),
    url('^(?P<category>[^/]+)/(?P<program_code>[^/]+)/schedule/$',
        views.program_schedule, name='programs-program-schedule'),
]
