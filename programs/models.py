import math
from django.core.exceptions import ValidationError
from django.contrib.gis.db.models import PointField
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.template import Template, Context
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils.dateformat import format as format_date
from django.utils import timezone
from localflavor.us import models as lf_models
import vinaigrette

from courses.models import Course
from accounts.models import Account
from students.models import Student
from volunteers.models import Volunteer, VolunteerRequirementType
from hive import utils
from .query import ProgramQuerySet, SemesterQuerySet, AttendanceQuerySet
from . import constants


class Location(models.Model):
    date_added = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=255)
    nickname = models.CharField(max_length=255, blank=True)
    about = models.TextField(blank=True)
    logo = models.ImageField(upload_to='programs/location_logos', blank=True)
    photo = models.ImageField(upload_to='programs/location_photos', blank=True)
    website = models.URLField(blank=True)
    address1 = models.CharField(max_length=255, blank=True)
    address2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state = lf_models.USStateField(blank=True)
    postal_code = lf_models.USZipCodeField(blank=True)
    phone = lf_models.PhoneNumberField(blank=True)
    extension = models.CharField(max_length=10, blank=True)
    instructions = models.TextField('Parking / other instructions', blank=True)
    notes = models.TextField(blank=True)
    geo = PointField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.date_modified = timezone.now()
        self.geo = utils.geocode('{} {}, {}, {} {}'.format(
            self.address1, self.address2, self.city, self.state,
            self.postal_code))
        super().save(*args, **kwargs)

    @property
    def short_name(self):
        return self.nickname or self.name

    @property
    def coords(self):
        return self.geo.coords

    def __str__(self):
        return self.name

vinaigrette.register(Location, ['about', 'instructions'])


class ProgramCategory(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    photo = models.ImageField(upload_to='programs/category_photos', blank=True)
    description = models.TextField(blank=True)
    learn_more_url = models.URLField(blank=True)

    class Meta:
        verbose_name_plural = 'Program categories'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse(
            'programs_category', kwargs={'slug': self.slug})

    @property
    def age_range_description(self):
        values = {}
        programs = self.programs.open_for_registration()
        for p in programs.filter(enrollment_type='PUBLIC'):
            min_grade = p.min_grade or 0
            max_grade = p.max_grade or 0
            min_age = p.min_age or 0
            max_age = p.max_age or 0
            if 'min_grade' not in values or min_grade < values['min_grade']:
                values['min_grade'] = min_grade
            if 'max_grade' not in values or max_grade > values['max_grade']:
                values['max_grade'] = max_grade
            if 'min_age' not in values or min_age < values['min_age']:
                values['min_age'] = min_age
            if 'max_age' not in values or max_age > values['max_age']:
                values['max_age'] = max_age
            print(p, values)

        return utils.get_age_range_description(**values)

vinaigrette.register(ProgramCategory, ['name', 'description'])


class ProgramManager(models.Manager.from_queryset(ProgramQuerySet)):
    use_in_migrations = True


class Program(models.Model):
    category = models.ForeignKey(
        ProgramCategory, related_name='programs', on_delete=models.PROTECT)
    name = models.CharField(max_length=255)
    program_code = models.CharField(max_length=32, unique=True, db_index=True)
    course = models.ForeignKey(Course, null=True, blank=True)
    base_description = models.TextField(
        'Description', blank=True, help_text='template tags available: {{location}}, {{course}}')
    location = models.ForeignKey(
        Location, related_name='programs', on_delete=models.PROTECT)
    location_info = models.TextField(
        'Location info / parking instructions', blank=True)
    photo = models.ImageField(upload_to='programs/program_photos', blank=True)
    registration_url = models.URLField(blank=True)
    enrollment_type = models.CharField(
        max_length=16, choices=constants.ENROLLMENT_TYPES)
    fee = models.DecimalField(max_digits=6, decimal_places=2)
    allow_monthly_payments = models.BooleanField()
    num_monthly_payments = models.IntegerField(null=True, blank=True)
    min_age = models.IntegerField(null=True, blank=True)
    max_age = models.IntegerField(null=True, blank=True)
    min_grade = models.IntegerField(null=True, blank=True)
    max_grade = models.IntegerField(null=True, blank=True)
    num_student_seats = models.IntegerField('# Student seats', null=True, blank=True)
    student_volunteer_ratio = models.DecimalField(
        max_digits=4, decimal_places=2, default=2.0,
        help_text='Ideal number of students per volunteer (for calculating coverage)')
    start_date = models.DateField(null=True, blank=True)
    registration_open_date = models.DateField(null=True, blank=True)
    registration_close_date = models.DateField(null=True, blank=True)
    schedule_description = models.CharField(
        max_length=255, blank=True, help_text="eg: Tuesdays at 2:00pm")
    hide_on_volunteer_signup = models.BooleanField(default=False)
    volunteer_requirements = models.ManyToManyField(
        VolunteerRequirementType, blank=True, related_name='programs')
    students = models.ManyToManyField(Student, through='StudentRegistration')
    volunteers = models.ManyToManyField(Volunteer, through='volunteers.VolunteerAssignment')

    objects = ProgramManager()

    class Meta:
        ordering = ['-pk']

    def __str__(self):
        return '{0.program_code} - {0.name}'.format(self)

    def clean(self):
        if self.allow_monthly_payments and self.num_monthly_payments is None:
            raise ValidationError(
                "num_monthly_payments is required when allow_monthly_payments "
                "is True")

    @property
    def description(self):
        context = Context({
            'location': self.location,
            'course': self.course
        })
        tpl = Template(self.base_description)
        return tpl.render(context)

    @property
    def schedule_description_with_date(self):
        if self.sessions.count() == 0:
            return self.schedule_description
        return '{schedule_description} - starts {start_date:%b %-d}'.format(
            schedule_description=self.schedule_description,
            start_date=self.start_date)

    @property
    def default_photo(self):
        if self.photo:
            return self.photo
        if self.location.photo:
            return self.location.photo
        return self.category.photo

    @property
    def is_full(self):
        if self.num_student_seats is not None:
            return self.students.count() >= self.num_student_seats
        return False

    @property
    def first_session(self):
        if self.sessions.count() > 0:
            return self.sessions.filter(type='PROGRAM').order_by('date')[0]

    @property
    def last_session(self):
        if self.sessions.count() > 0:
            return self.sessions.filter(type='PROGRAM').order_by('-date')[0]

    @property
    def add_to_cart_url(self):
        return reverse('programs-cart-add', kwargs={
            'category': self.category.slug,
            'program_code': self.program_code
        })

    @property
    def age_range_description(self):
        return utils.get_age_range_description(
            min_grade=self.min_grade,
            max_grade=self.max_grade,
            min_age=self.min_age,
            max_age=self.max_age
        )

    @property
    def url(self):
        return reverse('programs-program', kwargs={
            'category': self.category.slug,
            'program_code': self.program_code
        })

    @property
    def num_volunteers_per_session(self):
        return math.ceil(self.num_student_seats / self.student_volunteer_ratio)

    @property
    def student_slots(self):
        """
        Returns an array of "slots" a minimum size of `student_seats`, where
        each slot is either a `Registration` object or `None`.
        """
        slots = [None for i in range(0, self.num_student_seats or 0)]
        student_registrations = self.student_registrations.order_by('-student__gender')
        for i, studentreg in enumerate(student_registrations):
            if i >= len(slots):
                slots.append(studentreg)
            else:
                slots[i] = studentreg
        return slots

    @property
    def volunteer_slots(self):
        """
        Returns an array of "slots" with a minimum size of `num_volunteers_per_session`,
        where each slot is either a `Volunteer` object or `None`. The volunteer
        objects are also annotated with `.signup`, which is the matching
        `VolunteerSignup` object associated with this program.
        """
        volunteers = self.volunteers.order_by('-gender')
        slots = [None for i in range(0, self.num_volunteers_per_session or 0)]
        for i, volunteer in enumerate(volunteers):
            signups = volunteer.signups.filter(assignments__program=self)
            if signups.count() > 0:
                volunteer.signup = signups[0]
            if i >= len(slots):
                slots.append(volunteer)
            else:
                slots[i] = volunteer
        return slots

    @property
    def is_open_for_registration(self):
        today = timezone.now().date()
        return (
            (self.registration_open_date <= today or self.registration_date is None)
            and
            (self.registration_close_date > today or self.registration_close_date is None)
        )

    def get_absolute_url(self):
        return self.url


vinaigrette.register(Program, ['base_description', 'location_info', 'schedule_description'])


class Session(models.Model):
    program = models.ForeignKey(Program, related_name='sessions')
    type = models.CharField(max_length=32, choices=(
        ('PROGRAM', 'Pogram session'),
    ), default='PROGRAM')
    number = models.PositiveIntegerField(null=True)
    date = models.DateField()
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)
    description = models.CharField(max_length=255, blank=True)

    objects = SemesterQuerySet.as_manager('date')

    class Meta:
        ordering = ['date']
        unique_together = ['program', 'type', 'number']

    def __str__(self):
        return self.date_description

    @property
    def date_description(self):
        datestr = format_date(self.date, 'N j, Y')
        if self.start_time:
            datestr += ', ' + format_date(self.start_time, 'P')
        if self.end_time:
            datestr += ' - ' + format_date(self.end_time, 'P')
        return datestr

    def save(self, *args, **kwargs):
        if self.type == 'PROGRAM':
            if not self.number:
                self.number = self.program.sessions.filter(date__lt=self.date).count() + 1
            if self.number == 1 and not self.program.start_date:
                self.program.start_date = self.date
                self.program.save()
        super().save(*args, **kwargs)


class StudentAttendance(models.Model):
    """
    An attendance instance represents an expectation for a person to attend a session.
    """
    student = models.ForeignKey(Student, related_name='session_attendances')
    session = models.ForeignKey(Session, related_name='student_attendances')
    present = models.BooleanField(default=False)
    notes = models.TextField()

    objects = AttendanceQuerySet.as_manager()

    class Meta:
        unique_together = ['student', 'session']

    def is_future(self):
        return self.session.date > timezone.now().date()

    def __str__(self):
        status = self.present and 'PRESENT' or 'ABSENT'
        return '{:%Y-%m-%d} - {}: {}'.format(self.session.date, self.student, status)


class VolunteerAttendance(models.Model):
    """
    An attendance instance represents an expectation for a person to attend a session.
    """
    volunteer = models.ForeignKey(Volunteer, related_name='session_attendances')
    session = models.ForeignKey(Session, related_name='volunteer_attendances')
    present = models.BooleanField(default=False)
    notes = models.TextField()

    objects = AttendanceQuerySet.as_manager()

    class Meta:
        unique_together = ['volunteer', 'session']

    def is_future(self):
        return self.session.date > timezone.now().date()

    def __str__(self):
        status = self.present and 'PRESENT' or 'ABSENT'
        if self.is_future():
            return '{:%Y-%m-%d} - {}'.format(self.session.date, self.volunteer)
        else:
            return '{:%Y-%m-%d} - {}: {}'.format(self.session.date, self.volunteer, status)


class Registration(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    account = models.ForeignKey(Account, related_name='registrations')
    invoice_number = models.CharField(max_length=32, blank=True)
    payment_method = models.CharField(
        max_length=32, choices=constants.PAYMENT_METHODS, blank=True)
    num_payments = models.IntegerField(null=True, blank=True)
    payment_frequency = models.CharField(
        max_length=32, choices=constants.PAYMENT_FREQUENCIES, blank=True)
    payment_ref = models.CharField(max_length=128, blank=True)
    students = models.ManyToManyField(Student, through='StudentRegistration')
    programs = models.ManyToManyField(Program, through='StudentRegistration')
    financial_assistance_requested = models.DecimalField(
        max_digits=6, decimal_places=2, null=True, blank=True)
    financial_assistance_granted = models.DecimalField(
        max_digits=6, decimal_places=2, null=True, blank=True)
    form_data = JSONField(blank=True, null=True)

    objects = SemesterQuerySet.as_manager(
        'student_registrations__program__start_date')

    def __str__(self):
        return '#{} - {}'.format(
            self.registration_number, self.account.default_contact)

    @property
    def registration_number(self):
        return '{:0>6}'.format(self.id)

    @property
    def invoice_total(self):
        return sum(s.program.fee for s in self.student_registrations.all())

    def get_invoice_url(self):
        from billing.invoicing import get_invoice_url as get_url
        if self.invoice_number:
            return get_url(self.invoice_number)
        return None

    class Meta:
        ordering = ['-date']


class StudentRegistration(models.Model):
    registration = models.ForeignKey(
        Registration, related_name='student_registrations')
    student = models.ForeignKey(Student, related_name='registrations')
    program = models.ForeignKey(Program, related_name='student_registrations')
    status = models.CharField(
        max_length=32, choices=constants.REGISTRATION_STATUS_CHOICES, default='COMPLETE')

    objects = SemesterQuerySet.as_manager('program__sessions__date')

    def __str__(self):
        return '{} - {}'.format(self.program, self.student)

    class Meta:
        ordering = ['-registration__date']
