from django.core import serializers
from django.core.serializers.base import DeserializationError
from programs.models import Program
import json


def make_serializable(obj):
    return json.loads(json.dumps(obj, default=str))


class SessionCart:
    session_key = 'cart'

    def __init__(self, request):
        self._request = request

    def add(self, program, student, form_data):
        cart_items = self._request.session.get(self.session_key, [])

        serialized_student = serializers.serialize('json', [student])
        cart_items.append({
            'program_id': program.id,
            'student': serialized_student,
            'form_data': make_serializable(form_data)
        })

        self._request.session[self.session_key] = cart_items
        return cart_items

    def remove(self, index):
        session_cart = self._data
        del session_cart[int(index)]
        self._data = session_cart

    def clear(self):
        self._request.session['cart'] = []

    @property
    def total(self):
        return sum(item['program'].fee for item in self)

    @property
    def _data(self):
        return self._request.session.get(self.session_key, [])

    @_data.setter
    def _data(self, value):
        self._request.session[self.session_key] = value

    def __iter__(self):
        for item in self._data:
            try:
                deserialized_student = next(
                    serializers.deserialize('json', item['student']))
            except DeserializationError:
                continue
            yield {
                'program': Program.objects.get(pk=item['program_id']),
                'student': deserialized_student.object,
                'form_data': item['form_data']
            }

    def __len__(self):
        return len([i for i in self])
