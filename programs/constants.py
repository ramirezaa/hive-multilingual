from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy

ENROLLMENT_TYPES = (
    ('PUBLIC', 'Public Enrollment'),
    ('PRIVATE', 'Private Enrollment')
)

PAYMENT_METHODS = (
    ('ONLINE', _('Online')),
    #('INVOICE', _('Invoice')),
    ('ASSISTANCE', _('Financial Assistance'))
)

PAYMENT_FREQUENCIES = (
    ('ONE_TIME', _('One-time')),
    ('MONTHLY', _('Monthly'))
)

INCOME_CHOICES = ((n, n) for n in (
    _('Under $15,000'),
    '$15,000 - $25,000',
    '$25,000 - $35,000',
    '$35,000 - $45,000',
    '$45,000 - $55,000',
    _('Over $45,000')
))

PREVIOUS_ACTIVITIES_CHOICES = [(n, n) for n in (
    _("Code.org's Hour of Code"),
    _('Online tutorials, like Khan Academy, Code Academy, or Scratch'),
    _('Classes in school'),
    _('A previous Bold Idea program or workshop'),
    _('Workshops from another organization'),
)]

REFERRED_BY_CHOICES = [(n, n) for n in (
    _('Internet search'),
    _('Information Table'),
    _('School or Rec Center Activity/Program Guide'),
    _('Flyer'),
    _('Social Media'),
    _('Friend / Family'),
    _('Special Event'),
    _('Other'),
)]

REGISTRATION_STATUS_CHOICES = (
    ('COMPLETE', _('Complete')),
    ('PENDING', _('Pending')),
    ('CANCELED', _('Canceled'))
)

SHIRT_SIZE_CHOICES = (
    ('YSM', _('Youth Small')),
    ('YM', _('Youth Medium')),
    ('YL', _('Youth Large')),
    ('YXL', _('Youth XL')),
    ('SM', _('Adult Small')),
    ('M', _('Adult Medium')),
    ('L', _('Adult Large')),
    ('XL', _('Adult XL')),
    ('XXL', _('Adult XXL'))
)

RACE_CHOICES = (
    ('NATIVE', _('American Indian or Alaskan Native')),
    ('ASIAN', _('Asian')),
    ('BLACK', _('Black or African American')),
    ('PAC_ISLANDER', _('Hawaiian or Pacific Islander')),
    ('WHITE', _('White')),
    ('OTHER', _('Other')),
)

ETHNICITY_CHOICES = (
    ('HISPANIC', _('Hispanic or Latina/Latino')),
    ('NOT_HISPANIC', _('Not Hispanic or Latina/Latino')),
)

GENDER_CHOICES = (
    ('MALE', _('Male')),
    ('FEMALE', _('Female'))
)

MILITARY_STATUS_CHOICES = (
    ('ENLISTED', _('Enlisted in the US militiary guard/reserve')),
    ('VETERAN', _('US military veteran')),
)

#: The 48 contiguous states, plus the District of Columbia.
CONTIGUOUS_STATES = (
    ('AL', _('Alabama')),
    ('AZ', _('Arizona')),
    ('AR', _('Arkansas')),
    ('CA', _('California')),
    ('CO', _('Colorado')),
    ('CT', _('Connecticut')),
    ('DE', _('Delaware')),
    ('DC', _('District of Columbia')),
    ('FL', _('Florida')),
    ('GA', pgettext_lazy('US state', 'Georgia')),
    ('ID', _('Idaho')),
    ('IL', _('Illinois')),
    ('IN', _('Indiana')),
    ('IA', _('Iowa')),
    ('KS', _('Kansas')),
    ('KY', _('Kentucky')),
    ('LA', _('Louisiana')),
    ('ME', _('Maine')),
    ('MD', _('Maryland')),
    ('MA', _('Massachusetts')),
    ('MI', _('Michigan')),
    ('MN', _('Minnesota')),
    ('MS', _('Mississippi')),
    ('MO', _('Missouri')),
    ('MT', _('Montana')),
    ('NE', _('Nebraska')),
    ('NV', _('Nevada')),
    ('NH', _('New Hampshire')),
    ('NJ', _('New Jersey')),
    ('NM', _('New Mexico')),
    ('NY', _('New York')),
    ('NC', _('North Carolina')),
    ('ND', _('North Dakota')),
    ('OH', _('Ohio')),
    ('OK', _('Oklahoma')),
    ('OR', _('Oregon')),
    ('PA', _('Pennsylvania')),
    ('RI', _('Rhode Island')),
    ('SC', _('South Carolina')),
    ('SD', _('South Dakota')),
    ('TN', _('Tennessee')),
    ('TX', _('Texas')),
    ('UT', _('Utah')),
    ('VT', _('Vermont')),
    ('VA', _('Virginia')),
    ('WA', _('Washington')),
    ('WV', _('West Virginia')),
    ('WI', _('Wisconsin')),
    ('WY', _('Wyoming')),
)
