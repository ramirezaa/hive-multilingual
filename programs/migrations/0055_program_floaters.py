# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-05 21:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('volunteers', '0040_auto_20170830_1155'),
        ('programs', '0054_program_hide_on_volunteer_signup'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='floaters',
            field=models.ManyToManyField(blank=True, related_name='floater_programs', to='volunteers.Volunteer'),
        ),
    ]
