# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-22 22:50
from __future__ import unicode_literals

from django.db import migrations
from django.template.defaultfilters import slugify


def update_slugs(apps, schema_editor):
    ProgramCategory = apps.get_model('programs', 'ProgramCategory')
    for cat in ProgramCategory.objects.all():
        cat.slug = slugify(cat.name)
        cat.save()


class Migration(migrations.Migration):
    dependencies = [
        ('programs', '0010_auto_20161122_1650'),
    ]

    operations = [
        migrations.RunPython(update_slugs)
    ]
