# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-05 16:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0023_registration_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='program',
            name='type',
        ),
        migrations.AddField(
            model_name='program',
            name='enrollment_type',
            field=models.CharField(choices=[('OPEN', 'Open Enrollment'), ('CLOSED', 'Closed Enrollment')], default='OPEN', max_length=16),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='studentregistration',
            name='program',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='students', to='programs.Program'),
        ),
    ]
