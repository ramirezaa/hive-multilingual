# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-07-25 21:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0004_studentlogin'),
        ('volunteers', '0025_auto_20170725_1604'),
        ('programs', '0037_auto_20170715_2344'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='max_volunteers',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterUniqueTogether(
            name='studentattendance',
            unique_together=set([('student', 'session')]),
        ),
        migrations.AlterUniqueTogether(
            name='volunteerattendance',
            unique_together=set([('volunteer', 'session')]),
        ),
    ]
