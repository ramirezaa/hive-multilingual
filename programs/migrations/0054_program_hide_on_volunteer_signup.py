# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-30 17:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0053_auto_20170830_1149'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='hide_on_volunteer_signup',
            field=models.BooleanField(default=False),
        ),
    ]
