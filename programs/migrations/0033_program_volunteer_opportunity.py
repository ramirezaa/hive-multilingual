# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-06-21 22:44
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('volunteers', '0006_auto_20170621_1744'),
        ('programs', '0032_merge_20170621_1710'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='volunteer_opportunity',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='volunteers.VolunteerOpportunity'),
        ),
    ]
