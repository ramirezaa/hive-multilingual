# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-26 15:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0027_auto_20170126_0945'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='description',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
