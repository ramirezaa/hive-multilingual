# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-20 23:59
from __future__ import unicode_literals

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20161119_0904'),
        ('students', '0001_initial'),
        ('programs', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('invoice_number', models.CharField(blank=True, max_length=32)),
                ('payment_ref', models.CharField(blank=True, max_length=128)),
                ('financial_assistance_requested', models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True)),
                ('financial_assistance_granted', models.DecimalField(blank=True, decimal_places=2, max_digits=6, null=True)),
                ('form_data', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='accounts.Account')),
                ('program', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='programs.Program')),
                ('students', models.ManyToManyField(to='students.Student')),
            ],
        ),
        migrations.RemoveField(
            model_name='studentregistration',
            name='account',
        ),
        migrations.RemoveField(
            model_name='studentregistration',
            name='program',
        ),
        migrations.RemoveField(
            model_name='studentregistration',
            name='student',
        ),
        migrations.DeleteModel(
            name='StudentRegistration',
        ),
    ]
