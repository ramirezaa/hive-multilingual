# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-07-12 23:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0034_auto_20170706_1556'),
    ]

    operations = [
        migrations.AddField(
            model_name='session',
            name='type',
            field=models.CharField(choices=[('PROGRAM', 'Pogram session'), ('TRAINING', 'Volunteer training')], default='PROGRAM', max_length=32),
        ),
    ]
