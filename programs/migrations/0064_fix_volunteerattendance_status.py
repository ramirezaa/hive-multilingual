# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-01-19 20:32
from __future__ import unicode_literals

from django.db import migrations


def forward(apps, schema_editor):
    VolunteerAttendance = apps.get_model('programs', 'VolunteerAttendance')
    for attendance in VolunteerAttendance.objects.filter(status='A'):
        attendance.status = 'ABSENT'
        attendance.save()


def backward(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0063_populate_start_date'),
    ]

    operations = [
        migrations.RunPython(forward, backward)
    ]
