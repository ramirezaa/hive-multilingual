# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-05-25 20:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0074_program_volunteers'),
    ]

    operations = [
        migrations.AlterField(
            model_name='program',
            name='volunteer_opportunity',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='volunteers.VolunteerOpportunity'),
        ),
    ]
