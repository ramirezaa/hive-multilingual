# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-07-06 20:56
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0033_program_volunteer_opportunity'),
    ]

    operations = [
        migrations.AlterField(
            model_name='program',
            name='volunteer_opportunity',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='programs', to='volunteers.VolunteerOpportunity'),
        ),
    ]
