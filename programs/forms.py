from collections import OrderedDict
from django import forms
from django.utils.translation import gettext_lazy as _
from students.models import Student
from accounts.models import AccountContact
from hive.forms import BaseForm
from users import forms as user_forms
from . import constants
from . import utils


class UserCreationForm(BaseForm, user_forms.UserCreationForm):
    pass


class StudentForm(BaseForm, forms.ModelForm):
    birth_date = forms.DateField(label=_('Birth date (mm/dd/yyyy)'))
    grade_level = forms.ChoiceField(label=_('Grade level'))
    shirt_size = forms.ChoiceField(label=_('Shirt size'), choices=[('', '---------')] + list(constants.SHIRT_SIZE_CHOICES))
    previous_activities = forms.CharField(widget=forms.Textarea, required=False)
    possible_conflicts = forms.CharField(widget=forms.Textarea, required=False)
    other_info = forms.CharField(widget=forms.Textarea, required=False)
    race = forms.ChoiceField(label=_("Race"), choices=[('', '---------')] +list(constants.RACE_CHOICES))
    ethnicity = forms.ChoiceField(label=_("Ethnicity"), choices=[('', '---------')] +list(constants.ETHNICITY_CHOICES))
    gender = forms.ChoiceField(label=_("Gender"), choices=[('', '---------')] +list(constants.GENDER_CHOICES))

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        grade_label = _('Grade level ({year} school year)').format(
            year=utils.get_current_school_year_description())
        grade_choices = [('', '---------')] + utils.get_grade_choices()
        self.fields['grade_level'].label = grade_label
        self.fields['grade_level'].choices = grade_choices
        self.update_fields()

    class Meta:
        model = Student
        fields = [
            'first_name', 'last_name', 'birth_date', 'grade_level',
            'school_name', 'shirt_size', 'race', 'ethnicity', 'gender'
        ]


class AccountContactForm(BaseForm, forms.ModelForm):
    military_status = forms.ChoiceField(label=_('Military Status'), choices=[('', '---------')] +list(constants.MILITARY_STATUS_CHOICES))
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['phone'].required = True

    class Meta:
        model = AccountContact
        fields = [
            'first_name', 'last_name', 'address1', 'address2', 'city', 'state',
            'postal_code', 'email', 'phone', 'alt_phone', 'company',
            'military_status', 'relationship', 'legal_guardian',
            'preferred_language', 'emergency_contact', 'can_volunteer'
        ]


class RegistrationForm(BaseForm, forms.Form):
    payment_method = forms.ChoiceField(widget=forms.RadioSelect, choices=(
        ('ONE_TIME', _('Pay in full (${amount}) using credit card')),
        ('MONTHLY', _('Pay in {num} monthly payments (${amount}/month) using credit card')),
        ('ASSISTANCE', _('I would like to apply for financial assistance'))
    ))
    accept_terms = forms.BooleanField(required=True)
    accept_attendance_agreement = forms.BooleanField(required=True)
    referred_by = forms.ChoiceField(
        label=_('How did you hear about us?'),
        widget=forms.RadioSelect, required=False,
        choices=constants.REFERRED_BY_CHOICES)

    def __init__(self, cart, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if cart.total == 0:
            self.fields['payment_method'].required = False
            return

        self.fields['payment_method'].required = True

        # Put choices into an ordered dict so we can format the labels
        choices = OrderedDict(self.fields['payment_method'].choices)
        choices['ONE_TIME'] = choices['ONE_TIME'].format(amount=cart.total)

        # FIXME: Right now we assume all items in cart are homogenous with
        # regard to whether or not monthly payments are allowed (for example, we
        # allow monthly for ideaSpark, but a summer camp would likely be a
        # one-time payment). We may need a different solution if the cart
        # contains mixed registrations where some allow monthly and others don't
        self.num_monthly_payments = 1
        if any(item['program'].allow_monthly_payments for item in cart):
            self.num_monthly_payments = max(
                item['program'].num_monthly_payments or 1 for item in cart)
            self.monthly_price = cart.total / self.num_monthly_payments
            choices['MONTHLY'] = choices['MONTHLY'].format(
                num=self.num_monthly_payments, amount=self.monthly_price)

        # re-assign the update choice labels
        self.fields['payment_method'].choices = choices.items()


class FinancialAssistanceForm(BaseForm, forms.Form):
    total_family_income = forms.ChoiceField(choices=constants.INCOME_CHOICES)
    num_people_supported_by_income = forms.IntegerField(
        widget=forms.NumberInput)
    num_dependent_children = forms.IntegerField(widget=forms.NumberInput)
    extraordinary_circumstances = forms.CharField(
        widget=forms.Textarea, required=False)
    amount_requested = forms.DecimalField(max_digits=6, decimal_places=2)
    received_previously = forms.ChoiceField(
        widget=forms.RadioSelect,
        choices=(('No', _('No')), ('Yes', _('Yes'))))


class InvoicePaymentForm(BaseForm, forms.Form):
    payment_amount = forms.DecimalField(max_digits=6, decimal_places=2)
