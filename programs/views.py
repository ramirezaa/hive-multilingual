import json
from datetime import date
from django import http
from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import login_required
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.db import transaction
from django.forms import modelformset_factory
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from accounts.models import Account, AccountContact
from students.models import Student
from programs import models
from billing import payments
from billing import invoicing
from billing import BillingError, BillingValidationError
from hive.utils import json_defaults
from . import forms, utils
from .cart import SessionCart


def get_program_or_404(program_code):
    programs = models.Program.objects.open_for_registration()
    try:
        return programs.get(program_code=program_code)
    except models.Program.DoesNotExist:
        raise Http404


def get_account(request):
    if request.user and request.user.is_authenticated():
        try:
            return Account.objects.get(user=request.user)
        except Account.DoesNotExist:
            pass
    return None


@transaction.atomic
def create_account(request):
    form = forms.UserCreationForm()
    next = request.GET.get('next') or reverse('programs-index')
    if request.method == 'POST':
        next = request.POST.get('next') or reverse('programs-index')
        form = forms.UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            Account.objects.create(user=user)
            user = authenticate(
                email=form.cleaned_data['email'],
                password=form.cleaned_data['password1'])
            login(request, user)
            return HttpResponseRedirect(next)

    context = {
        'form': form,
        'next': next
    }

    return render(request, 'programs/create_account.html', context)


def show_private(request):
    if request.session.get('private'):
        return True

    if request.GET.get('private'):
        request.session['private'] = 1
        return True


def index(request):
    # Only show open enrollment locations
    #
    programs = models.Program.objects.open_for_registration()
    if not show_private(request):
        programs = programs.filter(enrollment_type='PUBLIC')

    categories = models.ProgramCategory.objects.filter(programs__in=programs)
    context = {
        'categories': categories.distinct().order_by('name')
    }

    return render(request, 'programs/index.html', context)


def category_index(request, category):
    category = get_object_or_404(models.ProgramCategory, slug=category)
    # Only show open-enrollment locations
    if request.GET.get('preview'):
        as_of = request.GET['preview']
    else:
        as_of = date.today()

    programs = models.Program.objects.open_for_registration(as_of)
    if not show_private(request):
        programs = programs.filter(category=category, enrollment_type='PUBLIC')

    context = {
        'category': category,
        'programs': programs.order_by('start_date')
    }
    return render(request, 'programs/category_index.html', context)


def view_cart(request):
    cart = SessionCart(request)
    if request.GET.get('remove'):
        remove_index = int(request.GET['remove']) - 1
        cart.remove(remove_index)
        return HttpResponseRedirect('.')

    # get a list of closed-enrollment programs in cart
    closed_programs = models.Program.objects.filter(
        pk__in=set(item['program'].pk for item in cart),
        enrollment_type='CLOSED')
    context = {
        'closed_programs': closed_programs,
        'cart': cart
    }

    return render(request, 'programs/cart.html', context)


def view_program(request, program_code, **kwargs):
    program = get_object_or_404(models.Program, program_code=program_code)
    return HttpResponseRedirect(program.add_to_cart_url)


@login_required(login_url=settings.LOGIN_URL)
def cart_add(request, program_code, **kwargs):
    program = get_object_or_404(models.Program, program_code=program_code)
    account = get_account(request)

    allow_overflow = request.GET.get('allow_overflow') or request.POST.get('allow_overflow')

    # if account exists, show screen to select existing student to enroll
    student = None
    student_id = request.GET.get('student') or request.POST.get('student')
    initial = {}
    account_students = None

    if student_id and student_id != 'new':
        student = Student.objects.get(
            account=account, pk=student_id)
        initial = {'grade_level': student.grade}
    elif account and account.students.count() > 0:
        account_students = account.students.all()

    if request.method == 'POST':
        form = forms.StudentForm(request.POST, instance=student)
        if form.is_valid():
            cart = SessionCart(request)
            student = form.save(commit=False)
            form_data = form.cleaned_data
            cart.add(program, student, form_data)
            return HttpResponseRedirect(reverse('programs-view-cart'))
    else:
        form = forms.StudentForm(instance=student, initial=initial)

    context = {
        'program': program,
        'allow_overflow': allow_overflow,
        'account': account,
        'account_students': account_students,
        'form': form,
        'student_id': student_id
    }
    return render(request, 'programs/cart_add.html', context)


@login_required
def checkout(request):
    account = get_account(request)
    cart = SessionCart(request)

    if len(cart) == 0:
        # Don't allow checkout if cart is empty
        return HttpResponseRedirect(reverse('programs-view-cart'))

    post_data = None
    if request.method == 'POST':
        post_data = request.POST

    contacts = AccountContact.objects.none()
    contacts_initial = []
    if account and account.contacts.count() > 0:
        contacts = account.contacts.all()
    else:
        contacts_initial = [{'email': request.user.email}]

    ContactFormSet = modelformset_factory(
        AccountContact, form=forms.AccountContactForm,
        extra=len(contacts_initial))

    # make online/one-time the default payment method
    initial = {'payment_method': 'ONE_TIME'}

    registration_form = forms.RegistrationForm(cart, post_data)
    contacts_formset = ContactFormSet(
        post_data, queryset=contacts, initial=contacts_initial)
    financial_assistance_form = forms.FinancialAssistanceForm(post_data)

    error = None
    validation_errors = {}
    billing_error = None
    created_invoice = None

    if request.method == 'POST':
        # use ref to determine if a transaction was created
        payment_transaction = None
        try:
            with transaction.atomic():
                registration = process_registration_forms(
                    request,
                    account=account,
                    contacts_formset=contacts_formset,
                    registration_form=registration_form,
                    financial_assistance_form=financial_assistance_form
                )
                if registration.invoice_total > 0:
                    created_invoice = invoicing.create_invoice(registration)
                    if registration.payment_method == 'ONLINE':
                        payment_transaction = payments.process_payment(request, registration)
        except ValidationError as e:
            error = 'VALIDATION'
            validation_errors = e.errors
        except (BillingError, BillingValidationError) as e:
            error = 'BILLING'
            billing_error = str(e)
        else:
            cart.clear()

            # Send emails
            utils.send_registration_confirmation(registration)
            utils.send_registration_notification(registration)

            # redirect to success url
            success_url = reverse(
                'programs-checkout-complete',
                kwargs={'regid': registration.pk}
            )

            # unset payment_transaction and created_invoice so they don't get
            # voided in cleanup
            payment_transaction = None
            created_invoice = None

            return HttpResponseRedirect(success_url)
        finally:
            # NOTE: This will be always be run, even after returning above
            if payment_transaction:
                # A transaction was created that needs to be voided.
                payments.cancel_transaction(payment_transaction)
            if created_invoice:
                # An invoice was created but needs to be deleted
                invoicing.void_invoice(created_invoice['InvoiceNumber'])

    else:
        registration_form = forms.RegistrationForm(cart, initial=initial)
        contacts_formset = ContactFormSet(
            queryset=contacts, initial=contacts_initial)

    context = {
        'settings': settings,
        'error': error,
        'validation_errors': json.dumps(validation_errors, indent=2),
        'billing_error': billing_error,
        'cart': cart,
        'account': get_account(request),
        'contacts_formset': contacts_formset,
        'registration_form': registration_form,
        'financial_assistance_form': financial_assistance_form
    }
    return render(request, 'programs/checkout.html', context)


def process_registration_forms(request, **kwargs):
    account = kwargs.get('account')
    cart = SessionCart(request)
    contacts_formset = kwargs.get('contacts_formset')
    registration_form = kwargs.get('registration_form')
    financial_assistance_form = kwargs.get('financial_assistance_form')

    errors = {}
    if not contacts_formset.is_valid():
        errors['contacts'] = contacts_formset.errors
    if not registration_form.is_valid():
        errors['registration'] = registration_form.errors

    # validate financial assistance ifneeded
    registration_data = registration_form.data

    # payment_method on the form has simplified choices of ONE_TIME, MONTHLY, and ASSISTANCE which
    # need to be broken down into the format we use for the model.
    payment_method = None
    payment_freq = None

    if registration_data.get('payment_method') == 'ASSISTANCE':
        payment_method = 'ASSISTANCE'
    else:
        payment_method = 'ONLINE'
        if registration_data.get('payment_method') == 'ONE_TIME':
            payment_freq = 'MONTHLY'
        else:
            payment_freq = 'ONE_TIME'

    if (payment_method == 'ASSISTANCE' and not financial_assistance_form.is_valid()):
        errors['assistance'] = financial_assistance_form.errors

    if errors:
        e = ValidationError('')
        e.errors = errors
        raise e

    reg_data = registration_form.cleaned_data

    form_data = {
        'registration': reg_data,
        'contacts': contacts_formset.cleaned_data,
        'students': [item['form_data'] for item in cart]
    }

    if payment_method == 'ASSISTANCE':
        form_data['financial_assistance'] = (
            financial_assistance_form.cleaned_data)

    if not account:
        account = Account.objects.create(user=request.user)

    # add/update account contacts
    contacts = contacts_formset.save(commit=False)
    for contact in contacts:
        contact.account = account
        contact.save()
        if not account.default_contact:
            account.default_contact = contact
            account.save()

    # create registration
    registration = models.Registration(
        account=account,
        payment_method=payment_method,
        form_data=form_data
    )

    # serialize form_data
    form_data = json.loads(json.dumps(form_data, default=json_defaults))
    registration.form_data = form_data

    if not registration.payment_method:
        registration.payment_method = ''

    if payment_method == 'ONLINE':
        registration.payment_frequency = payment_freq
        if payment_freq == 'MONTHLY':
            registration.num_payments = registration_form.num_monthly_payments

    if payment_method == 'ASSISTANCE':
        fa_data = financial_assistance_form.cleaned_data
        registration.status = 'PENDING'
        registration.financial_assistance_requested = (fa_data['amount_requested'])

    registration.save()

    # add students to registration
    for item in cart:
        student = item['student']
        grade_level = item['form_data'].get('grade_level')
        if grade_level:
            student.grade = int(grade_level)
        student.account = account
        student.save()
        student_registration = models.StudentRegistration(
            registration=registration,
            student=student,
            program=item['program'])
        student_registration.save()

    return registration


def checkout_complete(request, regid, **kwargs):
    context = {
        'registration': get_object_or_404(models.Registration, pk=regid)
    }
    return render(request, 'programs/checkout_complete.html', context)


def program_schedule(request, program_code, **kwargs):
    program = get_object_or_404(models.Program, program_code=program_code)
    tpl = 'programs/program_schedule.html'
    if request.GET.get('popup'):
        tpl = 'programs/program_schedule_popup.html'
    return render(request, tpl, {
        'program': program,
        'sessions': program.sessions.order_by('date')
    })


@login_required(login_url=settings.LOGIN_URL)
def my_account(request):
    account = get_account(request)
    if not account:
        raise Http404('Account not found')

    xero_contact = invoicing.get_contact(account)
    invoices = []
    total_outstanding = 0
    total_overdue = 0

    if xero_contact:
        invoices = invoicing.get_invoices(xero_contact['ContactID'])
        invoices = [i for i in invoices if i['Status'] != 'VOIDED']

        if xero_contact.get('Balances'):
            receivables = xero_contact['Balances']['AccountsReceivable']
            total_outstanding = receivables['Outstanding']
            total_overdue = receivables['Overdue']

    return render(request, 'programs/my_account.html', {
        'account': account,
        'total_outstanding': total_outstanding,
        'total_overdue': total_overdue,
        'invoices': invoices
    })


def view_invoice(request, invoice_id):
    account = get_account(request)
    invoice = invoicing.get_invoice_by_id(invoice_id)

    error = None
    billing_error = None
    validation_errors = None

    if request.method == 'POST':
        form = forms.InvoicePaymentForm(request.POST)
        if form.is_valid():
            try:
                invoicing.submit_payment(
                    request, invoice, form.cleaned_data['payment_amount'])
            except (BillingError, BillingValidationError) as e:
                error = 'BILLING'
                billing_error = str(e)
            else:
                messages.add_message(
                    request, messages.INFO,
                    'Thank you! Your payment is being processed. Please allow 2-3 business days '
                    'for your payment to be posted to your account.')

                if account:
                    return HttpResponseRedirect(reverse('programs-my-account'))
                else:
                    return HttpResponseRedirect(reverse('programs-index'))

        else:
            error = 'VALIDATION'
            validation_errors = form.errors

    else:
        initial = {'payment_amount': invoice['AmountDue']}
        form = forms.InvoicePaymentForm(initial=initial)

    return render(request, 'programs/view_invoice.html', {
        'account': account,
        'invoice': invoice,
        'form': form,
        'error': error,
        'billing_error': billing_error,
        'validation_errors': validation_errors,
        'settings': settings
    })


def view_invoice_pdf(request, invoice_id):
    invoice = invoicing.get_invoice_pdf(invoice_id)
    response = http.HttpResponse(invoice, content_type='application/pdf')
    if request.GET.get('download'):
        response['Content-Disposition'] = 'attachment; filename="invoice.pdf"'
    return response
