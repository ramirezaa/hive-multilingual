import re
from datetime import date

from django.db import models
from django.db.models import (
    Case, CharField, Max, Count, Q, Value, When, F, Subquery, OuterRef, ExpressionWrapper)
from django.db.models.functions import ExtractYear
from django.utils import timezone


class SemesterQuerySetMixin:
    semester_date_lookups = ['date']

    def with_semester(self):
        """
        Returns a QuerySet with year and semester
        """
        if len(self.semester_date_lookups) > 1:
            raise NotImplemented('Cannot use with_semester() with more than one lookup')
        lookup = self.semester_date_lookups[0]
        filter = {}
        if 'sessions' in lookup:
            # get the lookup up to sessions so we can filter only program
            # sessions
            sessions_lookup = re.sub(r'^(.*(__)?sessions).*$', r'\1', lookup)
            filter[sessions_lookup + '__type'] = 'PROGRAM'

        lookup_month_gte = lookup + '__month__gte'
        lookup_month_lte = lookup + '__month__lte'

        return self.filter(**filter).annotate(
            year=ExtractYear(lookup),
            semester=Case(
                When(Q(**{lookup_month_gte: 1, lookup_month_lte: 5}),
                     then=Value('SPRING')),
                When(Q(**{lookup_month_gte: 6, lookup_month_lte: 8}),
                     then=Value('SUMMER')),
                When(Q(**{lookup_month_gte: 9, lookup_month_lte: 12}),
                     then=Value('FALL')),
                output_field=CharField()
            )
        ).distinct()

    def for_year(self, year, fiscal=False):
        """
        Filter results for a specific year
        """
        q = Q()
        year = int(year)
        for lookup in self.semester_date_lookups:
            filter = {}
            if 'sessions' in lookup:
                # get the lookup up to sessions so we can filter only program
                # sessions
                sessions_lookup = re.sub(r'^(.*(__)?sessions).*$', r'\1', lookup)
                filter[sessions_lookup + '__type'] = 'PROGRAM'

            if fiscal:
                filter[lookup + '__gte'] = date(year-1, 7, 1)
                filter[lookup + '__lt'] = date(year, 7, 1)

            q = q | Q(**filter)

        return self.filter(q).distinct()

    def for_semester(self, year, semester):
        """
        Filter results for a specific year and semester where `semester` is one
        of "FALL", "SUMMER", or "SPRING"
        """
        q = Q()
        year = int(year)
        for lookup in self.semester_date_lookups:
            filter = {}
            if 'sessions' in lookup:
                # get the lookup up to sessions so we can filter only program
                # sessions
                sessions_lookup = re.sub(r'^(.*(__)?sessions).*$', r'\1', lookup)
                filter[sessions_lookup + '__type'] = 'PROGRAM'

            if semester == 'SPRING':
                filter[lookup + '__gte'] = date(year, 1, 1)
                filter[lookup + '__lt'] = date(year, 6, 1)
            elif semester == 'SUMMER':
                filter[lookup + '__gte'] = date(year, 6, 1)
                filter[lookup + '__lt'] = date(year, 9, 1)
            elif semester == 'FALL':
                filter[lookup + '__gte'] = date(year, 9, 1)
                filter[lookup + '__lt'] = date(year+1, 1, 1)
            else:
                raise ValueError(
                    "semester must be one of 'FALL', 'SUMMER', or 'SPRING'")
            q = q | Q(**filter)

        return self.filter(q).distinct()


class SemesterQuerySet(SemesterQuerySetMixin, models.QuerySet):
    @classmethod
    def as_manager(cls, *date_lookups):
        cls = type(cls.__name__, (cls,), {'semester_date_lookups': date_lookups})
        return super(SemesterQuerySet, cls).as_manager()
    as_manager.queryset_only = True


class ProgramQuerySet(SemesterQuerySetMixin, models.QuerySet):
    semester_date_lookups = ['start_date']

    def current(self):
        today = timezone.now().date()
        return self.annotate(
            end_date=Max('sessions__date')
        ).filter(start_date__lte=today, end_date__gte=today)

    def open_for_registration(self, as_of=None):
        if not as_of:
            as_of = timezone.now()
        return self.filter(
            Q(registration_close_date__isnull=True) |
            Q(registration_close_date__gte=as_of),
            registration_open_date__isnull=False,
            registration_open_date__lte=as_of,
        )

    def with_remaining_volunteers_needed(self):
        """
        Adds `remaining_volunteers_needed` to the queryset.

        This is a heuristic used for sorting programs based on the highest need. Historically, this
        was calculated based on `max_volunteers` and number of volunteers signed up for the program
        site as a whole. Since volunteers select their attendance ahead of time, site coverage can
        be uneven from session-to-session.

        This is now calculated by finding the individual session with the highest need, and
        returning the number of volunteers needed to cover that session.
        """
        from programs.models import Session

        # Build a subquery to get the session with the least number of signups
        # https://bit.ly/2J1plbD
        sessions_query = (
            Session.objects
            .filter(program=OuterRef('id'))
            .annotate(num_signed_up=Count('volunteer_attendances'))
            .order_by('num_signed_up')
            .values('num_signed_up')
        )
        lowest_count = Subquery(sessions_query[:1], output_field=models.IntegerField())

        volunteers_per_session = ExpressionWrapper(
            F('num_student_seats') / F('student_volunteer_ratio'),
            output_field=models.IntegerField())

        qs = self.annotate(
            remaining_volunteers_needed=(volunteers_per_session - lowest_count)
        )
        return qs


class PastFutureQuerySetMixin:
    date_lookup = 'date'

    def past(self):
        lookup = self.date_lookup + '__lt'
        return self.filter(**{lookup: timezone.now()})

    def future(self):
        lookup = self.date_lookup + '__gte'
        return self.filter(**{lookup: timezone.now()})


class AttendanceQuerySet(PastFutureQuerySetMixin, SemesterQuerySet):
    date_lookup = 'session__date'
