from django.conf import settings
from django.core.mail import send_mail
from django.template import loader
from django.utils.translation import get_language
from django.utils import timezone
from hive import utils


def get_semester(date):
    if date.month <= 5:
        return 'SPRING'
    if date.month >= 6 and date.month <= 8:
        return 'SUMMER'
    if date.month >= 9:
        return 'FALL'


def get_grade_choices():
    GRADE_CHOICES = []
    if get_language() == 'es':
        GRADE_CHOICES = [(n, (str(n)+'.º')) for n in range(1, 13)]
    else:
        GRADE_CHOICES = [(n, utils.ordinal(n)) for n in range(1, 13)]

    return GRADE_CHOICES


def grammatical_join(items):
    items = list(items)
    return ', '.join(items[:-2] + [' and '.join(items[-2:])])


def get_current_school_year(as_of=None):
    """
    Returns the current school year assuming the year ends May 31
    """
    today = as_of or timezone.now()
    if today.month < 6:
        return today.year - 1
    return today.year


def get_current_school_year_description(as_of=None):
    year = get_current_school_year(as_of)
    return '{} - {}'.format(year, year + 1)


def send_registration_confirmation(registration):
    subject = 'Your registration has been received'
    student_names = grammatical_join(s.first_name for s in registration.students.distinct())
    program_names = grammatical_join(p.category.name for p in registration.programs.distinct())
    current_school_year = get_current_school_year_description()

    registration_msg = loader.get_template(
        'programs/emails/registration_email.txt').render({
            'contact': registration.account.default_contact,
            'current_school_year': current_school_year,
            'student_names': student_names,
            'program_names': program_names,
            'registration': registration
        })
    send_mail(subject, registration_msg, settings.DEFAULT_FROM_EMAIL,
              [registration.account.user.email])


def send_registration_notification(registration):
    notification_subj = 'A registration has been submitted'
    notification_msg = loader.get_template(
        'programs/emails/registration_notification.txt').render({
            'registration': registration
        })
    send_mail(notification_subj, notification_msg,
              settings.DEFAULT_FROM_EMAIL,
              settings.REGISTRATION_NOTIFICATION_RECIPIENTS)
