(function($) {
  function getFormsetControl($formset) {
    var prefix = $formset.data('prefix');
    return {
      totalForms: $('#id_' + prefix + '-TOTAL_FORMS'),
      initialForms: $('#id_' + prefix + '-INITIAL_FORMS'),
      minNumForms: $('#id_' + prefix + '-MIN_NUM_FORMS'),
      maxNumForms: $('#id_' + prefix + '-MAX_NUM_FORMS')
    };
  }

  function updateFormsetHeadings($formset) {
    var $headings = $formset.find('.formset-heading').not('.hidden .formset-heading');
    $headings.each(function(n, heading) {
      var $heading = $(heading);
      $heading.find('.counter').text(n + 1);
      $heading.find('.initial').toggleClass('hidden', n > 0);
      $heading.find('.subsequent').toggleClass('hidden', n == 0);
    });
  }

  function initDatePickers() {
    // Datepicker
    $('.datepicker:visible').not('.datepicker-initialized').pickadate({
      selectMonths: true,
      selectYears: 80,
      format: 'mm/dd/yyyy'
    }).addClass('datepicker-initialized');
    // fix dom order of datepicker to allow for data-error message css
    $('.input-field .picker').each(function() {
      $label = $(this).siblings('label');
      $label.insertBefore(this);
    });
  }

  $(document).ready(function() {
    initDatePickers();
    Materialize.updateTextFields();

    // Billing address selector
    $('#billingAddressSelector').on('change', function() {
      if (this.value !== 'OTHER') {
        var address = addresses[parseInt(this.value)];
        $('#id_billing_address1').val(address.address1);
        $('#id_billing_address2').val(address.address2);
        $('#id_billing_city').val(address.city);
        $('#id_billing_state').val(address.state);
        $('#id_billing_zip').val(address.zip);
      } else {
        $('#id_billing_address1').val();
        $('#id_billing_address2').val();
        $('#id_billing_city').val();
        $('#id_billing_state').val();
        $('#id_billing_zip').val();
      }
      $('.billing-address').toggle(this.value == 'OTHER');
    }).change();


    // Formset add button
    $('.formset-add .btn').on('click', function(event) {
      var $formset = $(this).parents('.formset').first();
      var $form = $formset.parents('form').first();
      var $tpl = $formset.children('.hidden').first();
      var control = getFormsetControl($formset);
      var formCount = parseInt(control.totalForms.val());

      var maxNumForms = parseInt(control.maxNumForms.val());
      if (maxNumForms === formCount) {
        alert('Maximum number reached');
        return;
      }

      // clone the hidden empty form
      var $newForm = $tpl.clone();

      // replace __prefix__ with form count
      var $fieldElements = $newForm.find(
        '[id*="__prefix__"], [name*="__prefix__"], [for*="__prefix__"]');
      $fieldElements.each(function(n, el) {
        $.each(el.attributes, function(n, attr) {
          $(el).attr(attr.name, attr.value.replace('__prefix__', formCount));
        });
      });

      // insert the cloned form at the end of the formset
      var $lastForm = $formset.find('.formset-form').last();
      $newForm.insertAfter($lastForm);

      // update formset control value
      control.totalForms.val(formCount + 1);

      // show the form and initialize material UI
      $newForm.removeClass('hidden');
      updateFormsetHeadings($formset);
      Materialize.updateTextFields();
      $newForm.find('select').material_select();
      initDatePickers();
    });

    // Formset delete button
    $('.formset').on('click', '.formset-form .remove-btn', function() {
      var $formset = $(this).parents('.formset');
      var control = getFormsetControl($formset);
      var formCount = parseInt(control.totalForms.val());
      $(this).parents('.formset-form').first().remove();
      control.totalForms.val(formCount - 1);
      updateFormsetHeadings($formset);
    });

    // Payment method select
    $('.payment-method input[type="radio"]').on('change', function() {
      if (this.checked) {
        $('.charge-method').toggleClass('hidden', this.value == 'ASSISTANCE');
        $('.financial-assistance').toggleClass('hidden', this.value != 'ASSISTANCE');
        $('.financial-assistance select').material_select();
      }
    });

    $('.payment-method input[type="radio"]:checked').change();
  });

  // for debugging / prefilling forms
  window.updateMaterial = function() {
    $('select').material_select();
    Materialize.updateTextFields();
  }

})(jQuery);
