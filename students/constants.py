STUDENT_LOGIN_SERVICE_CHOICES = (
    ('GOOGLE', 'Google'),
    ('SCRATCH', 'Scratch'),
    ('PENCILCODE', 'PencilCode')
)

SHIRT_SIZE_CHOICES = (
    ('YSM', 'Youth Small'),
    ('YM', 'Youth Medium'),
    ('YL', 'Youth Large'),
    ('YXL', 'Youth XL'),
    ('SM', 'Adult Small'),
    ('M', 'Adult Medium'),
    ('L', 'Adult Large'),
    ('XL', 'Adult XL'),
    ('XXL', 'Adult XXL')
)
