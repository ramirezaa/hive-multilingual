from django import forms
from django.contrib import admin
from django.contrib import messages
from import_export import resources, fields as resource_fields
from import_export.admin import ExportActionModelAdmin

from hive.admin import register
from programs.admin import SemesterFilter as SemesterFilter
from students import models


class StudentResource(resources.ModelResource):
    name = resource_fields.Field()
    grade = resource_fields.Field()
    age = resource_fields.Field()
    guardian_name = resource_fields.Field()
    guardian_phone = resource_fields.Field()
    guardian_email = resource_fields.Field()
    address1 = resource_fields.Field()
    address2 = resource_fields.Field()
    city = resource_fields.Field()
    state = resource_fields.Field()
    postal_code = resource_fields.Field()
    other_info = resource_fields.Field()
    previous_experience = resource_fields.Field()
    location = resource_fields.Field()

    class Meta:
        model = models.Student
        fields = [
            'name', 'age', 'grade', 'birth_date', 'shirt_size', 'race', 'ethnicity', 'gender'
        ]

    def dehydrate_name(self, student):
        return student.name

    def dehydrate_age(self, student):
        return student.age

    def dehydrate_grade(self, student):
        return student.grade_name

    def dehydrate_guardian_name(self, student):
        return student.account.default_contact.name

    def dehydrate_guardian_phone(self, student):
        return student.account.default_contact.phone

    def dehydrate_guardian_email(self, student):
        return student.account.default_contact.email

    def dehydrate_previous_experience(self, student):
        if student.extra_data:
            return student.extra_data.get('previous_activities')

    def dehydrate_address1(self, student):
        return student.account.default_contact.address1

    def dehydrate_address2(self, student):
        return student.account.default_contact.address2

    def dehydrate_city(self, student):
        return student.account.default_contact.city

    def dehydrate_state(self, student):
        return student.account.default_contact.state

    def dehydrate_postal_code(self, student):
        return student.account.default_contact.postal_code

    def dehydrate_other_info(self, student):
        if student.extra_data:
            return student.extra_data.get('other_info')
        return ''

    def dehydrate_location(self, student):
        registrations = student.registrations.order_by('-registration__date')
        if registrations.count() > 0:
            return registrations[0].program.location
        return ''


# class StudentLoginInline(admin.TabularInline):
#     fields = ['service', 'username', 'password']


def generate_logins(service, modeladmin, request, queryset):
    for student in queryset:
        models.StudentLogin.objects.create(
            student=student,
            service=service
        )
    modeladmin.message_user(request, 'Successfully created {} {} logins'.format(
        queryset.count(), service.title()), messages.SUCCESS)


def generate_google_logins(modeladmin, request, queryset):
    generate_logins('GOOGLE', modeladmin, request, queryset)
generate_google_logins.short_description = 'Generate logins (Google)' # noqa


def generate_scratch_logins(modeladmin, request, queryset):
    generate_logins('SCRATCH', modeladmin, request, queryset)
generate_scratch_logins.short_description = 'Generate logins (Scratch)' # noqa


class NeedsLoginFilter(admin.SimpleListFilter):
    title = 'needs login'
    parameter_name = 'needs_login'

    def lookups(self, request, model_Admin):
        return (
            ('GOOGLE', 'Google'),
            ('SCRATCH', 'Scratch')
        )

    def queryset(self, request, queryset):
        service = self.value()
        if service is None:
            return queryset
        logins = models.StudentLogin.objects.filter(service=service)
        return queryset.exclude(logins__in=logins)


@register(models.Student)
class StudentAdmin(ExportActionModelAdmin):
    icon = '<i class="material-icons">face</i>'
    list_display = ['name', 'age', 'grade_name', 'guardian_name',
                    'guardian_phone']
    search_fields = ['first_name', 'last_name']
    list_filter = [
        SemesterFilter,
        'registrations__program__location',
        'registrations__program__category',
        NeedsLoginFilter
    ]
    resource_class = StudentResource
    actions = [
        generate_google_logins,
        generate_scratch_logins
    ]

    def guardian_name(self, obj):
        return obj.account.default_contact.name

    def guardian_phone(self, obj):
        return obj.account.default_contact.phone

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.select_related('account__default_contact')

    def lookup_allowed(self, *args, **kwargs):
        # Allow all lookups
        return True


class StudentLoginResource(resources.ModelResource):
    student_name = resource_fields.Field()
    student_age = resource_fields.Field()
    student_grade = resource_fields.Field()
    username = resource_fields.Field()
    password = resource_fields.Field()

    def dehydrate_student_name(self, login):
        return login.student.name

    def dehydrate_student_age(self, login):
        return login.student.age

    def dehydrate_student_grade(self, login):
        return login.student.grade

    def dehydrate_username(self, login):
        return login.username

    def dehydrate_password(self, login):
        return login.password


class StudentLoginAdminForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # allow blank username and password to be auto-generated by model
        self.fields['username'].required = False
        self.fields['username'].help_text = 'Leave blank to auto-generate'

        self.fields['password'].required = False
        self.fields['password'].help_text = 'Leave blank to auto-generate'

    class Meta:
        model = models.StudentLogin
        fields = '__all__'


@register(models.StudentLogin)
class StudentLogin(ExportActionModelAdmin):
    form = StudentLoginAdminForm
    icon = '<i class="material-icons">vpn_key</i>'
    list_display = ['student', 'service']
    search_fields = ['student__first_name', 'student__last_name']
    list_filter = [
        SemesterFilter,
        'student__registrations__program__location',
        'student__registrations__program__category',
        'service'
    ]
    resource_class = StudentLoginResource
