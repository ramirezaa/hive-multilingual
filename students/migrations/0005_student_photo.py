# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-07-26 21:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0004_studentlogin'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='photo',
            field=models.ImageField(blank=True, upload_to=''),
        ),
    ]
