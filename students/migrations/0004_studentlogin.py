# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-29 21:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0003_fix_other_info_20170125_1652'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentLogin',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateTimeField(auto_now_add=True)),
                ('service', models.CharField(choices=[('GOOGLE', 'Google'), ('SCRATCH', 'Scratch'), ('PENCILCODE', 'PencilCode')], max_length=255)),
                ('username', models.CharField(max_length=255, verbose_name='Username/email')),
                ('password', models.CharField(max_length=255, verbose_name='Password')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='students.Student')),
            ],
        ),
    ]
