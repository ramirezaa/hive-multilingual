from django.apps import AppConfig


class StudentsConfig(AppConfig):
    icon = '<i class="material-icons">face</i>'
    name = 'students'
