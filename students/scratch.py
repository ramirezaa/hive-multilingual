import re
import requests
import time


def strip(s):
    return re.sub(r'[^A-Za-z0-9]', '', s)


def find_available_username(first, last):
    first = strip(first).title()
    last = strip(last).title()

    username = first + last
    if len(username) > 20:
        username = first + last[0]

    counter = 0
    while counter < 50:
        # let's not pound the scratch servers
        counter += 1
        time.sleep(.5)

        r = requests.get('https://api.scratch.mit.edu/users/{}'.format(username))
        if r.status_code == 404:
            # username is available
            return username
        else:
            username += str(counter)
            if len(username) > 20:
                username = first + last[0] + str(counter)

    raise Exception('Could not find an available username for {} {}'.format(first, last))
