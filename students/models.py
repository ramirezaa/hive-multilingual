from datetime import date
from dateutil.relativedelta import relativedelta
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from hive import google
from hive.utils import ordinal
from contacts.models import Person, DemographicsModel
from accounts.models import Account
from users.models import User
from programs.query import SemesterQuerySet
from . import constants
from . import utils
from . import scratch


class Student(Person, DemographicsModel, models.Model):
    user = models.OneToOneField(User, related_name='student', null=True, blank=True)
    account = models.ForeignKey(Account, related_name='students')
    date_added = models.DateTimeField(default=timezone.now)
    date_modified = models.DateTimeField(default=timezone.now)
    birth_date = models.DateField(_('Birth date'), null=True, blank=True)
    school_name = models.CharField(_('School name'), max_length=255, blank=True)
    grade_offset = models.IntegerField(
        default=0, help_text=('By default, grade is determined based on age. '
                              'Use offset if student is ahead or behind of '
                              'typical grade for their age.'))
    shirt_size = models.CharField(
        _('Shirt size'), max_length=5, choices=constants.SHIRT_SIZE_CHOICES, blank=True)

    extra_data = JSONField(null=True, blank=True)

    programs = models.ManyToManyField('programs.Program', through='programs.StudentRegistration')

    objects = SemesterQuerySet.as_manager(
        'registrations__program__start_date')

    def save(self, *args, **kwargs):
        self.date_modified = timezone.now()
        super().save(*args, **kwargs)

    def grade_as_of(self, as_of_date):
        # We get grade dynamically so we don't have to change it each year
        if not self.birth_date:
            return None

        # A good approximation of school grade is to take the age of the
        # student at the start of the school year (usually around 9/1) and
        # subtract 5.
        if as_of_date.month < 9:
            last_september = date(as_of_date.year - 1, 9, 1)
        else:
            last_september = date(as_of_date.year, 9, 1)

        last_september_age = relativedelta(
            last_september, self.birth_date).years

        return last_september_age - 5 + self.grade_offset

    @property
    def grade(self):
        return self.grade_as_of(date.today())

    @grade.setter
    def grade(self, value):
        # Grade can be set, but it will actually change grade_offset.
        self.grade_offset = 0
        assumed_grade = self.grade
        self.grade_offset = int(value) - assumed_grade

    @property
    def grade_name(self):
        if not self.grade:
            return None

        if self.grade == 0:
            return 'K'
        if self.grade <= 12:
            return ordinal(self.grade)
        else:
            return 'Graduate'

    @property
    def age(self):
        if not self.birth_date:
            return None
        return relativedelta(date.today(), self.birth_date).years

    def get_absolute_url(self):
        return reverse('dashboard-student-details', kwargs={'student_id': self.id})

    def __str__(self):
        return '{0.first_name} {0.last_name}'.format(self)


class StudentLogin(models.Model):
    """
    The logins stored here are not used for authentication. This is primarily used to store the
    login info that is printed student folder labels.
    """
    date_added = models.DateTimeField(auto_now_add=True)
    student = models.ForeignKey(Student, related_name='logins')
    service = models.CharField(
        choices=constants.STUDENT_LOGIN_SERVICE_CHOICES,
        max_length=255)
    username = models.CharField('Username/email', max_length=255)
    # FIXME This password should be strongly encrypted, only mentors and staff can decrypt
    password = models.CharField('Password', max_length=255)

    objects = SemesterQuerySet.as_manager(
        'student__registrations__program__sessions__date')

    class Meta:
        unique_together = ['service', 'username']

    def save(self, *args, **kwargs):

        if self.service == 'GOOGLE':
            google.create_or_update_student_account(self)
            # associate a Hive user account with the student's google account
            if not self.student.user_id:
                self.student.user = User.objects.create(email=self.username)
            else:
                self.student.user.email = self.username
                self.student.user.save()

        if self.service == 'SCRATCH':
            self.username = scratch.find_available_username(
                self.student.first_name,
                self.student.last_name
            )
            self.password = utils.generate_kidsafe_password()

        super().save(*args, **kwargs)

    def __str__(self):
        return '[{}] {}'.format(self.service, self.student)
