from functools import update_wrapper
from django.contrib.admin.options import get_content_type_for_model
from django.template.response import TemplateResponse


class DetailViewMixin:
    def get_urls(self):
        from django.conf.urls import url

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        urlpatterns = [
            url(r'^(.+)/edit/$', wrap(self.changeform_view),
                name='%s_%s_change' % info),
        ] + super().get_urls()

        return urlpatterns

    def get_detail_context(self, request, obj, extra_context=None):
        return {
            'object': obj
        }

    def change_view(self, request, object_id, form_url='', extra_context=None):
        opts = self.model._meta
        app_label = opts.app_label
        obj = self.get_object(request, object_id)
        context = self.get_detail_context(request, obj, extra_context)
        context.update({
            'has_add_permission': self.has_add_permission(request),
            'has_change_permission': self.has_change_permission(request, obj),
            'has_delete_permission': self.has_delete_permission(request, obj),
            'opts': opts,
            'content_type_id': get_content_type_for_model(self.model).pk,
            'app_label': app_label,
            'change': True,
            'add': False,
            'is_popup': False,
            'save_as': False
        })
        return TemplateResponse(request, [
            "admin/%s/%s/detail.html" % (app_label, opts.model_name),
            "admin/%s/detail.html" % app_label,
            "admin/detail.html"
        ], context)
