from django import forms
from django.contrib import admin
from django.db.models import Q
from import_export import resources, fields as resource_fields
from import_export.admin import ExportActionModelAdmin

from . import models
from hive.admin import register
from programs.models import ProgramCategory, Location
from programs.admin import SemesterFilter


@register(models.VolunteerOpportunity)
class VolunteerOpportunityAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">announcement</i>'
    list_display = ['name', 'order', 'status']
    list_filter = ['status']
    prepopulated_fields = {'slug': ['name']}
    fields = [
        ('name', 'slug'), ('role', 'photo'), 'short_description', 'description_url',
        ('order', 'status'), 'programs', 'questionnaire'
    ]
    actions = ['publish_action', 'archive_action']

    def publish_action(self, request, queryset):
        updated = queryset.update(status='PUBLISHED')
        self.message_user(request, 'Published {} items'.format(updated))
    publish_action.short_description = 'Publish selected items'

    def archive_action(self, request, queryset):
        updated = queryset.update(status='ARCHIVED')
        self.message_user(request, 'archived {} items'.format(updated))
    archive_action.short_description = 'Archive selected items'


class VolunteerRequirementForm(forms.ModelForm):
    def has_changed(self):
        # Should return True if data differs from initial. By always returning
        # true even unchanged inlines will get validated and saved.
        return True

    class Meta:
        model = models.VolunteerRequirement
        fields = ['requirement_type', 'status', 'completed_date', 'notes']
        widgets = {
            'notes': forms.TextInput
        }


class VolunteerRequirementInline(admin.TabularInline):
    model = models.VolunteerRequirement
    form = VolunteerRequirementForm
    fields = ['requirement_type', 'status', 'completed_date', 'notes']
    extra = 0


class VolunteerProgramCategoryFilter(admin.SimpleListFilter):
    title = 'Program category'
    parameter_name = 'category_id'
    program_lookup = 'programs'

    def lookups(self, request, model_admin):
        return ((c.pk, c.name) for c in ProgramCategory.objects.all())

    def queryset(self, request, queryset):
        val = self.value()
        if val is None:
            return queryset
        filter = {
            self.program_lookup + '__category_id': self.value()
        }
        return queryset.filter(**filter).distinct()


class VolunteerProgramLocationFilter(admin.SimpleListFilter):
    title = 'Program location'
    parameter_name = 'location_id'
    program_lookup = 'programs'

    def lookups(self, request, model_admin):
        return ((l.pk, l.name) for l in Location.objects.all())

    def queryset(self, request, queryset):
        val = self.value()
        if val is None:
            return queryset
        filter = {
            self.program_lookup + '__location_id': self.value()
        }
        return queryset.filter(**filter).distinct()


class VolunteerResource(resources.ModelResource):
    age = resource_fields.Field()

    class Meta:
        model = models.Volunteer
        fields = [
            'first_name', 'last_name', 'full_legal_name', 'birth_date', 'age', 'shirt_size',
            'company', 'title', 'email', 'phone', 'alt_phone', 'address1', 'address2', 'city',
            'state', 'postal_code', 'race', 'ethnicity', 'gender'
        ]

    def dehydrate_age(self, obj):
        return obj.age


@register(models.Volunteer)
class VolunteerAdmin(ExportActionModelAdmin):
    icon = '<i class="material-icons">people</i>'
    list_display = ['__str__', 'email']
    search_fields = [
        'first_name', 'last_name', 'email'
    ]
    fields = ['first_name', 'last_name', 'email']
    readonly_fields = fields
    inlines = [VolunteerRequirementInline]
    resource_class = VolunteerResource

    list_filter = [
        'signups__opportunity',
        SemesterFilter,
        VolunteerProgramCategoryFilter,
        VolunteerProgramLocationFilter
    ]


class VolunteerSignupProgramCategoryFilter(VolunteerProgramCategoryFilter):
    program_lookup = 'assignments__program'


class VolunteerSignupProgramLocationFilter(VolunteerProgramLocationFilter):
    program_lookup = 'assignments__program'


@register(models.VolunteerSignup)
class VolunteerSignupAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">assignment_ind</i>'
    list_display = [
        'volunteer', 'opportunity', 'date'
    ]
    search_fields = [
        'volunteer__first_name', 'volunteer__last_name', 'volunteer__email'
    ]
    list_filter = [
        'opportunity',
        SemesterFilter,
        VolunteerSignupProgramCategoryFilter,
        VolunteerSignupProgramLocationFilter
    ]

    readonly_fields = ['volunteer', 'opportunity', 'form_data', 'date']


@register(models.TrainingSession)
class TrainingSessionAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">event</i>'
    list_display = ['name', 'date', '_time']
    list_filter = ['location']

    fields = [
        'name', 'description', 'location',
        ('date', 'start_time', 'end_time'),
        'opportunities', 'programs', 'facilitators'
    ]

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        q = ~Q(status='ARCHIVED')
        if obj:
            q |= Q(pk__in=obj.opportunities.all())
        opportunities = models.VolunteerOpportunity.objects.filter(q)
        form.base_fields['opportunities'].queryset = opportunities
        return form

    def _time(self, obj):
        return '{:%l:%M %P} - {:%l:%M %P}'.format(obj.start_time, obj.end_time)
    _time.short_description = 'Time'


@register(models.VolunteerRequirementType)
class VolunteerRequirementAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">done_all</i>'
    list_display = ['name', 'slug']
    prepopulated_fields = {'slug': ['name']}
