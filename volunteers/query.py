from django.db import models
from django.db.models import Q
from django.utils import timezone


class VolunteerRequirementQuerySet(models.QuerySet):
    def objects(self):
        return self.select_related('requirement_type')

    def pending(self):
        return (
            self.valid_and_current()
            .annotate(num_assignments=models.Count('assignments'))
            .filter(num_assignments__gt=0)
            .exclude(status='COMPLETED')
        )

    def valid_and_current(self, as_of=None):
        as_of = timezone.now().date()
        return self.exclude(status='INVALID').filter(
            Q(expire_date__isnull=True) | Q(expire_date__gt=as_of)
        )
