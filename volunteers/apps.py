from django.apps import AppConfig


class VolunteersConfig(AppConfig):
    icon = '<i class="material-icons">people</i>'
    name = 'volunteers'
