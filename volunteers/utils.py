from volunteers.models import VolunteerRequirement, VolunteerAssignment


def get_requirements(volunteer, programs):
    """
    Returns an array of `VolunteerRequirement` instances based on requirements of the given
    programs. If a `VolunteerRequirement` already exists and has not expired, it will be used
    instead of creating a new one.
    """

    # Keep track of requirements by type, starting with any existing unexpired requirements
    requirements = {r.requirement_type.id: r for r in volunteer.requirements.valid_and_current()}

    for program in programs:
        for requirement_type in program.volunteer_requirements.all():
            if requirement_type.id not in requirements:
                requirements[requirement_type.id] = VolunteerRequirement(
                    requirement_type=requirement_type,
                    volunteer=volunteer
                )

    return list(requirements.values())


def create_assignment(volunteer, program, role, requirements=None, signup=None):
    """
    Creates a new VolunteerAssignment.
    """
    assignment = VolunteerAssignment.objects.create(
        program=program,
        volunteer=volunteer,
        role=role,
        signup=signup
    )

    if requirements is None:
        requirements = get_requirements(volunteer, [program])

    # Add requirements specific to this assignment
    program_requirements = program.volunteer_requirements.all()
    assignment.requirements.set([
        r for r in requirements if r.requirement_type in program_requirements
    ])
