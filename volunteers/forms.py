from django import forms
from django.conf import settings

from hive.forms import BaseForm
from hive.utils import get_age
from hive.widgets import ProfilePhotoInput
from questionnaires.forms import QuestionnaireModelForm
from users import forms as user_forms
from volunteers import models, constants


class UserCreationForm(BaseForm, user_forms.UserCreationForm):
    pass


class UserChangeForm(BaseForm, user_forms.UserChangeForm):
    pass


class VolunteerForm(QuestionnaireModelForm):
    questionnaire_slug = 'volunteer-profile'

    def __init__(self, *args, **kwargs):
        self.language_fields = []
        instance = kwargs.get('instance', None)
        self.base_fields['email'].required = True
        self.base_fields['phone'].required = True
        self.base_fields['birth_date'].required = True

        # Add fields for language
        for lang_code, lang_name in settings.LANGUAGES:
            initial = None
            if instance and isinstance(instance.languages, dict):
                initial = instance.languages.get(lang_code)
            choices = ((None, ''),) + constants.LANGUAGE_PROFICENCY_CHOICES
            field = forms.ChoiceField(
                label=lang_name, required=True, choices=choices, initial=initial)
            self.base_fields['lang_{}'.format(lang_code)] = field

        super().__init__(*args, **kwargs)

        for lang_code, lang_name in settings.LANGUAGES:
            self.language_fields.append(self['lang_{}'.format(lang_code)])

    def save(self, commit=True):
        instance = super().save(commit=False)
        languages = {}
        for lang_code, lang_name in settings.LANGUAGES:
            key = 'lang_{}'.format(lang_code)
            languages[lang_code] = self.cleaned_data[key]
        instance.languages = languages
        instance.save()
        return instance

    def clean_birth_date(self):
        birth_date = self.cleaned_data['birth_date']
        if get_age(birth_date) < 13:
            raise forms.ValidationError(
                'You must be over 13 to volunteer with Bold Idea.')
        return birth_date

    class Meta:
        model = models.Volunteer
        fields = [
            'photo', 'first_name', 'last_name', 'address1', 'address2', 'city',
            'state', 'postal_code', 'email', 'phone', 'alt_phone',
            'is_student', 'company', 'school', 'birth_date',
            'emergency_contact', 'shirt_size', 'race', 'ethnicity', 'gender'
        ]
        widgets = {
            'photo': ProfilePhotoInput(
                message_default=(
                    "Click to change your profile photo. Make sure it clearly "
                    "shows your face! We use this to feature mentors on our "
                    "website.")
            ),
            'birth_date': forms.DateInput(attrs={
                # data-placeholder will only show placeholder when focused
                # (see hive/static/hive/js/init.js)
                'data-placeholder': 'MM/DD/YYYY'
            })
        }


class VolunteerSignupForm(QuestionnaireModelForm):
    questionnaire_data_field = 'form_data'
    birth_date = forms.DateField(required=True)
    full_legal_name = forms.CharField(required=True)
    agreement = forms.BooleanField(required=True)

    def __init__(self, *args, **kwargs):
        opportunity = kwargs.pop('opportunity', None)
        if opportunity and opportunity.questionnaire:
            self.questionnaire_slug = opportunity.questionnaire.slug
        super().__init__(*args, **kwargs)

    def clean_birth_date(self):
        birth_date = self.cleaned_data['birth_date']
        if get_age(birth_date) < 13:
            raise forms.ValidationError(
                'You must be over 13 to volunteer with Bold Idea.')
        return birth_date

    class Meta:
        model = models.VolunteerSignup
        exclude = ['volunteer', 'opportunity', 'form_data', 'date']
