import json
from urllib.parse import quote as quote_url

from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import login_required
from django.contrib import messages
from django.core.mail import send_mail
from django.db import transaction
from django.http import HttpResponseRedirect
from django.template.loader import get_template
from django.shortcuts import render, get_object_or_404
from django.urls import reverse

from programs.models import Program, VolunteerAttendance
from volunteers import models, forms, utils
from hive.utils import json_defaults


def require_profile(request):
    messages.add_message(
        request, messages.INFO,
        'Please complete your volunteer profile before continuing.')
    next = quote_url(request.get_full_path())
    redirect = '{}?next={}'.format(reverse('volunteers-profile'), next)
    return HttpResponseRedirect(redirect)


def profile_required(view):
    @login_required(login_url=settings.LOGIN_URL)
    def wrapper(request, *args, **kwargs):
        profile = get_profile(request)
        if not profile:
            return require_profile(request)
        return view(request, *args, **kwargs)
    return wrapper


def get_profile(request):
    if request.user and request.user.is_authenticated():
        try:
            return models.Volunteer.objects.get(user=request.user)
        except models.Volunteer.DoesNotExist:
            pass
    return None


def opportunities(request):
    context = {
        'opportunities': models.VolunteerOpportunity.objects.filter(status='PUBLISHED')
    }
    return render(request, 'volunteers/opportunities.html', context)


@transaction.atomic
def profile(request):
    user = request.user.is_authenticated and request.user or None
    contact = user and user.contact or None
    profile = get_profile(request)
    next = request.GET.get('next')
    initial = {}

    # If contact exists, use that as base for volunteer
    if contact and not profile:
        profile = models.Volunteer(contact_ptr=contact)
        profile.__dict__.update(contact.__dict__)

    if user:
        UserForm = forms.UserChangeForm
    else:
        UserForm = forms.UserCreationForm

    if request.method == 'POST':
        form = forms.VolunteerForm(
            request.POST, request.FILES, instance=profile, initial=initial)
        user_form = UserForm(request.POST, instance=user)
        next = request.POST.get('next')
        if form.is_valid() and user_form.is_valid():
            # Save the user, then associate the user w/ the volunteer object
            volunteer = form.save()
            user = user_form.save(commit=False)
            user.contact = volunteer
            user.save()

            # log the user in
            if not request.user.is_authenticated():
                user = authenticate(
                    email=form.cleaned_data['email'],
                    password=user_form.cleaned_data['password1']
                )
                login(request, user)

            # redirect
            if next:
                redirect = next
            else:
                redirect = reverse('volunteers-opportunities')
            return HttpResponseRedirect(redirect)
        else:
            # form has errors
            messages.add_message(
                request, messages.ERROR,
                'There was an error with your submission. Please check the '
                'form for errors.')
    else:
        user_form = UserForm(instance=user)
        form = forms.VolunteerForm(instance=profile, initial=initial)

    context = {
        'profile': profile,
        'form': form,
        'user_form': user_form,
        'user': user,
        'next': next,
        'languages': settings.LANGUAGES
    }
    return render(request, 'volunteers/profile.html', context)


@transaction.atomic
def signup(request, slug):
    opportunity = get_object_or_404(models.VolunteerOpportunity, slug=slug)
    volunteer = get_profile(request)

    # order programs by # volunteers so that greatest need is at the top
    programs = (opportunity.programs.with_remaining_volunteers_needed()
                .exclude(hide_on_volunteer_signup=True)
                .order_by('-remaining_volunteers_needed'))

    selected_session_ids = []
    selected_program_ids = request.GET.getlist('program', request.POST.getlist('program'))
    selected_programs = Program.objects.filter(pk__in=selected_program_ids)

    select_multiple = False
    confirm_schedule = False

    working_with_minors = opportunity.role in ('MENTOR', 'FLOATER', 'COORDINATOR')

    if opportunity.role == 'FLOATER':
        select_multiple = True

    if opportunity.role == 'MENTOR' and selected_programs.count() > 0:
        confirm_schedule = True
        # All sessions are pre-selected by default.
        selected_session_ids = selected_programs[0].sessions.values_list('id', flat=True)

    context = {
        'opportunity': opportunity,
        'programs': programs,
        'select_multiple': select_multiple,
        'confirm_schedule': confirm_schedule,
        'selected_programs': selected_programs,
        'selected_session_ids': selected_session_ids,
        'working_with_minors': working_with_minors,
    }

    # whether or not the opportunity has programs associated with it
    has_programs = programs.count() > 0

    # If program hasn't selected, go ahead and render the map page (no need for the form yet)
    if has_programs and len(selected_programs) == 0:
        return render(request, 'volunteers/signup.html', context)

    if not volunteer and (len(selected_programs) > 0 or not has_programs):
        return require_profile(request)

    requirements = []
    if selected_programs.count() > 0:
        requirements = utils.get_requirements(volunteer, selected_programs)

    context['requirements'] = [
        r for r in requirements if r.requirement_type.responsible_party == 'VOLUNTEER'
    ]

    initial = {
        'volunteer': volunteer,
        'opportunity': opportunity,
        'full_legal_name': volunteer.full_legal_name,
        'birth_date': volunteer.birth_date,
        'selected_programs': selected_programs
    }

    if request.method == 'POST' and request.POST.get('submit-signup-form'):
        form = forms.VolunteerSignupForm(
            request.POST, opportunity=opportunity, initial=initial)
        selected_session_ids = set(map(int, request.POST.getlist('session')))
        if form.is_valid():
            # Create the signup instance
            signup = form.save(commit=False)
            signup.opportunity = opportunity
            signup.volunteer = volunteer

            # Add form data for historical purposes
            form_data = signup.form_data or {}
            form_data.update(form.cleaned_data)
            form_data['opportunity'] = signup.opportunity.slug
            form_data['selected_programs'] = [p.program_code for p in selected_programs.all()]
            signup.form_data = json.loads(json.dumps(form_data, default=json_defaults))

            signup.save()

            # Update the legal name / birth date on volunteer
            volunteer.full_legal_name = form.cleaned_data['full_legal_name']
            volunteer.birth_date = form.cleaned_data['birth_date']
            volunteer.save()

            # Save requirements
            for requirement in requirements:
                requirement.save()

            # Create assignments
            for program in selected_programs:
                # Automatically create assignment and associate it with the signup
                utils.create_assignment(volunteer, program, opportunity.role, requirements, signup)

            for session_id in selected_session_ids:
                # An attendance record represents the expectation that a person is present
                # at a given session.
                VolunteerAttendance(session_id=session_id, volunteer=volunteer).save()

            send_signup_confirmation_emails(request, signup)

            redirect = reverse(
                'volunteers-signup-complete',
                kwargs={'slug': slug, 'signup_id': signup.pk})
            return HttpResponseRedirect(redirect)
        else:
            # form has errors
            messages.add_message(
                request, messages.ERROR,
                'There was an error with your submission. Please check the '
                'form for errors.')
    else:
        form = forms.VolunteerSignupForm(
            opportunity=opportunity, initial=initial)

    context['form'] = form

    return render(request, 'volunteers/signup.html', context)


def signup_complete(request, slug, signup_id):
    opportunity = get_object_or_404(models.VolunteerOpportunity, slug=slug)
    signup = get_object_or_404(models.VolunteerSignup, pk=signup_id)
    requirements = signup.requirements.pending().filter(
        requirement_type__responsible_party='VOLUNTEER')
    context = {
        'opportunity': opportunity,
        'signup': signup,
        'assignments': signup.assignments.all(),
        'requirements': requirements,
        'training_sessions': signup.training_sessions
    }
    return render(request, 'volunteers/signup_complete.html', context)


def send_signup_confirmation_emails(request, signup):
    # Volunteer email (html & plaintext)
    subject = ('Your Bold Idea volunteer registration is confirmed! '
               'Details inside.')
    requirements = signup.requirements.pending().filter(
        requirement_type__responsible_party='VOLUNTEER')
    admin_change_url = reverse('admin:volunteers_volunteersignup_change', args=[signup.id])
    context = {
        'signup': signup,
        'assignments': signup.assignments.all(),
        'requirements': requirements,
        'training_sessions': signup.training_sessions,
        'details_url': request.build_absolute_uri(admin_change_url)
    }
    plain_message = get_template(
        'volunteers/signup_complete_email.txt').render(context)
    html_message = get_template(
        'volunteers/signup_complete_email.html').render(context)
    send_mail(
        subject, plain_message, settings.DEFAULT_FROM_EMAIL,
        [signup.volunteer.email], html_message=html_message)

    # Admin notification (plaintext)
    notification_msg = get_template(
        'volunteers/signup_notification.txt').render(context)
    notification_subj = 'A volunteer has signed up for "{}"'.format(
        signup.opportunity.name)
    send_mail(
        notification_subj, notification_msg, settings.DEFAULT_FROM_EMAIL,
        settings.REGISTRATION_NOTIFICATION_RECIPIENTS)
