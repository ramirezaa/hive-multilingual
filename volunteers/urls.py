from django.conf.urls import url
from volunteers import views


urlpatterns = [
    url('^$', views.opportunities, name='volunteers-opportunities'),
    url('^profile/$', views.profile, name='volunteers-profile'),
    url('^(?P<slug>[^/]+)/complete/(?P<signup_id>[^/]+)/$',
        views.signup_complete, name='volunteers-signup-complete'),
    url('^(?P<slug>[^/]+)/$', views.signup, name='volunteers-signup'),
]
