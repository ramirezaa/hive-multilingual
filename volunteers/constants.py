SHIRT_SIZE_CHOICES = (
    ('', ''),
    ('SM', 'Small'),
    ('M', 'Medium'),
    ('L', 'Large'),
    ('XL', 'XL'),
    ('XXL', 'XXL')
)

LANGUAGE_PROFICENCY_CHOICES = (
    (0, '0 - No proficiency'),
    (1, '1 - Elementary proficiency'),
    (2, '2 - Limited working proficiency'),
    (3, '3 - Professional working proficiency'),
    (4, '4 - Full professional proficiency'),
    (5, '5 - Native or bilingual proficiency')
)

VOLUNTEER_ROLE_CHOICES = (
    ('', ''),
    ('MENTOR', 'Mentor'),
    ('FLOATER', 'Floater'),
    ('COORDINATOR', 'Coordinator')
)

VOLUNTEER_REQUIREMENT_STATUS_CHOICES = (
    ('INCOMPLETE', 'Incomplete'),
    ('PROCESSING', 'Processing'),
    ('COMPLETED', 'Completed'),
    ('INVALID', 'Invalid')
)
