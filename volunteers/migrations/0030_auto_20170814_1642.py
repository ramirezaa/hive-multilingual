# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-14 21:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('volunteers', '0029_auto_20170731_2220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='volunteerinterest',
            name='program',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='programs.Program'),
        ),
    ]
