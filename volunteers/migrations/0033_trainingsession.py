# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-29 22:59
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('programs', '0052_program_advisor'),
        ('volunteers', '0032_auto_20170818_2015'),
    ]

    operations = [
        migrations.CreateModel(
            name='TrainingSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('start_time', models.TimeField(blank=True, null=True)),
                ('end_time', models.TimeField(blank=True, null=True)),
                ('name', models.CharField(max_length=128)),
                ('description', models.TextField(blank=True)),
            ],
        ),
        migrations.AlterModelOptions(
            name='volunteer',
            options={'ordering': ['first_name', 'last_name']},
        ),
        migrations.AddField(
            model_name='trainingsession',
            name='attendees',
            field=models.ManyToManyField(blank=True, related_name='attended_training_sessions', to='volunteers.Volunteer'),
        ),
        migrations.AddField(
            model_name='trainingsession',
            name='facilitators',
            field=models.ManyToManyField(blank=True, related_name='facilitated_training_sessions', to='volunteers.Volunteer'),
        ),
        migrations.AddField(
            model_name='trainingsession',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='programs.Location'),
        ),
        migrations.AddField(
            model_name='trainingsession',
            name='opportunity',
            field=models.ForeignKey(blank=True, help_text='This session will be for volunteers who sign up for this opportunity', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='training_sessions', to='volunteers.VolunteerOpportunity'),
        ),
        migrations.AddField(
            model_name='trainingsession',
            name='programs',
            field=models.ManyToManyField(blank=True, help_text='This session will be for volunteers assigned to these programs.', related_name='training_sessions', to='programs.Program'),
        ),
    ]
