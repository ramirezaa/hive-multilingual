# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-07-20 15:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('volunteers', '0020_auto_20170718_1735'),
    ]

    operations = [
        migrations.AddField(
            model_name='volunteer',
            name='is_student',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='volunteer',
            name='school',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
