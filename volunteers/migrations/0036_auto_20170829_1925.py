# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-30 00:25
from __future__ import unicode_literals

from django.db import migrations

def migrate_fwd(apps, schema_editor):
    TrainingSession = apps.get_model('volunteers.TrainingSession')
    for session in TrainingSession.objects.all():
        if session.programs.count() > 0:
            session.opportunity = session.programs.all()[0].volunteer_opportunity
            session.save()


class Migration(migrations.Migration):

    dependencies = [
        ('volunteers', '0035_auto_20170829_1832'),
    ]

    operations = [
        migrations.RunPython(migrate_fwd)
    ]
