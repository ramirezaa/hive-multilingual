from dateutil.relativedelta import relativedelta
from django.utils import timezone
from django.conf import settings
from django.db import models
from django.db.models import Q
from django.contrib.postgres.fields import JSONField
from django.template.defaultfilters import slugify
from django.utils.dateformat import format as format_date

from contacts.models import Contact, DemographicsModel
from questionnaires.models import Questionnaire
from programs.query import SemesterQuerySet
from . import constants
from .query import VolunteerRequirementQuerySet


class Volunteer(DemographicsModel, Contact):
    shirt_size = models.CharField(max_length=5, choices=constants.SHIRT_SIZE_CHOICES, blank=True)
    full_legal_name = models.CharField(max_length=128, blank=True)
    emergency_contact = models.TextField(
        blank=True, help_text=(
            'Provide the name and phone number of the person we should '
            'contact in the event of an emergency'))
    is_student = models.BooleanField()
    school = models.CharField(blank=True, max_length=255)
    last_agreement_date = models.DateTimeField(null=True, blank=True)
    extra_data = JSONField(null=True, blank=True)
    languages = JSONField(null=True, blank=True)

    programs = models.ManyToManyField('programs.Program', through='VolunteerAssignment')

    objects = SemesterQuerySet.as_manager('programs__start_date')

    class Meta:
        ordering = ['first_name', 'last_name']

    @property
    def agreement_expired(self):
        if not self.last_accepted_agreement:
            return True
        agreement_age = timezone.now() - self.last_accepted_agreement
        return agreement_age >= settings.VOLUNTEER_TERMS_RENEWAL_PERIOD

    def get_upcoming_training_sessions(self):
        sessions = TrainingSession.objects.filter(
            date__gte=timezone.now()).order_by('date', 'start_time')

        if self.user.is_superuser:
            return sessions

        my_opportunities = (VolunteerOpportunity.objects
                            .exclude(status='ARCHIVED')
                            .filter(signups__volunteer=self))
        my_programs = self.programs.all()

        return sessions.filter(
            Q(programs__in=my_programs) |
            Q(programs=None, opportunities__in=my_opportunities) |
            Q(facilitators=self)
        ).distinct()


class VolunteerRequirementType(models.Model):
    name = models.CharField(max_length=64)
    slug = models.SlugField()
    responsible_party = models.CharField(max_length=16, choices=(
        ('VOLUNTEER', 'Volunteer'),
        ('STAFF', 'Staff')
    ), default='VOLUNTEER')
    renewal_months = models.IntegerField(null=True, blank=True)
    instructions = models.TextField(help_text="markdown supported", blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Volunteer requirement'
        verbose_name_plural = 'Volunteer requirements'


class VolunteerRequirement(models.Model):
    requirement_type = models.ForeignKey(
        VolunteerRequirementType, related_name='pending_or_completed_requirements',
        on_delete=models.PROTECT)
    volunteer = models.ForeignKey(Volunteer, related_name='requirements')
    status = models.CharField(
        max_length=16, choices=constants.VOLUNTEER_REQUIREMENT_STATUS_CHOICES,
        default='INCOMPLETE')
    completed_date = models.DateField(null=True, blank=True)
    notes = models.TextField(blank=True)
    expire_date = models.DateField(null=True, blank=True)

    objects = VolunteerRequirementQuerySet.as_manager()

    def save(self, *args, **kwargs):
        if not self.expire_date and self.completed_date and self.requirement_type.renewal_months:
            delta = relativedelta(months=self.requirement_type.renewal_months)
            self.expire_date = self.completed_date + delta
        super().save(*args, **kwargs)

    def is_expired(self, as_of=None):
        as_of = as_of or timezone.now()
        return self.expire_date is None or self.expired_ate >= as_of

    @property
    def name(self):
        return self.requirement_type.name

    @property
    def instructions(self):
        return self.requirement_type.instructions

    def __str__(self):
        return self.name


class VolunteerOpportunity(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField()
    short_description = models.TextField()
    description_url = models.URLField(blank=True)
    role = models.CharField(max_length=32, choices=constants.VOLUNTEER_ROLE_CHOICES, blank=True)
    photo = models.ImageField(blank=True)
    status = models.CharField(max_length=16, choices=(
        ('DRAFT', 'Draft'),
        ('PUBLISHED', 'Published'),
        ('ARCHIVED', 'Archived')
    ), default='DRAFT')
    order = models.IntegerField(default=0)
    questionnaire = models.ForeignKey(Questionnaire, blank=True, null=True)
    confirmation_extra_info = models.TextField(blank=True)
    programs = models.ManyToManyField(
        'programs.Program', blank=True, related_name='volunteer_opportunities')
    requirements = models.ManyToManyField(VolunteerRequirementType, blank=True)
    role = models.CharField(max_length=32, choices=constants.VOLUNTEER_ROLE_CHOICES, blank=True)

    objects = SemesterQuerySet.as_manager('programs__start_date')

    def __str__(self):
        if self.status == 'ARCHIVED':
            return self.name + ' (archived)'
        if self.status == 'DRAFT':
            return self.name + ' (draft)'
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    @property
    def program_locations(self):
        locations = self.programs.values_list('location__name', flat=True)
        return locations.distinct()

    class Meta:
        verbose_name_plural = 'Volunteer opportunities'
        ordering = ['order']


class TrainingSession(models.Model):
    opportunities = models.ManyToManyField(
        VolunteerOpportunity, blank=True, related_name='training_sessions',
        help_text="This session will be for volunteers who have signed up for this opportunity")
    programs = models.ManyToManyField(
        'programs.Program', blank=True, related_name='training_sessions',
        help_text="This session will be for volunteers assigned to these programs.")
    location = models.ForeignKey('programs.Location')
    date = models.DateField()
    start_time = models.TimeField(null=True, blank=True)
    end_time = models.TimeField(null=True, blank=True)
    name = models.CharField(max_length=128)
    description = models.TextField(blank=True)
    facilitators = models.ManyToManyField(
        Volunteer, blank=True, related_name='facilitated_training_sessions')
    attendees = models.ManyToManyField(
        Volunteer, blank=True, related_name='attended_training_sessions')
    location_info = models.TextField(
        'Location info / parking instructions', blank=True)
    notes = models.TextField(blank=True)

    @property
    def date_description(self):
        datestr = format_date(self.date, 'N j, Y')
        if self.start_time:
            datestr += ', ' + format_date(self.start_time, 'P')
        if self.end_time:
            datestr += ' - ' + format_date(self.end_time, 'P')
        return datestr

    @property
    def invitees(self):
        programs = self.programs.all()
        opportunities = self.opportunities.all()
        if opportunities and not programs:
            # get volunteers who have signed up for the associated opportunity
            filter = Q(signups__opportunity__in=opportunities)
        elif programs:
            # get volunteers who have signed up for the associated programs
            filter = Q(programs__in=programs)

        # exclude volunteers who have already attended training for this opportunity
        filter &= ~Q(attended_training_sessions__opportunities__in=opportunities)

        # include facilitators
        filter |= Q(facilitated_training_sessions=self)

        # include attendees of THIS session
        filter |= Q(attended_training_sessions=self)

        return Volunteer.objects.filter(filter).distinct()

    def __str__(self):
        return self.name


class VolunteerSignup(models.Model):
    """
    Represents the event that a volunteer signed up for an opportunity.
    """
    date = models.DateTimeField(default=timezone.now, null=True)
    volunteer = models.ForeignKey(Volunteer, related_name='signups')
    opportunity = models.ForeignKey(VolunteerOpportunity, related_name='signups')
    form_data = JSONField(null=True, blank=True)

    objects = SemesterQuerySet.as_manager('opportunity__programs__start_date')

    def __str__(self):
        return '{} - {}'.format(self.opportunity, self.volunteer)

    @property
    def requirements(self):
        return VolunteerRequirement.objects.filter(assignments__in=self.assignments.all())

    @property
    def training_sessions(self):
        q = models.Q(programs=None, opportunities=self.opportunity)
        if self.assignments.count() > 0:
            programs = self.assignments.values_list('program', flat=True)
            q |= models.Q(programs__in=programs)
        return TrainingSession.objects.filter(q).distinct()

    @property
    def attended_training_sessions(self):
        return self.training_sessions.filter(attendees=self.volunteer).distinct()

    @property
    def session_attendances(self):
        from programs.models import VolunteerAttendance
        return VolunteerAttendance.objects.filter(
            volunteer=self.volunteer, session__program__in=self.opportunity.programs.all())


class VolunteerAssignment(models.Model):
    """
    Represents a volunteer's role for a given program. This is independent of the signup, although
    it can be created automatically during a signup, during which case the signup will be
    associated with the Assignment for reference purposes. Assignments can also be created manually
    by admins.
    """
    program = models.ForeignKey('programs.Program', related_name='volunteer_assigments')
    volunteer = models.ForeignKey(Volunteer, related_name='assignments')
    role = models.CharField(max_length=32, choices=constants.VOLUNTEER_ROLE_CHOICES, blank=True)
    requirements = models.ManyToManyField(
        VolunteerRequirement, related_name='assignments', blank=True)
    signup = models.ForeignKey(
        VolunteerSignup, related_name='assignments', null=True, blank=True,
        on_delete=models.SET_NULL)

    objects = SemesterQuerySet.as_manager('program__start_date')

    def __str__(self):
        return '{} - {}'.format(self.program, self.get_role_display())
