��    
     l  g  �      X  �  Y  �   9  �     �      |   �     
  �   �  �     c   �  �     �   �    j  t  l  z   �   G   \!  �   �!  n   )"  l   �"  Z   #  L  `#  �   �$  (   �%     �%     �%     �%     �%     &     &     0&     ?&     N&     Z&     g&     s&  	   |&     �&     �&  �   �&  
   j'  !   u'  <   �'     �'     �'     �'  
   �'     �'  
   (     (     %(     ?(  
   R(     ](     f(     r(     w(     �(     �(     �(     �(     �(     �(     �(     �(  +   �(     )  �   &)  &   $*     K*     ^*     g*     n*     �*     �*     �*     �*  *   �*     �*  	   �*  	   �*     �*     �*     �*      +  ^   4+  
   �+     �+     �+     �+     �+     �+      �+     �+     �+  L   ,     X,     u,     �,     �,  =   �,  .   �,     -     %-     .-     6-     H-     X-  �  ]-     I/     P/  	   Y/     c/     j/     w/  	   /     �/  3   �/     �/     �/     �/     �/     �/     �/     0  	   0     0     *0     30     ;0  Q   C0  
   �0     �0     �0     �0     �0  
   �0  
   �0     �0     �0     �0     �0     1     1     ,1  !   21  )   T1     ~1     �1     �1     �1  =   �1     �1     �1     �1     �1     2     2     02  )   F2  A   p2     �2     �2     �2     �2     �2     �2  I   �2     43     N3     Z3     m3     u3     ~3     �3     �3     �3     �3     �3     �3     �3     
4     '4     =4     N4     d4     w4     �4     �4     �4     �4     �4      5     5     -5     D5     _5     v5     �5  +   �5  
   �5     �5     �5     �5     �5     6     6     6     "6  H  /6     x7  	   �7     �7     �7     �7  �   �7  A  �8  A  �;  A  ?  >   FB      �B  =   �B  ?   �B     $C     ;C     RC  E   XC  �  �C     qE     �E     �E     �E     �E     �E     �E  	   �E     �E     �E     �E     F  
   
F     F     /F     JF     bF     zF     �F     �F     �F     �F     �F  	   �F  	   �F  #   �F     G     &G  +   *G  E   VG     �G     �G     �G     �G     �G     �G  (   �G  	   H     H  P  %H    vI  �   �K    ZL  w   bM  �   �M  �   eN  �   �N  �   �O  o   3P  �   �P  �   `Q  �   �Q  J  �R  �   T  J   �T  �   �T  �   �U  �   V  V   �V  ]  �V    WX  %   ]Y     �Y     �Y     �Y     �Y     �Y     �Y     Z     Z     (Z     6Z     EZ  	   UZ  
   _Z     jZ     qZ  �   yZ      k[  "   �[  C   �[     �[     �[  
   \  
   \     \     9\     E\     ^\     t\  
   �\  	   �\     �\     �\     �\     �\     �\     �\     �\     ]     ]     0]     9]  .   E]  	   t]  [  ~]  .   �^     	_     _     (_     1_     F_     O_     c_     x_  3   _     �_  	   �_     �_     �_     �_     �_      �_  c   `     |`     �`     �`     �`     �`     �`     �`     �`     �`  B   �`      @a     aa     ya     �a  B   �a  )   �a     b     b     #b     +b     ?b     Tb  �  Yb     ,e     3e     <e     Ee     Ue     ge  	   ve     �e  8   �e     �e     �e     �e     �e     �e     �e     f  	   f     f     (f     1f     9f  I   Af  	   �f     �f     �f     �f     �f  
   �f  
   �f     �f     �f     �f     �f     �f     g     *g  $   0g  ,   Ug     �g     �g     �g  	   �g  >   �g     �g     �g     �g  (   �g     %h     1h     Nh  +   nh  E   �h     �h     �h  	   �h     �h     	i     i  T   #i     xi     �i     �i     �i  	   �i     �i     �i     �i     j     *j     Bj     Kj     dj     qj     �j     �j     �j     �j     �j     �j     k     k     3k     Gk     ak     uk     �k     �k     �k     �k     �k  >   l     Al     Rl     _l     nl     {l     �l     �l  
   �l     �l  �  �l     Un  	   in     sn     �n     �n  �   �n  2  �o  �  �s  �  �v  ?   �y  )   �y  >   z  Q   \z     �z     �z     �z  Q   �z  �  -{     �}      �}     �}     �}  
   ~     ~     ~     ~     0~     >~     N~     Z~  
   c~     n~     �~     �~     �~     �~     �~               0     >  	   J     T     ]     {     �  :   �  O   �     �     -�     :�     H�     W�     `�  ,   e�  	   ��     ��     �   '      �   ;   �   E   s       m   �       U       c   {       *       �       �   w   �   �             �       h   y   5      b   W     �       !            �       �   �   �   �   A       8             L   �   "   B       �             _   x       �       a   �   +   �           �   F       r   M   �   �   �   �   T   I       t          �       �   /   ]       �                 �      �      �   �       �   �               R   �   f     �   �       �   �   &   �     0       #       �         6   V   D   n       2         )       u   =      @   �      Z       �   �       
   �           �   -   �   \           �       �       G          Y   �       �           �      �      �   �      �       �   O   �   d   `   q   �      �   �       C       �   �   �       �   �                          �       �       �   �   �       �   �   K   k   	   �   �   �   �             �   z   �   ^   �     |   4   �   
        �       �   �      �   �       Q           P   �   �   7   �   .          j           �   �       �   �   �   9               �   }          �          �           v   ,       �   �   �       N   :   g   �   �   �   i   <   �   J       �       ?   1   �   �       p   �   �           �   (   �   %       �   �   �   �      H   e           l   ~   o   �   �       X   �   	  3   S   �       �   �   �   �   �   �       �   $   �   >                  �   [    
        I understand that when participating in Bold Idea activities, my child
        may be photographed for print, video, or electronic imaging. I
        understand that the images may be used in promotional materials, news
        releases, and other formats for Bold Idea. I acknowledge that any images
        will be the sole property of Bold Idea. By submitting this form, I swear
        that all information provided on this application is true and accurate.
         
        Please verify that you can take your child to <strong>at least 75%% of the program sessions</strong>.
        If this is not possible, please leave this seat open for another child.
         
      * Bold Idea is a 501(c)(3) nonprofit organization, and your gift may qualify
      as a charitable deduction for federal income tax purposes. Consult with your
      tax advisers or the IRS to determine whether a contribution is deductible.
       
      Is there anything else would you like us to know about your child that
      might impact our ability to mentor or teach them?
       
      On the next screen you'll have the option to complete your registration or
      add more students if desired.
       
      Please list any coding activities that your child has done in the past. 
      (No prior experience is required.)
       
      Please list any other activities or after-school programs that might 
      conflict with their participation in this program
       
      Please make sure your student will be available for the majority of the
      sessions. If there are any potential conflicts, please note them below.
       
      See <a target="_blank" href=%(bold_idea_url)s>https://boldidea.org</a> for more info.
       
      The following is a summary of your registration. If you need to make
      changes, click <a href=%(programs_view_cart)s>here</a> to go
      back to your cart.
       
      To apply for financial assistance, please complete the following
      information. We will contact you as soon as we review your application.
       
    Although not required, we encourage families to provide the following
    information on the student's racial background, ethnicity, and gender. This
    information will be used to improve our outreach efforts and advance Bold
    Idea's mission.
     
    Bold Idea is a nonprofit* organization that is able to provide its
    programs to area students through donations from people like you. Consider
    making a one-time or recurring donation to Bold Idea. Your support will enable
    us to provide computer science education programs to more students in the
    Dallas area. Thank you in advance for your support.
     
    Select an existing student to enroll in this program, or select "Register a new
    student" to add a new student.
   
  Click "Checkout" when you're ready to complete your registration.
   
  Click "Checkout" when you're ready to complete your registration.
  To add another student, click "Add another student" below.
   
  There was an error with your registration. Please review the form fields below
  for any error messages.
   
  To add another student, click <a href=%(programs_index)s>here</a>
  to go back to the program catalog.
   
  We're sorry, there was an error processing your card: %(billing_error|linebreaksbr)s
   * Most sessions will be held in Building 3 at Granite Park. Enter the building near the west entrance of the parking garage.
* On September 27, the session will be held in Building 5
* On October 25, the session will be held in Building 4

A map of granite park can be found [here](https://boldidea.org/static/granite-park-map.png). *Please note that the first session will be from 1-3pm. All other sessions will be 10-12pm.*

The session will be held in Conference Room A. As you walk in the door, turn right, and you'll see a room with glass windows at the far back. A previous Bold Idea program or workshop Add Student Add another contact Add another student Add another student to Add to cart Additional Information Address line 1 Address line 2 Adult Large Adult Medium Adult Small Adult XL Adult XXL Ages Alabama All sessions will be held in Building 3 at Granite Park. Enter the building near the west entrance of the parking garage. A map of granite park can be found [here](https://boldidea.org/static/granite-park-map.png). Alt. phone American Indian or Alaskan Native Amount of financial assistance (discount) you are requesting Arizona Arkansas Asian Attendance Billing zip Birth date Birth date (mm/dd/yyyy) Black or African American Bold Idea Programs California Canceled Card number Cart Change my password Checkout City Classes in school Code.org's Hour of Code Colorado Company/employer Complete Connecticut Contact this person in case of an emergency Continue Create new worlds with computing! Students will bring their imagination to life by coding their own creative projects, while learning the basics of computer science with their friends. Projects include animations, storytelling, the arts and game design. Created with	&#x2764; at Bold Idea HQ. Creative Computing Delaware Delete District of Columbia Donate Today Email Email address: English Enlisted in the US militiary guard/reserve Errors Ethnicity Exp. date Fee Female Financial Assistance Financial Assistance Information Financial assistance is available for qualifying families. See details on the checkout screen. First name Florida Flyer Friend / Family Gender Grade level Grade level ({year} school year) Grades HIVE Administration Have you received financial assistance from Bold Idea in the past 12 months? Hawaiian or Pacific Islander Hispanic or Latina/Latino How did you hear about us? I agree I can bring my child to at least 75%% of the program sessions I would like to apply for financial assistance Idaho Illinois Indiana Information Table Internet search Iowa Jr. High students begin using computer science as a problem-solving tool. The 14-week ideaSpark course is grounded in a design thinking process that includes discovery, ideation, prototyping, user testing and sharing. Students develop proficiency in computer programming languages and collaborate on their own creative project, from mobile apps to websites. At the end of the semester, student teams share their projects at a Demo Day event with family, mentors and sponsors in the audience. Kansas Kentucky Last name Log in Log in again Log out Louisiana Maine Make a BOLD impact. Become a Bold Idea donor today. Male Maryland Massachusetts Michigan Middle name Military Status Military status Minnesota Mississippi Missouri Montana Monthly Monthly payment plans and financial assistance are available for select programs. My Account Nebraska Nevada New New Hampshire New Jersey New Mexico New York New password: No North Carolina North Dakota Not Hispanic or Latina/Latino Notes Number of children living at home Number of people supported by this income Ohio Oklahoma One-time Online Online tutorials, like Khan Academy, Code Academy, or Scratch Oregon Other Over $45,000 Parent / Legal Guardian info Password Password confirmation Passwords don't match Pay in full (${amount}) using credit card Pay in {num} monthly payments (${amount}/month) using credit card Payment Payment method Pending Pennsylvania Phone Photo Please list any unusual or extraordinary family expenses or circumstances Please verify attendance. Postal code Preferred language Program Programs Race Race, Ethnicity and Gender Register a new student Registration Complete! Relationship to student Remove Reset my password Rhode Island Saturday, June 24 10am - 1pm Saturdays,  1pm - 3pm Saturdays, 1-3pm Saturdays, 10 - 12 pm Saturdays, 10-12pm Saturdays, 10am - 12pm Saturdays, 10am-12pm Saturdays, 11:30 - 1pm Saturdays, 12pm - 2pm Saturdays, 1pm - 3pm Saturdays, 2:30pm - 4:30pm Saturdays, 2pm-4pm Saturdays, 3:30 - 5:30 pm Saturdays, 3:30-5:30pm Saturdays, 3:30pm - 5:30pm Saturdays, 9:30 - 11am Saturdays, 9:30am - 11:30am School name School or Rec Center Activity/Program Guide Shirt size Social Media South Carolina South Dakota Spanish Special Event State Student Student Info Students should wait in the cafeteria after school. There will be a Bold Idea sign for them to gather near with their volunteer mentors. After the session, they should be picked up in the front of the school. Students do NOT need to bring a laptop or other supplies; Rosemont chromebook devices will be provided for student use. Submit Registration Tennessee Terms and Conditions Texas Thanks for using our site! The MIX is located at 9125 Diceman Drive, Dallas, TX 75218 at White Rock United Methodist Church. Here is a Google Maps view: http://themixcoworking.spaces.nexudus.com/en/contact. The program will be located in room *3.209* in the Edith O'Donnell Arts and Technology building (ATC).

To find the Edith O'Donnell Arts and Technology building:

* Enter campus via University Parkway
* Veer right onto Armstrong Drive and continue north onto East Drive
* Turn left on Drive A, then right into Parking Structure 1.
* Park in any _visitor parking_ space on the upper level. You will not need to pay at the ground level pay-to-park box.
* Take the footpath west from the parking structure to enter the Edith O’Donnell Arts and Technology Building (ATC)

Once inside the building, take the elevator to the 3rd floor. You can locate room 3.209 using the building map here:  http://www.utdallas.edu/locator/ATC_3.209. 

(Please print this map or save it on your phone so that you can easily locate the room upon arrival) The program will be located in room *3.914* in the Edith O'Donnell Arts and Technology building (ATC).

To find the Edith O'Donnell Arts and Technology building:

* Enter campus via University Parkway
* Veer right onto Armstrong Drive and continue north onto East Drive
* Turn left on Drive A, then right into Parking Structure 1.
* Park in any _visitor parking_ space on the upper level. You will not need to pay at the ground level pay-to-park box.
* Take the footpath west from the parking structure to enter the Edith O’Donnell Arts and Technology Building (ATC)

Once inside the building, take the elevator to the 3rd floor. You can locate room 3.601 using the building map here:  http://www.utdallas.edu/locator/ATC_3.601. 

(Please print this map or save it on your phone so that you can easily locate the room upon arrival) The program will be located in room *3.914* in the Edith O'Donnell Arts and Technology building (ATC).

To find the Edith O'Donnell Arts and Technology building:

* Enter campus via University Parkway
* Veer right onto Armstrong Drive and continue north onto East Drive
* Turn left on Drive A, then right into Parking Structure 1.
* Park in any _visitor parking_ space on the upper level. You will not need to pay at the ground level pay-to-park box.
* Take the footpath west from the parking structure to enter the Edith O’Donnell Arts and Technology Building (ATC)

Once inside the building, take the elevator to the 3rd floor. You can locate room 3.601 using the building map here:  http://www.utdallas.edu/locator/ATC_3.914. 

(Please print this map or save it on your phone so that you can easily locate the room upon arrival) There are currently no programs available for open-enrollment. There are no items in your cart. This person is interested in volunteering at the program site This program is available only to students of Rosemont Schools. Thursdays, 3:45 - 5:30 Thursdays, 3:45-5:30pm Title To finalize your registration, please complete the information below. To find the Edith O’Donnell Arts and Technology Building:

* Enter campus via University Parkway
* Veer right onto Armstrong Drive and continue north onto East Drive
* Turn left on Drive A, then right into Parking Structure 1.
* Park in any _visitor parking_ space on the upper level. You will not need to pay at the ground level pay-to-park box.
* Take the footpath west from the parking structure to enter the Edith O’Donnell Arts and Technology Building (ATC) Total family income US military veteran US stateGeorgia Under $15,000 Update Utah Vermont View Cart View Programs View locations View schedule Virginia Washington Wednesdays, 3:45 - 5:30pm Wednesdays, 3:45 - 5:45 pm Wednesdays, 3:45-5:45pm Wednesdays, 4:30-6:00pm Wednesdays, 4:30-6:30pm Wednesdays, 4:30-6pm Wednesdays, 5 - 7 pm Wednesdays, 5pm - 7pm West Virginia White Wisconsin Workshops Workshops from another organization Wyoming Yes You must agree to the terms and conditions. You will soon receive an email with the details of your registration. Your password was changed. Youth Large Youth Medium Youth Small Youth XL age eg: mother, father, legal guardian, etc. ideaSpark schedule Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-21 16:54-0500
PO-Revision-Date: 2018-07-21 16:49-0500
Last-Translator: 
Language-Team: 
Language: es_MX
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.7.1
 
        Yo entiendo que cuando mi hijo/hija participe en actividades de Bold Idea, ellos podran ser fotografiados para imprimir, vídeo, o imágenes electrónicas. Yo entiendo que las imágenes podrían ser usadas para materiales promocionales, comunicado de prensa, y otros formatos para Bold Idea. Yo reconozco que cualquier imagen tomada durante las actividades de Bold Idea sera la propiedad única de Bold Idea. Al entregar este documento, yo juro que toda la información en esta aplicación es verdadero y preciso.
         
        Por favor verifique que su hijo/hija podra atender <strong>por lo menos 75%% de las sesiones</strong>.
        Si esto no es posible, por favor deje el espacio abierto para otro estudiante.
         
   *Bold Idea es una 501(c)(3) organizacion sin fines de lucro, y su regalo puede
   cualificar como una deduccion de caridad, para propositos de impuestos federales.
   Consulte con su asesor de impuestos o el IRS para ver si su contribución es deductible.
    
       Hay algo mas que necesitamos saber sobre su hijo/hija que pudiera impactar nuestra habilidad para enseñarlos?  
       En la siguiente pantalla tendrá la oportunidad para completar su registracion o agregar a mas estudiantes, si usted desea.        
       Por favor escriba actividades relacionadas con computación que su hijo/hija ha hecho en el pasado. (Experiencia previa no es necesario) 
       Por favor escriba otras actividades o programas despues de la escuela 
       que pudieran crear un conflicto con el programa de Bold Idea.        
      Por favor asegure que su estudiante estara disponible para la mayor parte de las sesiones.
      Si va ha ver conflictos, favor de anotarlos abajo.
       
          Vea <a target="_blank" href=%(bold_idea_url)s>https://boldidea.org</a> para mas información.
       
      Lo siguiente es un resumen de su registracion. Si necesita hacer cambios
      , haga click <a href=%(programs_view_cart)s>aqui</a> para regresar a su
      carro de compras.
       
      Para aplicar para ayuda financiera, por favor escriba la siguiente información. Nosotros los contactaremos tan pronto que veamos su aplicación.       
    Aunque no es necesario, nos ayudaria mucho si incluye la informacion
    sobre la raza, etnicidad y genero del estudiante. Esta información sera usada para mejorar la misión de Bold Idea. 
   Bold Idea es un organizacion sin fines de lucro* que establece programas para estudiantes
   con donaciones de gente como usted. Considere hacer una sola donacion o una donacion
   periodica. Su ayuda nos deja producir programas relacionadas a ciencia de computación en el
   área de Dallas. Muchas gracias por su ayuda.
    
    Seleccione a un estudiante existente para enrollar en este programa, o seleccione "Registrar a un estudiante nuevo"
    para agregar a un estudiante nuevo.
   
  Haga click en "Pagar" cuando este listo para completar su registracion. 
  Haga click en "Pagar" cuando este listo para completar su registracion. Para
  agregar a otro estudiante, seleccione "Agregar otro Estudiante" abajo
   
  Hubo un error con su registracion. Por favor revise los campos de entrada    en el caso que digan un mensaje sobre los errores
   
   Para agregar a otro estudiante, haga click <a href=%(programs_index)s>aqui</a>
   para regresar al catalogo de los programas.
    
  Perdon, pero hubo un error procesando su tarjeta: %(billing_error|linebreaksbr)s
   * La mayoría de las sesiones serán en el Edificio 3 en Granite Park. Entre por el edificio cerca de la entrada oeste del edificio de estacionamiento.
*En Septiembre 27, la sesion sera en el edificio 5
*En Octubre 25, la sesion sera en el edificio 4

Puede encontrar un mapa de Granite Park [aqui](https://boldidea.org/static/granite-park-map.png). *Por favor note que la primera sesión sera de la 1-3pm. Todas las otras sesiones serán a las 10-12pm.*

Las sesión sera en el cuarto de conferencia A. Cuando camine hacia la puerta, vaya hacia la derecha, y muy atrás vera a un cuarto con ventanas de vidrio. Programa de Bold Idea previa o taller Agregar Estudiante Agregar otro contacto Agregar otro Estudiante Agregar otro Estudiante Agregar a carro de compras Información Adicional Dirección 1 Dirección 2 Adulto Grande Adulto Mediano Adulto Pequeño Adulto XL Adulto XXL Edades Alabama * Todas las sesiones serán en el Edificio 3 en Granite Park. Entre por el edificio cerca de la entrada oeste del edificio de estacionamiento.

Puede encontrar un mapa de Granite Park [aqui](https://boldidea.org/static/granite-park-map.png). Número de teléfono alternativo Indio Americano o Nativo de Alaska La cantidad de ayuda financiera (descuento) que usted esta pidiendo Arizona Arkansas Asiáticos Asistencia Código postal de facturación Cumpleaños Cumpleaños (mm/dd/yyyy) Negro o Afroamericano Programas de Bold Idea California Cancelado Tarjeta de credito Carro de Compras Cambiar mi contraseña Pagar Ciudad Clases en la escuela Code.org Hora de Codigo Colorado Empresa/empleador Completo Connecticut Contactar a esta persona en caso de emergencia Continuar Crea nuevo mundos con computación! Los estudiantes tendrán la oportunidad de realizar sus imaginaciones a la vida real con la creación de sus proyectos creativos escribiendo código, mientras aprenden las cosas básicas de la ciencia de computación. Los proyectos incluyen animaciones, la narración de cuentos, las artes y diseños de juegos. Creado con	&#x2764; en el centro de Bold Idea. Computación Creativa Delaware Eliminar District of Columbia Done Hoy Correo electrónico Correo electrónico: Ingles Alistado en el militar o guarda/reserva de los EEUU Errores Etnicidad Fecha de caducidad Precio Hembra Ayuda Financiera Información de Ayuda Financiera Ayuda financiera es disponible para familias que califican. Vea los detallesen la pantalla de pago. Primer nombre Florida Volantes Amigos / Familia Genero Nivel de grado Grado ({year} año escolar) Grados Administración de HIVE Ha recibido ayuda financiera de Bold Idea en los pasados 12 meses? Hawaiano o Isleño del Pacífico Hispano o Latina/Latino Como escucho sobre nosotros? Estoy de acuerdo Yo puedo traer a mi hijo/hija a por lo menos 75%% de las sesiones. Me gustaria aplicar para ayuda financiera Idaho Illinois Indiana Mesa de Informacion Busqueda de internet Iowa Los estudiantes de la secundaria empiezaran a usar la ciencia de computacion como herramienta para resolver problemas. La clase de ideaSpark sera 14 semanas y esta creada en una manera que revuelve alrededor de pensando en un proceso de diseño que incluye descubrimiento, creacion de ideas, creando prototipos, examenes de usuarios y compartiendo codigo. Los estudiantes desarollaran competencia en diferentes lenguas de programacion y colaboraran en sus proyectos creativos que pueden incluir aplicaciones mobiles hasta sitios de web. Al fin del semestre, los equipos de estudiantes van a compartir sus proyectos en un día de Demonstracion que sera un evento con las familias, mentores, y patrocinadores en la audencia. Kansas Kentucky Apellido Iniciar Sesión Reiniciar sesión Cerrar Sesión Louisiana Maine Haga un GRAN impacto. Sea un donante para Bold Idea hoy. Varon Maryland Massachusetts Michigan Segundo nombre Estado Militar Estado militar Minnesota Mississippi Missouri Montana Mensual Pagos mensuales y ayuda financiera estan disponibles para unas programas. Mi Cuenta Nebraska Nevada Nuevo New Hampshire New Jersey New Mexico New York Nueva contraseña:  No North Carolina North Dakota No es Hispano ni Latina/Latino Notas Numero de niños viviendo en la casa Numero de personas viviendo con este ingreso Ohio Oklahoma Una vez En línea Tutoriales en línea como Khan Academy, Code Academy o Scratch Oregon Otro Mas de $45,000 Información del Padre / Guardián legal Contraseña Confirmación de contraseña Las contraseñas no son iguales Pagar (${amount}) usando tarjeta de credito Hacer {num} pagos mensuales (${amount}/mes) usando tarjeta de credito Pago Modo de pago Pendiendo Pennsylvania Número de teléfono Foto Por favor escriba circumstancias o gastos familiares que sean raros o extraordinares Por favor verifique asistencia. Código Postal Lenguaje preferido Programa Programas Raza Raza, Etnicidad y Genero Registrar a un estudiante nuevo Su registracion esta Completa! Relación al estudiante Eliminar Reiniciar mi contraseña Rhode Island Sábados, Junio 24 10am - 1pm Sábados, 1pm - 3pm Sábados, 1- 3pm Sábados, 10 - 12pm Sábados, 10 - 12pm Sábados, 10am - 12pm Sábados, 10am - 12pm Sábados, 11:30 - 1pm Sábados, 12pm - 2pm Sábados, 1pm - 3pm Sábados, 2:30pm - 4:30pm Sábados, 2pm - 4pm Sábados, 3:30 - 5:30pm Sábados, 3:30 - 5:30pm Sábados, 3:30pm - 5:30pm Sábados, 9:30 - 11am Sábados, 9:30am - 11:30am Nombre de escuela Guia de Actividades/Programs en Escuela o Centro de Recreacion Medida de camisa Media Social South Carolina South Dakota Español Evento Especial Estado Estudiante Informacion del Estudiante Los estudiantes tienen que esperar en la cafetería después de la escuela. Va ha ver un letrero de Bold Idea para reunir los estudiantes con los mentores voluntarios. Despues de la sesion, los estudiantes tendran que se recogidos en frente de la escuela. Los estudiantes NO necesitan traer una computadora o otros materiales; computadoras chromebook serán disponibles para los estudiantes de Rosemont. Enviar Registracion Tennessee Terminos y Condiciones Texas Gracias por usar nuestro sitio! La localizacion del sitio "The MIX" es 9125 Diceman Drive, Dallas, TX 75218 en White Rock United Methodist Church. Aqui esta la vista del mapa de Google: http://themixcoworking.spaces.nexudus.com/en/contact. El programa sera localizado en el cuarto *3.209* en el edificio llamado "Edith O’Donnell Arts and Technology"(ATC)

Para encontrar el edificio llamado "Edith O’Donnell Arts and Technology":

* Entre en el campo escolar vía "University Parkway"
* Vaya hacia la derecha a la calle "Armstrong Drive" y continúe al Norte  en la calle "East Drive" 
* Luego vaya a la izquierda en la calle "Drive A", y luego vaya a la derecha en la estructura de estacionamiento 1 para estacionarse.
* Se puede estacionarse en cualquier espacio de visitante en el nivel superior. No tiene que pagar en el primer nivel en la caja de pago.
* Tome la banqueta al oeste desde la estructura del estacionamiento para entrar al edificio llamado "Edith O’Donnell Arts and Technology" (ATC)

Cuando este adentro del edificio, tome el elevador al tercer piso. Puede encontrar el cuarto 3.209 usando el mapa del edificio aqui: http://www.utdallas.edu/locator/ATC_3.209. 

(Por favor imprima el mapa o guarde el mapa en su telefono para facilitar la localizacion del cuarto cuando llegue al edificio) El programa sera localizado en el cuarto *3.914* en el edificio llamado "Edith O’Donnell Arts and Technology"(ATC)

Para encontrar el edificio llamado "Edith O’Donnell Arts and Technology":

* Entre en el campo escolar vía "University Parkway"
* Vaya hacia la derecha a la calle "Armstrong Drive" y continúe al Norte  en la calle "East Drive" 
* Luego vaya a la izquierda en la calle "Drive A", y luego vaya a la derecha en la estructura de estacionamiento 1 para estacionarse.
* Se puede estacionarse en cualquier espacio de visitante en el nivel superior. No tiene que pagar en el primer nivel en la caja de pago.
* Tome la banqueta al oeste desde la estructura del estacionamiento para entrar al edificio llamado "Edith O’Donnell Arts and Technology" (ATC) El programa sera localizado en el cuarto *3.914* en el edificio llamado "Edith O’Donnell Arts and Technology"(ATC)

Para encontrar el edificio llamado "Edith O’Donnell Arts and Technology":

* Entre en el campo escolar vía "University Parkway"
* Vaya hacia la derecha a la calle "Armstrong Drive" y continúe al Norte  en la calle "East Drive" 
* Luego vaya a la izquierda en la calle "Drive A", y luego vaya a la derecha en la estructura de estacionamiento 1 para estacionarse.
* Se puede estacionarse en cualquier espacio de visitante en el nivel superior. No tiene que pagar en el primer nivel en la caja de pago.
* Tome la banqueta al oeste desde la estructura del estacionamiento para entrar al edificio llamado "Edith O’Donnell Arts and Technology" (ATC) Ahorita no hay programas disponibles para inscripción abierta. No hay artículos en su carro de compras. Esta persona esta interesada en hacer voluntario en este sitio Este programa es disponible solamente para estudiantes de la escuela de Rosemont. Jueves, 3:45-5:30 Jueves, 3:45-5:30pm Titulo Para finalizar su registracion, por favor termine de llenar la informacion abajo. Para encontrar el edificio de "Edith O’Donnell Arts and Technology":

* Entre en el campo escolar vía "University Parkway"
* Vaya hacia la derecha a la calle "Armstrong Drive" y continúe al Norte  en la calle "East Drive" 
* Luego vaya a la izquierda en la calle "Drive A", y luego vaya a la derecha en la estructura de estacionamiento 1 para estacionarse.
* Se puede estacionarse en cualquier espacio de visitante en el nivel superior. No tiene que pagar en el primer nivel en la caja de pago.
* Tome la banqueta al oeste desde la estructura del estacionamiento para entrar al edificio llamado "Edith O’Donnell Arts and Technology" (ATC) Ingreso familiar total Veterano del militar de los EEUU Georgia Menos de $15,000 Actualizar Utah Vermont Ver Carro de Compras Ver Programas Ver ubicaciones Ver horario Virginia Washington Miércoles, 3:45 - 5:30 pm Miércoles, 3:45 - 5:45 pm Miércoles, 3:45 - 5:45 pm Miércoles, 4:30-6:00pm Miércoles, 4:30-6:30pm Miércoles, 4:30-6pm Miércoles, 5 - 7 pm Miércoles, 5pm - 7 pm West Virginia Caucásicos Wisconsin Talleres Talleres de otra organizacion Wyoming Si Tiene que estar de acuerdo con los terminos y condiciones. Pronto va a recibir un correo electrónico con los detalles de su registracion. Su contraseña ha cambiado. Joven Grande Joven Mediano Joven Pequeño Joven XL edad ejemplo: madre, padre, guardián legal, etc. ideaSpark horario 