from django.conf.urls import url
from dashboard import views


urlpatterns = [
    url('^$', views.home, name='dashboard-home'),
    url('^programs/(?P<program_id>[^/]+)/$',
        views.program_site, name='dashboard-program-site'),
    url('^programs/(?P<program_id>[^/]+)/sessions/(?P<session_id>[^/]+)/$',
        views.program_site, name='dashboard-program-site'),
    url('^programs/(?P<program_id>[^/]+)/sessions/(?P<session_id>[^/]+)/$',
        views.program_site, name='dashboard-program-site'),
    url('^programs/exit-tickets/(?P<exit_ticket_id>[^/]+)/$',
        views.exit_ticket_answers, name='dashboard-exit-ticket-answers'),
    url('^programs/(?P<program_id>[^/]+)/exit-tickets/(?P<exit_ticket_id>[^/]+)/$',
        views.exit_ticket_answers, name='dashboard-exit-ticket-answers'),
    url('^volunteers/(?P<volunteer_id>[^/]+)/$',
        views.volunteer_details, name='dashboard-volunteer-details'),
    url('^students/(?P<student_id>[^/]+)/$',
        views.student_details, name='dashboard-student-details'),
    url('^training-sessions/$',
        views.training_sessions_index, name='dashboard-training-sessions-index'),
    url('^training-sessions/(?P<session_id>[^/]+)/$',
        views.training_session, name='dashboard-training-session'),
    url('^create-account/$', views.create_account, name='create-account')
]
