from collections import OrderedDict
from datetime import timedelta

from django.contrib import messages
from django.db.models import Min
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils import timezone

from programs.models import (
    Program, Session, VolunteerAttendance, StudentAttendance, Student, Volunteer)
from volunteers.models import TrainingSession
from volunteers.views import profile_required, get_profile
from courses.models import ExitTicket, StudentExitTicket


def create_account(request):
    url = reverse('volunteers-profile')
    next = request.GET.get('next')
    if next:
        url += '?next=' + next
    return HttpResponseRedirect(url)


def home(request):
    if not request.user or not request.user.is_authenticated:
        return HttpResponseRedirect(reverse('volunteers-opportunities'))

    def view(request):
        volunteer = get_profile(request)

        # get upcoming programs, within a week "buffer" time
        time_ago = timezone.now() - timedelta(days=7)

        programs = Program.objects.with_semester() \
            .annotate(next_date=Min('sessions__date')) \
            .filter(sessions__date__gte=time_ago)

        # get upcoming training sessions
        training_sessions = volunteer.get_upcoming_training_sessions()
        show_all_sessions = volunteer.facilitated_training_sessions.all().count() > 0

        # get any pending requirements
        requirements = volunteer.requirements.filter(
            requirement_type__responsible_party='VOLUNTEER').pending()

        assignments = volunteer.assignments.filter(program__in=programs)

        context = {
            'show_all_programs': request.user.is_superuser and request.GET.get('show_all'),
            'requirements': requirements,
            'programs': programs.order_by('next_date'),
            'assignments': assignments,
            'training_sessions': training_sessions.distinct(),
            'show_all_sessions': show_all_sessions or request.user.is_superuser
        }
        return render(request, 'dashboard/home.html', context)

    return profile_required(view)(request)


@profile_required
def training_sessions_index(request):
    volunteer = get_profile(request)

    if request.user.is_superuser:
        past_sessions = TrainingSession.objects.filter(
            date__lt=timezone.now().date()
        )
    else:
        past_sessions = TrainingSession.objects.filter(
            date__lt=timezone.now().date(),
            facilitators=volunteer
        )

    context = {
        'upcoming_sessions': volunteer.get_upcoming_training_sessions(),
        'past_sessions': past_sessions.distinct()
    }

    return render(request, 'dashboard/training_sessions_index.html', context)


@profile_required
def training_session(request, session_id):
    volunteer = get_profile(request)
    session = get_object_or_404(TrainingSession, pk=session_id)

    if request.method == 'POST':
        session.attendees.clear()
        for volunteer_id in request.POST.getlist('volunteers'):
            session.attendees.add(int(volunteer_id))

    can_manage = request.user.is_superuser or volunteer in session.facilitators.all()
    context = {
        'session': session,
        'can_manage': can_manage
    }

    return render(request, 'dashboard/training_session.html', context)


@profile_required
def program_site(request, program_id, session_id=None):
    program = get_object_or_404(Program, pk=program_id)

    if session_id:
        session = Session.objects.get(pk=session_id)
    else:
        next_sessions = program.sessions.filter(date__gte=timezone.now().date()).order_by('date')
        if next_sessions.count() == 0:
            # if last session is over, just show the page for the last session
            session = program.sessions.order_by('-date')[0]
        else:
            session = next_sessions[0]

    # get existing attendances
    volunteer_attendances = OrderedDict()
    student_attendances = OrderedDict()

    for attendance in session.volunteer_attendances.all():
        volunteer_attendances[attendance.volunteer.pk] = attendance
    for attendance in session.student_attendances.all():
        student_attendances[attendance.student.pk] = attendance

    if session.type == 'PROGRAM':
        for student in program.students.order_by('first_name', 'last_name'):
            if not student_attendances.get(student.pk):
                student_attendances[student.pk] = StudentAttendance(
                    session=session, student=student)

    if request.method == 'POST':
        # Update existing attendances
        for id, attendance in volunteer_attendances.items():
            if request.POST.get('volunteer_{}_present'.format(id)):
                attendance.present = True
            else:
                attendance.present = False
            attendance.notes = request.POST.get('volunteer_{}_notes'.format(id))
            attendance.save()

        # Add any new attendances selected from dropdown
        for volunteer in program.volunteers.all():
            if volunteer.id in volunteer_attendances:
                continue
            if request.POST.get('volunteer_{}_present'.format(volunteer.id)):
                VolunteerAttendance(session=session, volunteer=volunteer, present=True).save()

        # Update student attendances
        for id, attendance in student_attendances.items():
            if request.POST.get('student_{}_present'.format(id)):
                attendance.present = True
            else:
                attendance.present = False
            attendance.notes = request.POST.get('student_{}_notes'.format(id))
            attendance.save()

        messages.add_message(request, messages.INFO, "Attendance saved.")
        return HttpResponseRedirect('.')

    sessions = [s.pk for s in program.sessions.all()]
    prev_session = None
    next_session = None
    i = sessions.index(session.pk)
    if i > 0:
        prev_session = sessions[i - 1]
    if i < len(sessions)-1:
        next_session = sessions[i + 1]

    # Get exit ticket results
    exit_ticket = None
    student_exit_tickets = []
    try:
        exit_ticket = ExitTicket.objects.get(course=program.course, session_number=session.number)
        for student in student_attendances:
            student_ticket = exit_ticket.get_student_best(student)
            if student_ticket:
                student_exit_tickets.append(student_ticket)
    except ExitTicket.DoesNotExist:
        pass

    other_volunteers = program.volunteers.exclude(pk__in=volunteer_attendances.keys())

    context = {
        'prev_session': prev_session,
        'next_session': next_session,
        'program': program,
        'session': session,
        'volunteer_attendances': volunteer_attendances,
        'student_attendances': student_attendances,
        'other_volunteers': other_volunteers,
        'exit_ticket': exit_ticket,
        'student_exit_tickets': student_exit_tickets
    }
    return render(request, 'dashboard/program_site.html', context)


@profile_required
def exit_ticket(request, program_id, student_exit_ticket_id):
    answers = StudentExitTicket.objects.get(pk=student_exit_ticket_id)
    context = {
        'answers': answers
    }
    return render(request, 'dashboard/exit_ticket_result.html', context)


@profile_required
def volunteer_details(request, volunteer_id):
    volunteer = get_object_or_404(Volunteer, pk=volunteer_id)
    context = {
        'volunteer': volunteer
    }
    return render(request, 'dashboard/volunteer_details.html', context)


@profile_required
def student_details(request, student_id):
    student = get_object_or_404(Student, pk=student_id)
    guardian = student.account.default_contact
    attendances = student.session_attendances.order_by('-session__date')
    context = {
        'student': student,
        'guardian': guardian,
        'attendances': attendances
    }
    return render(request, 'dashboard/student_details.html', context)


@profile_required
def exit_ticket_answers(request, exit_ticket_id, program_id=None):
    exit_ticket = get_object_or_404(ExitTicket, pk=exit_ticket_id)

    program = None
    if program_id:
        program = get_object_or_404(Program, pk=program_id)

    correct_answers = [q.correct_answers_set for q in exit_ticket.questions.all()]
    team = None
    if program:
        team = program.students.all()
    results = exit_ticket.build_results(correct_answers, team)

    def get_color(percent):
        # material red, orange, green 100
        if percent < 33:
            return '#ffcdd2'
        if percent > 66:
            return '#c8e6c9'
        return '#ffe0b2'

    for result in results:
        result['global_results']['color'] = get_color(result['global_results']['percent'])
        if result.get('team_results'):
            result['team_results']['color'] = get_color(result['team_results']['percent'])

    exit_tickets = [t.pk for t in exit_ticket.course.exit_tickets.all()]

    next_exit_ticket = None
    prev_exit_ticket = None
    prev_url = None
    next_url = None

    i = exit_tickets.index(exit_ticket.pk)
    if i > 0:
        prev_exit_ticket = exit_tickets[i - 1]
    if i < len(exit_tickets)-1:
        next_exit_ticket = exit_tickets[i + 1]

    if next_exit_ticket:
        kwargs = {'exit_ticket_id': next_exit_ticket}
        if program:
            kwargs['program_id'] = program.id
        next_url = reverse('dashboard-exit-ticket-answers', kwargs=kwargs)

    if prev_exit_ticket:
        kwargs = {'exit_ticket_id': prev_exit_ticket}
        if program:
            kwargs['program_id'] = program.id
        prev_url = reverse('dashboard-exit-ticket-answers', kwargs=kwargs)

    context = {
        'program_id': program and program.id or None,
        'prev_url': prev_url,
        'next_url': next_url,
        'exit_ticket': exit_ticket,
        'exit_ticket_results': results
    }
    return render(request, 'dashboard/exit_ticket_answers.html', context)
