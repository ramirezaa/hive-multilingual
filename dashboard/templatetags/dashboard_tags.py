from django.template import Library
from django.urls import reverse
from django.contrib.staticfiles.templatetags.staticfiles import static
from easy_thumbnails.files import get_thumbnailer
from programs.models import StudentAttendance, VolunteerAttendance

register = Library()

THUMB_SIZE = 42


@register.inclusion_tag('dashboard/includes/attendance_item.html')
def attendance_item(attendance):
    extra_info = ''

    if isinstance(attendance, StudentAttendance):
        id = attendance.student.id
        attendance_type = 'student'
        name = str(attendance.student)
        photo = attendance.student.photo
        details_url = reverse('dashboard-student-details',
                              kwargs={'student_id': attendance.student.pk})
        extra_info = attendance.student.grade_name
        if attendance.student.grade > 0 and attendance.student.grade <= 12:
            extra_info += ' grade'

    elif isinstance(attendance, VolunteerAttendance):
        attendance_type = 'volunteer'
        id = attendance.volunteer.id
        name = str(attendance.volunteer)
        photo = attendance.volunteer.photo
        details_url = reverse('dashboard-volunteer-details',
                              kwargs={'volunteer_id': attendance.volunteer.pk})

    else:
        raise ValueError("Expected `VolunteerAttendance` or `StudentAttendance` instance")

    if photo:
        thumbnailer = get_thumbnailer(photo)
        thumb_opts = {
            'size': (42, 42),
            'crop': True
        }
        thumb_url = thumbnailer.get_thumbnail(thumb_opts).url
    else:
        thumb_url = static('hive/images/generic-user.png')

    return {
        'type': attendance_type,
        'id': id,
        'name': name,
        'details_url': details_url,
        'thumb_url': thumb_url,
        'thumb_size': THUMB_SIZE,
        'present': attendance.present,
        'notes': attendance.notes,
        'extra_info': extra_info,
    }


@register.inclusion_tag('dashboard/includes/attendance_item.html')
def attendance_item_template(attendance_type):
    # See `onAddFloaterClick()` in dashboard/program_site.html for replacement tags
    return {
        'type': attendance_type,
        'id': '__id__',
        'name': '__name__',
        'details_url': '__profile_url__',
        'thumb_url': '__photo__',
        'thumb_size': THUMB_SIZE
    }
