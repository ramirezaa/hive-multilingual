(function($) {
  var otherVolunteers = window.VOLUNTEERS;

  function toggleItem($item, expand) {
    if (typeof(expand) === 'undefined') {
      expand = !($item.attr('data-expanded') == 'true');
    }
    console.log(expand);
    $item.attr('data-expanded', expand ? 'true' : 'false');
  }

  function onMoreBtnClick() {
    var $item = $(this).parents('.attendance-item');
    toggleItem($item);

    // hide all other items
    $(this).parents('ul').find('.attendance-item[data-expanded="true"]').each(function() {
      var $otherItem = $(this);
      if ($otherItem.attr('id') != $item.attr('id')) {
        toggleItem($(this), false);  
      }
    });
  }

  // otherVolunteers functionality
  function onAddVolunteerClick() {
    var id = $(this).data('volunteer-id');
    if ($('#id_volunteer_' + id + '_present').length) {
      return;
    }
    var cloneHtml = $('#otherVolunteerTemplate').text();
    cloneHtml = cloneHtml.replace(/__id__/g, id);
    cloneHtml = cloneHtml.replace(/__name__/g, otherVolunteers[id].name);
    cloneHtml = cloneHtml.replace(/__profile_url__/g, otherVolunteers[id].url);
    cloneHtml = cloneHtml.replace(/__photo__/g, otherVolunteers[id].photo);
    var el = $(cloneHtml).insertBefore('.btn-row');
    $('#id_volunteer_' + id + '_present').on('change', function() {
      if (!this.checked) {
        $(this).parents('li').remove();
      }
    }).attr('checked', true);
  }

  function onShowTab(elements) {
    if (history.pushState) {
      window.history.pushState(null, null, '#' + elements[0].id);
    } else {
      window.location.hash = '#!' + elements[0].id;
    }
  }

  function onHashChange(event) {
    var hash = window.location.hash.replace(/^#!?/, '') || 'attendance';
    if (hash) {
      $('ul.tabs').tabs('select_tab', hash);
    }
    // update prev/next links
    $('.session-nav a').each(function(i, el) {
      var href = $(el).attr('href').replace(/#.*$/, '');
      $(el).attr('href', href + '#' + hash);
    });
  }

  $(document).ready(function() {
    // hook things up
    $('.modal').modal();
    $('.dropdown-button').dropdown();
    $('.add-volunteer').on('click', onAddVolunteerClick);
    $('ul.tabs').tabs({'onShow': onShowTab});
    $('.more-btn').on('click', onMoreBtnClick);
    window.addEventListener('hashchange', onHashChange);
    onHashChange();
  });
})(jQuery);
