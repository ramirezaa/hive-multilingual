class ContactAdminMixin:
    fieldsets = (
        ('Basic info', {'fields': (
            'first_name', 'last_name', 'email', 'phone', 'alt_phone'
        )}),
        ('Address', {'fields': (
            'address1', 'address2', 'city', 'state', 'postal_code'
        )}),
        ('Other info', {'fields': (
            'birth_date', 'company', 'notes'
        )})
    )
