RACE_CHOICES = (
    ('NATIVE', 'American Indian or Alaskan Native'),
    ('ASIAN', 'Asian'),
    ('BLACK', 'Black or African American'),
    ('PAC_ISLANDER', 'Hawaiian or Pacific Islander'),
    ('WHITE', 'White'),
    ('TWO_OR_MORE', 'Two or More Races'),
    ('OTHER', 'Other')
)

ETHNICITY_CHOICES = (
    ('HISPANIC', 'Hispanic or Latina/Latino'),
    ('NOT_HISPANIC', 'Not Hispanic or Latina/Latino'),
)

GENDER_CHOICES = (
    ('MALE', 'Male'),
    ('FEMALE', 'Female')
)

MILITARY_STATUS_CHOICES = (
    ('ENLISTED', 'Enlisted in the US militiary guard/reserve'),
    ('VETERAN', 'US military veteran'),
)
