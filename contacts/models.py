from django.db import models
from django.conf import settings
from django.contrib.gis.db.models import PointField
from django.utils.translation import gettext_lazy as _

from localflavor.us import models as lf_models
from hive import utils
from . import constants


class Person(models.Model):
    """
    Abstract model with fields used for any person in the system
    """
    first_name = models.CharField(_('First name'), max_length=255)
    middle_name = models.CharField(_('Middle name'), max_length=255, blank=True)
    last_name = models.CharField(_('Last name'), max_length=255)
    photo = models.ImageField(_('Photo'), blank=True, upload_to='person-photos')

    class Meta:
        abstract = True

    @property
    def name(self):
        return self.first_name + ' ' + self.last_name

    def clean(self):
        super().clean()
        self.first_name = self.first_name.title()
        self.middle_name = self.first_name.title()
        self.last_name = self.last_name.title()


class DemographicsModel(models.Model):
    """
    Abstract model with fields for race, ethnicity, and gender
    """
    race = models.CharField(
        _('Race'), max_length=30, choices=constants.RACE_CHOICES, blank=True)
    race_other = models.CharField(max_length=100, blank=True)
    ethnicity = models.CharField(
        _('Ethnicity'), max_length=30, choices=constants.ETHNICITY_CHOICES, blank=True)
    gender = models.CharField(
        _('Gender'), max_length=30, choices=constants.GENDER_CHOICES, blank=True)

    class Meta:
        abstract = True


class Contact(Person):
    """
    A person in the database with various contact info fields
    """
    date_added = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    birth_date = models.DateField(_('Birth date'), null=True, blank=True)
    email = models.EmailField(_('Email'), null=True, blank=True)
    address1 = models.CharField(_('Address line 1'), max_length=255, blank=True)
    address2 = models.CharField(_('Address line 2'), max_length=255, blank=True)
    city = models.CharField(_('City'), max_length=255, blank=True)
    state = lf_models.USStateField(_('State'), blank=True)
    postal_code = lf_models.USZipCodeField(_('Postal code'), blank=True)
    company = models.CharField(
        _('Company/employer'), max_length=255, blank=True)
    title = models.CharField(_('Title'), max_length=255, blank=True)
    notes = models.TextField(_('Notes'), blank=True)
    phone = lf_models.PhoneNumberField(_('Phone'), blank=True)
    alt_phone = lf_models.PhoneNumberField(_('Alt. phone'), blank=True)
    military_status = models.CharField(
        _('Military status'), max_length=255, blank=True,
        choices=constants.MILITARY_STATUS_CHOICES)
    geo = PointField(null=True, blank=True)
    preferred_language = models.CharField(
        _('Preferred language'), max_length=8, choices=settings.LANGUAGES,
        default=settings.LANGUAGE_CODE)

    @property
    def age(self):
        return utils.get_age(self.birth_date)

    @property
    def formatted_address(self):
        if not self.address1:
            return ''
        address = self.address1
        if self.address2:
            address += "\n" + self.address2
        return "{}\n{}, {} {}".format(
            address, self.city, self.state, self.postal_code)

    def clean(self):
        super().clean()
        self.address1 = self.address1.title()
        self.address2 = self.address2.title()
        self.city = self.city.title()
        self.company = self.company.title()

    def save(self, *args, **kwargs):
        self.geo = utils.geocode('{} {}, {}, {} {}'.format(
            self.address1, self.address2, self.city, self.state,
            self.postal_code))
        super().save(*args, **kwargs)

    def __str__(self):
        return '{0.first_name} {0.last_name}'.format(self)
