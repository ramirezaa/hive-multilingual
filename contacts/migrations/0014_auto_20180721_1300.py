# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-07-21 18:00
from __future__ import unicode_literals

from django.db import migrations, models
import localflavor.us.models


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0013_contact_preferred_language'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='alt_phone',
            field=localflavor.us.models.PhoneNumberField(blank=True, max_length=20, verbose_name='Alt. phone'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='birth_date',
            field=models.DateField(blank=True, null=True, verbose_name='Birth date'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='city',
            field=models.CharField(blank=True, max_length=255, verbose_name='City'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='email',
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name='Email'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='first_name',
            field=models.CharField(max_length=255, verbose_name='First name'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='last_name',
            field=models.CharField(max_length=255, verbose_name='Last name'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='middle_name',
            field=models.CharField(blank=True, max_length=255, verbose_name='Middle name'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='military_status',
            field=models.CharField(blank=True, choices=[('ENLISTED', 'Enlisted in the US militiary guard/reserve'), ('VETERAN', 'US military veteran')], max_length=255, verbose_name='Military status'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='notes',
            field=models.TextField(blank=True, verbose_name='Notes'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='phone',
            field=localflavor.us.models.PhoneNumberField(blank=True, max_length=20, verbose_name='Phone'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='photo',
            field=models.ImageField(blank=True, upload_to='person-photos', verbose_name='Photo'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='postal_code',
            field=localflavor.us.models.USZipCodeField(blank=True, max_length=10, verbose_name='Postal code'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='preferred_language',
            field=models.CharField(choices=[('en', 'English'), ('es', 'Spanish')], default='en', max_length=8, verbose_name='Preferred language'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='state',
            field=localflavor.us.models.USStateField(blank=True, max_length=2, verbose_name='State'),
        ),
        migrations.AlterField(
            model_name='contact',
            name='title',
            field=models.CharField(blank=True, max_length=255, verbose_name='Title'),
        ),
    ]
