# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-19 22:41
from __future__ import unicode_literals

from django.db import migrations


def forward(apps, schema_editor):
    from hive.utils import geocode
    Contact = apps.get_model('contacts', 'Contact')
    for contact in Contact.objects.all():
        contact.geo = geocode('{} {}, {}, {} {}'.format(
            contact.address1, contact.address2, contact.city, contact.state,
            contact.postal_code))
        contact.save()


def backward(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0011_contact_geo'),
    ]

    operations = [
        migrations.RunPython(forward, backward)
    ]
