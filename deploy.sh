#!/bin/sh
[[ -n "$1" ]] && ENV="$1";
[[ -z "$ENV" ]] && echo "Please specify an environment" && exit 1;

# Only production env for now. In the future we might add a testing environment.
case $ENV in
    prod|production)
        BRANCH="production";
        HOST="hive.boldidea.org";
        DIR="/srv/hive";;
    test|testing|staging)
        BRANCH="testing";
        HOST="hive.boldidea.org";
        DIR="/srv/hive-testing";;
esac

REPO='git@bitbucket.org:boldidea/hive.git';
NAME='hive';

UPDATE_CMD="set -o pipefail && ";
UPDATE_CMD+="$DIR/bin/pip install -e git+$REPO@$BRANCH#egg=$NAME ";
UPDATE_CMD+="| grep -v 'Requirement already satisfied'";
DEPLOY_CMD="$DIR/bin/manage.py migrate && ";
DEPLOY_CMD+="$DIR/bin/manage.py collectstatic --noinput && ";
DEPLOY_CMD+="touch $DIR/reload";

echo -e "Updating source...\n> $UPDATE_CMD" &&
ssh -A deploy@$HOST "$UPDATE_CMD" &&
git tag "${ENV}_$(date +%Y-%m-%d_%H%M%S)" && git push --tags &&
echo -e "\n\nDeploying...\n> $DEPLOY_CMD" &&
ssh -A deploy@$HOST "$DEPLOY_CMD" &&
echo -e "\n\nDone.";
