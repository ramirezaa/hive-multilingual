from django import forms
from django.utils.safestring import mark_safe


def add_class(field, class_name):
    class_str = field.widget.attrs.get('class', '')
    class_list = list(set(class_str.split(' ')))
    class_list.append(class_name)
    field.widget.attrs['class'] = ' '.join(class_list)


class BaseForm:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.update_fields()

    def update_fields(self):
        for name, field in self.fields.items():
            # add validate class to all fields
            add_class(field, 'validate')

            # add materialize class for textareas
            if isinstance(field.widget, forms.Textarea):
                add_class(field, 'materialize-textarea')

            if isinstance(field.widget, forms.DateInput):
                add_class(field, 'date-picker')
                field.localize = True
                field.widget.format = '%m/%d/%Y'

            # add asteristk to all required fields
            if not field.label:
                field.label = ' '.join(name.split('_')).capitalize()
            if field.required and 'class="required"' not in field.label:
                field.label += '<span class="required">*</span>'
            field.label = mark_safe(field.label)

    def full_clean(self):
        super().full_clean()
        for name, field in self.fields.items():
            # add invalid and data-error attr to fields with errors
            errors = self.errors.get(name)
            if errors:
                add_class(field, 'invalid')
