import json
import sqlparse
import datetime
import decimal
from urllib import request
from urllib.parse import quote_plus
from django.contrib.admin.utils import NestedObjects
from django.contrib.gis.geos import Point
from django.db import models
from django.utils import timezone
from django.utils.encoding import smart_str, force_text
from django.utils.text import capfirst
from programs.utils import get_semester  # noqa
from django.utils.translation import gettext_lazy as _

def get_age(date, as_of=None):
    if date is None:
        return None
    if as_of is None:
        as_of = timezone.now().date()
    return (as_of - date) // datetime.timedelta(days=365.2425)


def ordinal(n):
    s = "tsnrhtdd"[(n//10 % 10 != 1) * (n % 10 < 4) * n % 10::4]
    return "{}{}".format(n, s)


def geocode(location):
    location = quote_plus(smart_str(location))
    url = ('http://maps.googleapis.com/maps/api/geocode/json?address={}'
           '&sensor=false').format(location)
    response = request.urlopen(url).read().decode('utf-8')
    result = json.loads(response)
    if result['status'] == 'OK':
        lat = str(result['results'][0]['geometry']['location']['lat'])
        lng = str(result['results'][0]['geometry']['location']['lng'])
        return Point(float(lat), float(lng))
    else:
        return None


def get_age_range_description(**kwargs):
    min_grade = kwargs.get('min_grade')
    max_grade = kwargs.get('max_grade')
    min_age = kwargs.get('min_age')
    max_age = kwargs.get('max_age')
    grades_text = _("Grades")
    ages_text = _("Ages")
    if min_grade and max_grade:
        return grades_text+" {} - {}".format(min_grade, max_grade)
    elif min_grade:
        return grades_text+" {} and up".format(min_grade)
    elif max_grade:
        return grades_text+" {} and under".format(max_grade)
    elif min_age and max_age:
        return ages_text+" {} - {}".format(min_age, max_age)
    elif min_age:
        return ages_text+" {} and up".format(min_age)
    elif max_age:
        return ages_text+" {} and under".format(min_age)
    return ''


def preview_delete(objs, using='default'):
    collector = NestedObjects(using=using)
    collector.collect(objs)

    def format(obj):
        return (capfirst(obj._meta.verbose_name), force_text(obj))

    return collector.nested(format)


def format_sql(query):
    if isinstance(query, models.QuerySet):
        query = str(query.query)
    return sqlparse.format(query, reindent=True, keyword_case='upper')


def print_sql(query, nocolor=False):
    """
    Prints a formatted sql string or QuerySet
    """
    sql = format_sql(query)
    if nocolor:
        return sql
    from pygments import highlight, lexers, formatters
    lexer = lexers.get_lexer_by_name('postgresql')
    print(highlight(sql, lexer, formatters.TerminalFormatter()))


def json_defaults(obj):
    if isinstance(obj, (datetime.date, datetime.datetime)):
        return obj.isoformat()
    if isinstance(obj, models.Model):
        return obj.pk
    if isinstance(obj, decimal.Decimal):
        return str(obj)
    raise TypeError("{} is not JSON serializable".format(repr(obj)))
