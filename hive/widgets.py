from django.contrib.staticfiles.storage import staticfiles_storage
from django.forms import FileInput


class ProfilePhotoInput(FileInput):
    def __init__(self, *args, **kwargs):
        self.instance_count = 0
        self.message_default = kwargs.pop('message_default', None)
        self.message_replace = kwargs.pop('message_replace',
                                          self.message_default)
        super().__init__(*args, **kwargs)

    def render(self, name, value, attrs=None):
        classes = set(attrs.get('class', '').split(' '))
        classes.add('dropify')
        if value and hasattr(value, 'url'):
            url = value.url
        else:
            url = staticfiles_storage.url('hive/images/generic-user.png')
        attrs = {
            'data-max-file-size': '512M',
            'data-min-height': '200',
            'data-min-width': '200',
            'data-height': '175',
            'data-show-remove': 'false',
            'data-default-file': url,
            'data-message-default': self.message_default,
            'data-message-replace': self.message_replace
        }
        attrs['class'] = ' '.join(classes)
        return super().render(name, value, attrs)
