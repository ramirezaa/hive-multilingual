"""
WSGI config for hive project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application
from dotenv import load_dotenv

# Load environment from config.env file
ENV_FILE = os.environ.get('ENV_FILE', os.path.join(sys.prefix, 'config.env'))
ENV_DIR = os.path.dirname(ENV_FILE)
load_dotenv(ENV_FILE, override=False)

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hive.settings")

application = get_wsgi_application()
