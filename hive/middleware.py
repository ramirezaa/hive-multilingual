from django.conf import settings
from django.utils.translation import activate


class AdminLanguageMiddleware(object):
    """
    Use this middleware to ensure the admin is always running under the
    default language, to prevent vinaigrette from clobbering the registered
    fields with the user's picked language in the change views. Aslo make
    sure that this is after any LocaleMiddleware like classes.
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def is_admin_request(self, request):
        """
        Returns True if this request is for the admin views.
        """
        return request.path.startswith('/admin/')

    def __call__(self, request):
        response = self.get_response(request)

        if not self.is_admin_request(request):
            # We are in the admin site
            activate(settings.LANGUAGE_CODE)

        return response
