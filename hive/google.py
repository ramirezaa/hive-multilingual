import hashlib
import logging
from apiclient.discovery import build
from django.conf import settings
from httplib2 import Http
from oauth2client.service_account import ServiceAccountCredentials
from students.utils import generate_kidsafe_password
from googleapiclient.errors import HttpError

logger = logging.getLogger(__name__)


class UserExists(Exception):
    pass


def get_directory_admin_service():
    scopes = [
        'https://www.googleapis.com/auth/admin.directory.user'
    ]
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        settings.GOOGLE_APPLICATION_CREDENTIALS, scopes=scopes)
    credentials = credentials.create_delegated(settings.GOOGLE_ADMIN_ACCOUNT)
    http_auth = credentials.authorize(Http())
    return build('admin', 'directory_v1', http=http_auth)


def hash_password(password):
    hash = hashlib.sha1()
    hash.update(password.encode('utf8'))
    return hash.hexdigest()


def create_or_update_student_account(login):
    if not login.username:
        login.username = '{}.{}@{}'.format(
            login.student.first_name.split(' ')[0].lower(),
            login.student.last_name.split(' ')[-1].lower(),
            settings.GOOGLE_STUDENT_ACCOUNT_DOMAIN
        )

    if not login.password:
        login.password = generate_kidsafe_password()

    user_account = get_user_account(login.username)

    kwargs = {
        'org': settings.GOOGLE_STUDENT_ORG_PATH,
        'email': login.username,
        'password': login.password,
        'first_name': login.student.first_name,
        'last_name': login.student.last_name
    }

    if not user_account:
        return create_user_account(**kwargs)
    else:
        return update_user_account(**kwargs)


def create_user_account(email, password, first_name, last_name, org='/'):
    service = get_directory_admin_service()
    body = {
        'orgUnitPath': org,
        'primaryEmail': email,
        'name': {
            'familyName': last_name,
            'givenName': first_name,
        },
        'password': hash_password(password),
        'hashFunction': 'SHA-1',
        'changePasswordAtNextLogin': False
    }
    try:
        logger.info('Creating user: {}'.format(body))
        service.users().insert(body=body).execute()
    except HttpError as e:
        if e.resp.status == 409:
            raise UserExists()
        raise


def get_user_account(email):
    service = get_directory_admin_service()
    query = 'email={}'.format(email)
    result = service.users().list(customer='my_customer', query=query).execute()
    users = result.get('users')
    if users:
        return users[0]


def update_user_account(email, **kwargs):
    body = {}
    if kwargs.get('org'):
        body['orgUnitPath'] = kwargs['org']
    if kwargs.get('new_email'):
        body['primaryEmail'] = email
    if kwargs.get('first_name') or kwargs.get('last_name'):
        body['name'] = {}
        if kwargs.get('first_name'):
            body['name']['givenName'] = kwargs['first_name']
        if kwargs.get('last_name'):
            body['name']['familyName'] = kwargs['last_name']
    if kwargs.get('password'):
        body['password'] = hash_password(kwargs['password'])
        body['hashFunction'] = 'SHA-1'
        body['changePasswordAtNextLogin'] = False
    service = get_directory_admin_service()
    logger.info('Updating user: {}'.format(body))
    return service.users().update(userKey=email, body=body).execute()
