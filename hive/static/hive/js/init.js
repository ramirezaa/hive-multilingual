(function($){
  $(document).ready(function() {
    $('.button-collapse').sideNav();
    $('select:not(.hidden)').material_select();

    // fields with data-placeholder only show when focused
    $('input[data-placeholder]').each(function() {
      var $this = $(this);
      $this.removeAttr('placeholder');
      $this.on('focus', function() {
        $this.attr('placeholder', $this.data('placeholder'));
      }).on('blur', function() {
        $this.removeAttr('placeholder');
        Materialize.updateTextFields();
      });
    });
  });
})(jQuery);

/* CustomEvent polyfill */
(function () {
  if (typeof window.CustomEvent === 'function') return false;
  function CustomEvent (event, params) {
    params = params || {bubbles: false, cancelable: false, detail: undefined};
    var evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
    return evt;
  }
  CustomEvent.prototype = window.Event.prototype;
  window.CustomEvent = CustomEvent;
})();
