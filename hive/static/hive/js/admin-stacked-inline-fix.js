(function($) {
  function updateFormsetForm(form) {
    var $form = $(form);
    var prefix = $form.parents('[data-formset-prefix]').data('formsetPrefix');
    $form.removeClass('l4');
    $form.removeClass('m6');
  }
  $(document).ready(function() {
    $('[data-formset-prefix]').on('formAdded', function(event) {
      updateFormsetForm(event.target);
    });
    $('[data-formset-form]').each(function(i, form) {
      updateFormsetForm(form);
    });
  });
})(django.jQuery);
