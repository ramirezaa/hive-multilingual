(function($) {
  // remove unresolved attribute on any webcomponents
  window.addEventListener('WebComponentsReady', function(e) {
    $('[unresolved]').removeAttr('unresolved');
  });
  var currentField;
  var datePicker = $('#datePickerDialog paper-date-picker')[0];
  var dialog = $('#datePickerDialog')[0];
  dialog.addEventListener('iron-overlay-closed', function(event) {
    if (currentField && event.detail.confirmed) {
      var date = datePicker.date;
      currentField.value = datePicker.dateFormat(date, 'MM/DD/YYYY');
      currentField = null;
      Materialize.updateTextFields();
    }
  });
  $(document).ready(function() {
    $('.date-picker').each(function(i, el) {
      $('<i class="material-icons">today</i>')
        .insertAfter(el)
        .on('click', function() {
          datePicker.date = moment(el.value).toDate(); 
          datePicker._selectPage('chooseDate');
          currentField = el;
          dialog.toggle();
        });
    });
  });
})(jQuery);
