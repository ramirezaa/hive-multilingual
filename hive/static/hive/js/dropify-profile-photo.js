$(document).ready(function() {
  function updateImage(el, src) {
    var $parent = $(el).parents('.dropify-wrapper');
    var $renderer = $parent.find('.dropify-render');
    if (!src) {
      src = $renderer.find('img').attr('src');
    }
    $renderer.css({
      'background-image': 'url(' + src + ')'
    });
  }
  $('.dropify').each(function(i, el) {
    var $el = $(el);
    $el.dropify({
      messages: {
        'default': $el.data('messageDefault'),
        'replace': $el.data('messageReplace')
      }
    });
  }).on('dropify.fileReady', function(event, _, src) {
    updateImage(this, src);
  });
  $('.dropify').each(function(i, el) {
    updateImage(el);
  });
});
