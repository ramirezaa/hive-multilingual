from django import template
from material.admin.base import Inline
from material import Layout

import json as _json

register = template.Library()


@register.filter
def json(obj):
    return _json.dumps(obj, indent=2)


@register.filter
def is_list(obj):
    if isinstance(obj, list):
        return True
    return False


@register.assignment_tag
def inline_fieldset_layout(adminform, inline_admin_formsets):
    inlines = [Inline(inline) for inline in inline_admin_formsets]
    return Layout(*inlines)
