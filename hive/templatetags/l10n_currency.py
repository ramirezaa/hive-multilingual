import locale
from django.template import Library

register = Library()


@register.filter
def currency(value):
    locale.setlocale(locale.LC_ALL, '')
    return locale.currency(value)
