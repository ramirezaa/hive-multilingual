import re
from django import template
from django.utils.safestring import mark_safe
from markdown import markdown

register = template.Library()


@register.filter(name='markdown')
def render_markdown(text, inline=False):
    html = markdown(text, extensions=['markdown.extensions.codehilite', 'urlize'])
    if inline:
        html = re.sub(r'^<p>(.*?)</p>', r'\1', html)
    return mark_safe(html)
