import re

from django import template
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape
from markdown import markdown

register = template.Library()


@register.simple_tag(name='field_label')
def field_label(field, label=None):
    if not field:
        raise template.TemplateSyntaxError('Invalid field')
    data_error = ''
    if hasattr(field, 'errors') and len(field.errors) > 0:
        error = conditional_escape(field.errors[0])
        data_error = ' data-error="{}"'.format(error)
    label = label or field.label
    label = re.sub(r'<p>(.*?)</p>', r'\1', markdown(label))
    return mark_safe('<label for="{}"{}>{}</label>'.format(
        field.id_for_label, data_error, label or field.label))


@register.inclusion_tag('hive/includes/field.html', takes_context=True)
def field(context, form_field):
    # KLUDGE: https://code.djangoproject.com/ticket/26041
    context = context.flatten()
    context['form_field'] = form_field
    return context


@register.inclusion_tag('hive/includes/minimal_error_list.html')
def minimal_error_list(*forms, only=None, show_required_fields=False):
    """
    Renders a flat error list showing only as much information as is needed.
    If a field has multiple errors, only the first error is shown. With no
    arguments both non-field and field errors are shown.

    By default, if any required fields are empty, a single "Complete all
    required fields" error will be given. If `show_required_fields` is `True`,
    individual field errors will be shown.

        {% error_list show_required_fields=True %}

    Pass `only="field"` or `only="non_field" to show only fields of that type.

        {% error_list only="non_field" %}
    """
    has_errors = False
    field_errors = []
    required_field_errors = []

    for form in forms:
        if form.errors:
            has_errors = True

        if not only == 'field':
            non_field_errors = form.non_field_errors

        if not only == 'non_field':
            for field in form:
                if not field.errors:
                    continue
                err = field.errors[0]
                if err == field.field.error_messages['required']:
                    required_field_errors.append(field)
                    if not show_required_fields:
                        continue
                field_errors.append((field, err))

    return {
        'has_errors': has_errors,
        'only': only,
        'field_errors': field_errors,
        'required_field_errors': required_field_errors,
        'non_field_errors': non_field_errors,
        'show_required_fields': show_required_fields
    }

