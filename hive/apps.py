from importlib import import_module
from django.conf import settings
from django.contrib.admin.apps import SimpleAdminConfig


class HiveAdminConfig(SimpleAdminConfig):
    def ready(self):
        super().ready()
        for app in settings.ADMIN_APPS:
            import_module('{}.admin'.format(app))
