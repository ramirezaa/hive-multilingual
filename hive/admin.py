import csv
from collections import OrderedDict
from django import http
from django.contrib import admin
from django.db.models import Count, Subquery, OuterRef, Q, Value, CharField
from django.shortcuts import render
from django.utils import timezone
from django.views.decorators.cache import never_cache

from programs import utils


class HiveAdminSite(admin.AdminSite):
    @never_cache
    def index(self, request, extra_context=None):
        from programs.models import Program
        extra_context = extra_context or {}
        # Get any programs open for volunteer sign-ups OR student registration
        reg_open_programs = Program.objects.open_for_registration()
        volunteer_programs = Program.objects.filter(volunteer_opportunities__status='PUBLISHED')
        programs = reg_open_programs.union(volunteer_programs)
        extra_context['programs'] = programs
        return super().index(request, extra_context)

    def get_urls(self):
        from django.conf.urls import url
        return super().get_urls() + [
            url('^map/$', self.admin_view(self.map_view), name='map'),
            url('^student-history/$', self.admin_view(self.student_history_view),
                name='student-history'),
            url('^tshirts/$', self.admin_view(self.tshirts_view), name='tshirts'),
            url('^tshirts/download/$', self.admin_view(self.tshirts_csv_view),
                name='tshirts-download')
        ]

    def map_view(self, request, *args, **kwargs):
        from programs.models import Program, Session, Location
        from students.models import Student
        from volunteers.models import Volunteer

        show = request.GET.getlist('show') or ['p', 's', 'v']

        # Get semester dropdown options
        sessions = Session.objects.all().with_semester()
        sessions = sessions.values('year', 'semester').distinct()
        semester_options = []
        for item in sessions.order_by('-year', 'semester'):
            semester_options.append((
                '{}-{}'.format(item['year'], item['semester']),
                '{} {}'.format(item['semester'].title(), item['year'])
            ))

        if request.GET.get('semester'):
            year, semester = request.GET['semester'].split('-')
        else:
            today = timezone.now()
            year = today.year
            semester = utils.get_semester(today)

        all_locations = set()
        for program in Program.objects.for_semester(year, semester):
            all_locations.add(program.location)

        locations = all_locations
        if request.GET.getlist('locations'):
            locations = Location.objects.filter(pk__in=request.GET.getlist('locations'))

        students = Student.objects.none()
        if 's' in show:
            students = Student.objects.for_semester(year, semester).exclude(
                account__default_contact__geo=None)
            if locations:
                students = students.filter(registrations__program__location__in=locations)

        volunteers = Volunteer.objects.none()
        if 'v' in show:
            volunteers = Volunteer.objects.for_semester(year, semester).exclude(geo=None)
            if locations:
                volunteers = volunteers.filter(signups__program__location__in=locations)

        programs = Program.objects.none()
        if 'p' in show:
            programs = Program.objects.for_semester(year, semester).exclude(location__geo=None)
            if locations:
                programs = programs.filter(location__in=locations)

        context = {
            'all_locations': all_locations,
            'locations': locations,
            'semester_options': semester_options,
            'year': year,
            'semester': semester,
            'year_semester': '{}-{}'.format(year, semester),
            'show': show,
            'programs': programs,
            'students': students,
            'volunteers': volunteers,
        }
        return render(request, 'admin/map.html', context)

    def tshirts_view(self, request, *args, **kwargs):
        from programs.models import Student
        from volunteers.models import Volunteer
        from students.constants import SHIRT_SIZE_CHOICES as student_shirt_sizes
        from volunteers.constants import SHIRT_SIZE_CHOICES as volunteer_shirt_sizes

        request.current_app = self.name

        if request.GET.get('semester'):
            year, semester = request.GET['semester'].split('-')
        else:
            today = timezone.now()
            year = today.year
            semester = utils.get_semester(today)

        # for sorting sizes
        sizes = OrderedDict(student_shirt_sizes)
        sizes.update(volunteer_shirt_sizes)
        sizes = list(sizes.keys())

        students = (
            Student.objects.filter(pk__in=Student.objects.for_semester(year, semester))
            .values('shirt_size')
            .annotate(total=Count('shirt_size'))
            .order_by()
        )
        volunteers = (
            Volunteer.objects.filter(pk__in=Volunteer.objects.for_semester(year, semester))
            .values('shirt_size')
            .annotate(total=Count('shirt_size'))
            .order_by()
        )

        student_counts = dict((s['shirt_size'], s['total']) for s in students)
        volunteer_counts = dict((v['shirt_size'], v['total']) for v in volunteers)

        counts = []
        for size in sizes:
            if size == '':
                continue
            counts.append({
                'size': size,
                'students': student_counts.get(size, 0),
                'volunteers': volunteer_counts.get(size, 0),
                'total': student_counts.get(size, 0) + volunteer_counts.get(size, 0)
            })

        totals = {
            'students': sum(c['students'] for c in counts),
            'volunteers': sum(c['volunteers'] for c in counts),
            'total': sum(c['total'] for c in counts)
        }

        context = {
            'year': year,
            'semester': semester,
            'counts': counts,
            'totals': totals
        }
        return render(request, 'admin/tshirts.html', context)

    def tshirts_csv_view(self, request, *args, **kwargs):
        from programs.models import Student, Program, Location
        from volunteers.models import Volunteer
        request.current_app = self.name

        if request.GET.get('semester'):
            year, semester = request.GET['semester'].split('-')
        else:
            today = timezone.now()
            year = today.year
            semester = utils.get_semester(today)

        students = Student.objects.for_semester(year, semester)
        volunteers = Volunteer.objects.for_semester(year, semester).distinct()

        student_location = Program.objects.for_semester(year, semester).filter(
            students=OuterRef('pk')
        ).values('location_id').order_by()[:1]
        volunteer_location = Program.objects.for_semester(year, semester).filter(
            Q(volunteers=OuterRef('pk')) |
            # Q(floaters=OuterRef('pk')) |
            Q(coordinator=OuterRef('pk'))
        ).values('location_id').order_by()[:1]

        students = students.annotate(
            type=Value('student', output_field=CharField(max_length=16)),
            location=Subquery(student_location)
        ).values('first_name', 'last_name', 'type', 'shirt_size', 'location')
        volunteers = volunteers.annotate(
            type=Value('volunteer', output_field=CharField(max_length=16)),
            location=Subquery(volunteer_location)
        ).values('first_name', 'last_name', 'type', 'shirt_size', 'location')

        # Create map of locations
        locations = {}
        for location in Location.objects.all():
            locations[location.pk] = location

        rows = students.union(volunteers).order_by('location', 'shirt_size', 'first_name')

        response = http.HttpResponse(content_type='text/csv')
        filename = 'tshirts-{}-{}.csv'.format(semester.lower(), year)
        response['Content-Disposition'] = 'attachment; filename=' + filename
        csv_writer = csv.writer(response)
        csv_writer.writerow(['name', 'type', 'size', 'location'])

        for row in rows:
            name = row['first_name'] + ' ' + row['last_name']
            location = locations.get(row['location'])
            if location:
                location_name = location.short_name
            else:
                location_name = 'Floater/Other'
            csv_writer.writerow([name, row['type'], row['shirt_size'], location_name])

        return response

    def student_history_view(self, request, *args, **kwargs):
        from students.models import Student
        order_by = {
            'name': ('first_name', 'last_name')
        }[request.GET.get('o', 'name')]

        students = Student.objects.order_by(*order_by)

        context = {
            'students': students
        }

        return render(request, 'admin/student_history.html', context)


admin_site = HiveAdminSite()


def register(cls):
    return admin.register(cls, site=admin_site)
