from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.http import Http404

from hive.admin import admin_site
import programs.urls
import volunteers.urls
import dashboard.urls
import courses.urls


def not_found(request):
    raise Http404


if settings.SITE == 'programs':
    urlpatterns = [
        url(r'^', include('django.contrib.auth.urls')),
        url(r'^', include(programs.urls)),
    ]

elif settings.SITE == 'courses':
    urlpatterns = [
        url(r'^', include(courses.urls)),
        url(r'^', include('social_django.urls', namespace='social'))
    ]

else:
    urlpatterns = [
        url(r'^admin/', include(admin_site.urls)),
        url(r'^volunteer/', include(volunteers.urls)),
        url(r'^', include(dashboard.urls)),
        url(r'^', include('django.contrib.auth.urls')),
    ]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
