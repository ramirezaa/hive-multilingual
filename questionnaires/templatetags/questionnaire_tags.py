from django.template import Library
from django.template.loader import get_template

register = Library()


@register.simple_tag
def question_field(question, field):
    type = question.type
    template_name = 'questionnaires/fields/{}.html'.format(type.lower())
    template = get_template(template_name)
    return template.render({
        'question': question,
        'field': field
    })
