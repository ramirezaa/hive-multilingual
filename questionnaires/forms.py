from django import forms

from hive.forms import BaseForm
from .models import Questionnaire


class QuestionnaireForm(BaseForm):
    questionnaire_slug = None
    questionnaire = None

    def __init__(self, *args, **kwargs):
        if self.questionnaire_slug is None:
            super().__init__(*args, **kwargs)
            return
        self.questionnaire = Questionnaire.objects.get(
            slug=self.questionnaire_slug)
        super().__init__(*args, **kwargs)
        for question in self.questionnaire.questions.all():
            field_id = 'question_{}'.format(question.id)
            self.fields[field_id] = self.get_field_for_question(question)
        self.update_fields()

    def set_initial_questionnaire_data(self, saved_data):
        self._initial_questionnaire_data = {}
        data = saved_data
        for question in self.questionnaire.questions.all():
            key = 'question_{}'.format(question.id)
            value = data.get(question.internal_key, {}).get('value')
            self.initial[key] = value

    @property
    def questionnaire_fields(self):
        if self.questionnaire:
            for question in self.questionnaire.questions.all():
                field_id = 'question_{}'.format(question.id)
                yield question, self[field_id]

    def get_field_for_question(self, question):
        field_kwargs = {
            'label': question.question_text,
            'required': question.required
        }

        if question.type in ('CHECKBOX', 'RADIO', 'SELECT'):
            choices = question.choices.strip().splitlines()
            if question.type == 'SELECT':
                choices = (('', '--------'),) + choices
            field_kwargs['choices'] = ((s, s) for s in choices)

        if question.type == 'TEXTAREA':
            Field = forms.CharField
            widget = forms.Textarea()
        elif question.type == 'CHECKBOX':
            Field = forms.MultipleChoiceField
            widget = forms.CheckboxSelectMultiple()
        elif question.type == 'RADIO':
            Field = forms.ChoiceField
            widget = forms.RadioSelect()
        elif question.type == 'SELECT':
            Field = forms.ChoiceField
            widget = forms.Select()
        else:
            Field = forms.CharField
            widget = forms.TextInput()

        return Field(widget=widget, **field_kwargs)

    def get_questionnaire_data(self):
        cleaned_data = self.cleaned_data
        data = {}
        if not self.questionnaire:
            return data
        for question in self.questionnaire.questions.all():
            form_key = 'question_{}'.format(question.id)
            value = cleaned_data.get(form_key, None)
            data[question.internal_key] = {
                'label': question.internal_label,
                'value': value
            }
        return data


class QuestionnaireModelForm(QuestionnaireForm, forms.ModelForm):
    questionnaire_data_field = 'extra_data'
    questionnaire_key = 'questionnaire'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = kwargs.get('instance', None)
        if instance:
            instance_data = getattr(instance, self.questionnaire_data_field)
            instance_data = instance_data or {}
            questionnaire_data = instance_data.get(self.questionnaire_key, {})
            self.set_initial_questionnaire_data(questionnaire_data)

    def save(self, commit=True):
        instance = super().save(commit=False)
        form_data = self.get_questionnaire_data()
        instance_data = getattr(
            instance, self.questionnaire_data_field, None) or {}
        questionnaire_data = instance_data.get(self.questionnaire_key) or {}
        questionnaire_data.update(form_data)
        instance_data[self.questionnaire_key] = questionnaire_data
        setattr(instance, self.questionnaire_data_field, instance_data)
        if commit:
            instance.save()
        return instance
