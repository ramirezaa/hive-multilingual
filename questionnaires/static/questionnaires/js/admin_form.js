(function($) {
  function updateFormsetForm(form) {
    var $form = $(form);
    var prefix = $form.parents('[data-formset-prefix]').data('formsetPrefix');
    function selectField(name) {
      return $form.find('[id^="id_'+prefix+'-"][id$="-'+name+'"]');
    }

    // fix layout since materialize doesn't support field layouts in admin
    $form.removeClass('l4');
    $form.removeClass('m6');
    var $label = selectField('internal_label').parents('.input-field');
    var $key = selectField('internal_key').parents('.input-field');
    var $type = selectField('type').parents('.select-field');
    var $required = selectField('required').parents('.checkbox-field');
    var $saveAnswer = selectField('save_answer').parents('.checkbox-field');
    var $order = selectField('order').parents('.input-field');
    $label.parent().append($key);
    $label.parent().append($type);
    $label.addClass('m4');
    $key.addClass('m4');
    $type.addClass('m4');
    $required.parent().append($saveAnswer);
    $required.parent().append($order);
    $required.addClass('m4');
    $saveAnswer.addClass('m4');
    $order.addClass('m4');

    selectField('type').on('change', function() {
      var choiceTypes = ['CHECKBOX', 'RADIO', 'SELECT'];
      var showChoices = choiceTypes.indexOf(this.value) >= 0;
      selectField('choices').parents('.input-field').toggle(showChoices);
      selectField('include_other').parents('.checkbox-field').toggle(showChoices);
    }).change();

    $form.show();
  }
  $(document).ready(function() {
    $('[data-formset-prefix]').on('formAdded', function(event) {
      updateFormsetForm(event.target);
    });
    $('[data-formset-form]').each(function(i, form) {
      updateFormsetForm(form);
    });
  });
})(django.jQuery);
