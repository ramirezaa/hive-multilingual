from django.contrib import admin
from django import forms
from hive.admin import register
from . import models


class QuestionForm(forms.ModelForm):
    class Meta:
        model = models.Question
        fields = '__all__'
        widgets = {
            'type': forms.Select(attrs={'class': 'type-select'})
        }


class QuestionInline(admin.StackedInline):
    model = models.Question
    form = QuestionForm
    fields = [
        ('internal_label', 'internal_key', 'type'),
        'question_text',
        'choices',
        'include_other',
        ('order', 'required', 'save_answer')
    ]
    # FIXME: This isn't working
    prepopulated_fields = {'internal_key': ['internal_label']}
    extra = 0


@register(models.Questionnaire)
class QuestionnaireAdmin(admin.ModelAdmin):
    icon = '<i class="material-icons">live_help</i>'
    list_display = ['name', 'slug']
    inlines = [QuestionInline]
    fields = [('name', 'slug')]
    prepopulated_fields = {'slug': ['name']}
