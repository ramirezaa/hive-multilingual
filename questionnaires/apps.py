from django.apps import AppConfig


class QuestionnairesConfig(AppConfig):
    icon = '<i class="material-icons">live_help</i>'
    name = 'questionnaires'
