from django.db import models


class Questionnaire(models.Model):
    name = models.CharField(max_length=64)
    slug = models.SlugField()

    def save(self, *args, **kwargs):
        # update order fields
        for i, question in enumerate(self.questions.all()):
            question.order = i + 1
            question.save()
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Question(models.Model):
    questionnaire = models.ForeignKey(Questionnaire, related_name='questions')
    order = models.IntegerField(default=0)
    internal_label = models.CharField(max_length=64)
    internal_key = models.SlugField()
    question_text = models.CharField(max_length=255)
    required = models.BooleanField()
    type = models.CharField(max_length=16, choices=(
        ('TEXT', 'Text'),
        ('TEXTAREA', 'Paragraph text'),
        ('CHECKBOX', 'Checkboxes'),
        ('RADIO', 'Multiple choice'),
        ('SELECT', 'Select from a list'),
    ))
    choices = models.TextField(
        blank=True, help_text='One choice per line')
    include_other = models.BooleanField('Include "Other" option')
    save_answer = models.BooleanField(
        help_text='Use most recent answer to pre-fill future submissions')

    class Meta:
        ordering = ['order']
        unique_together = ['questionnaire', 'internal_label']

    def __str__(self):
        return self.internal_label
