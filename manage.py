#!/usr/bin/env python
import os
import sys
from dotenv import load_dotenv


if __name__ == "__main__":
    from django.core.management import execute_from_command_line

    # Load environment from config.env file
    ENV_FILE = os.environ.get('ENV_FILE', os.path.join(sys.prefix, 'config.env'))
    ENV_DIR = os.path.dirname(ENV_FILE)
    load_dotenv(ENV_FILE, override=False)

    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hive.settings')

    execute_from_command_line(sys.argv)
